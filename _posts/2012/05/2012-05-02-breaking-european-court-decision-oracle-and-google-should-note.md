---
id: 1541
title: European Court decision. Oracle and Google should note.
date: 2012-05-02T10:39:53+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1541
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
This case (SAS v WPL) has a lot in common with the Oracle v Google case in the US.

The very short form: WPL created a re-implementation of the SAS Language, using the original documentation of SAS and a freebie version for personal and educational use. SAS claimed they thus infringed on copyright etc.

Seems SAS lost big time.

Now you can almost directly compare this case with Oracle v Google. Simply replace SAS Language with JAVA and watch this drama unfold. Note: IANAL but it seems Oracle wouldn&#8217;t have a chance in the EU with the current set of arguments used in the US case.

I am sure some people will downplay this decision, but IMHO this is truly important for API and the freedom to use and even reimplement them.

From the [court decision](http://curia.europa.eu/juris/document/document.jsf?text=&#038;docid=122362&#038;pageIndex=0&#038;doclang=EN&#038;mode=lst&#038;dir=&#038;occ=first&#038;part=1&#038;cid=115060):

> WPL perceived that there was a market demand for alternative software capable of executing application programs written in the SAS Language.

Sounds familiar? Think Android (Dalvik) and JAVA.

> Its [SAS] principal claims are that WPL:
> 
> – copied the manuals for the SAS System published by SAS Institute when creating the ‘World Programming System’, thereby infringing SAS Institute’s copyright in those manuals;
> 
> – in so doing, indirectly copied the computer programs comprising the SAS components, thereby infringing its copyright in those components;
> 
> – used a version of the SAS system known as the ‘Learning Edition’, in breach of the terms of the licence relating to that version and of the commitments made under that licence, and in breach of SAS Institute’s copyright in that version; and
> 
> – infringed the copyright in the manuals for the SAS System by creating its own manual.

Agains, sounds familiar? Oracle claims almost identical things wrt Android and JAVA.

Decision:

> 1. Article 1(2) of Council Directive 91/250/EEC of 14 May 1991 on the legal protection of computer programs must be interpreted as meaning that neither the functionality of a computer program nor the programming language and the format of data files used in a computer program in order to exploit certain of its functions constitute a form of expression of that program and, as such, are not protected by copyright in computer programs for the purposes of that directive.
> 
> 2. Article 5(3) of Directive 91/250 must be interpreted as meaning that a person who has obtained a copy of a computer program under a licence is entitled, without the authorisation of the owner of the copyright, to observe, study or test the functioning of that program so as to determine the ideas and principles which underlie any element of the program, in the case where that person carries out acts covered by that licence and acts of loading and running necessary for the use of the computer program, and on condition that that person does not infringe the exclusive rights of the owner of the copyright in that program.
> 
> 3. Article 2(a) of Directive 2001/29/EC of the European Parliament and of the Council of 22 May 2001 on the harmonisation of certain aspects of copyright and related rights in the information society must be interpreted as meaning that the reproduction, in a computer program or a user manual for that program, of certain elements described in the user manual for another computer program protected by copyright is capable of constituting an infringement of the copyright in the latter manual if – this being a matter for the national court to ascertain – that reproduction constitutes the expression of the intellectual creation of the author of the user manual for the computer program protected by copyright.

Not much needs to be added here. IMHO this is a very important decision and Oracle and Google should take note.

[UPDATE]

Awesome official press release of EU court at http://curia.europa.eu/jcms/upload/docs/application/pdf/2012-05/cp120053en.pdf is awesome:

> &#8220;The functionality of a computer program and the programming language cannot be protected by copyright &#8221;
> 
> &#8220;On the basis of those considerations, the Court holds that neither the functionality of a computer program nor the programming language and the format of data files used in a computer program in order to exploit certain of its functions constitute a form of expression. Accordingly, they do not enjoy copyright protection. 
> 
> To accept that the functionality of a computer program can be protected by copyright would amount to making it possible to monopolise ideas, to the detriment of technological progress andindustrial development.&#8221;
> 
> &#8220;In this respect, the Court takes the view that, in the present case, the keywords, syntax, commands and combinations of commands, options, defaults and iterations consist of words, figures or mathematical concepts, considered in isolation, are not, as such, an intellectual creation of the author of that program.&#8221;

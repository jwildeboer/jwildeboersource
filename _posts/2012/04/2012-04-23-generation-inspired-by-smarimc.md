---
id: 1539
title: 'Generation @ &#8211; inspired by @smarimc'
date: 2012-04-23T11:02:29+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1539
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
I think Smári McCarthy, a fellow [transnational](http://utrn.org) citizen, Uberhacker and admired activist, touched a special nerve when he recently [twittered](https://twitter.com/#!/smarimc/status/194354716845674496):

> Ours is a world where @ is replacing ©. Attribution, not restrictions.

Spot on. Hence I propose we start calling ourselves **Generation @** to indicate that we prefer decentralised attribution over monopolistic, old-school and restrictive, centralised systems like Copyright, patents.

It makes a lot of sense. But it lacks the most fundamental element at this moment &#8211; a decentralised, secure, reliable and open system of identity. How can we make sure we put attribution to the right person or entity? See the whole pinterest discussions and the realname policies at Google and Facebook.

Hence we need an open, decentralised, neutral identity layer on the net and in the real life &#8211; #freedentity. Let&#8217;s start working on that.

---
id: 1550
title: 'TNP/IP and TNP/ISP &#8211; Braindump of a solution #needhelp'
date: 2012-07-31T12:44:59+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1550
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
A candidate plan for a decentralized, distributed network that can complement and eventually replace a lot of proprietary stuff we use nowadays.

This is a braindump. I am hoping to find people that understand what I am talking about and are willing to get this started and participate in the discussion and implementation. Will offer beer and pizza in Munich.

My weird plan is called (since 5 minutes 😉 TNP/IP &#8211; The Transnational Protocol running on the Internet.

The short form:

  * Everything is pure JavaScript
  * Everything is Free Software, AGPLv3
  * It is modular
  * It is liberal in both input and output.

It can run on a raspberry pi or similar cheap hardware. It implements the needed standards (TBD, but for sure Jabber, SMTP, IMAP, Statusnet and RemoteStorage AKA unhosted)

Rationale: Using a single language approach we can avoid the need for expensive hardware requirements. Speed is not the big problem, freedom is.

By using webID or a similar approach this becomes your personal data engine.

The other part of the solurtion is TNP/ISP &#8211; the hosting service open standard. The only thing it does is run DNS servers (and possibly offer a caching service when your box is on limited bandwidth).

The DNS config of your domain is for free. The caching service would come at a cheap rate.

You will be free to chose whatever ISP you trust, as long as they implement the TNP/ISP standard.﻿

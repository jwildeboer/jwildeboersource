---
id: 1565
title: BREAKING! Deutsche Verleger haben eine Erweiterung der robots.txt vorgeschlagen!
date: 2012-12-05T17:02:49+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1565
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
Der Vorschlag wurde mir geleakt von einem befreundetem DevOps in einem ungenannten Verlag.

```
# robots.txt zu http://www.bild.de/
User-agent: Google-Crawler*<br />
Pay: /* {PayPal:SpingerVerlagLSR@bild.de:0.05€/click}
Free: /404.html
Free: /500.html
Pay: /erotik*  {PayPal:SpingerVerlagLSR@bild.de:2.50€/click}
```

Es wird bereits über erweiterte Sytax diskutiert. Unter anderem ein Promo: mit Datumsangaben für Freicrawlertage.

Ach ja, wer Sarkasmus und Ironie findet darf es behalten.

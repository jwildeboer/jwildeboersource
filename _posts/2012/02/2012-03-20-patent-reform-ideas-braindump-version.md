---
id: 1528
title: Patent reform ideas, braindump version
date: 2012-03-20T13:32:55+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1528
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
1. &#8220;All proceedings from infringement MUST go to the original inventor.&#8221;
1. &#8220;Ownership of a patent is bound to the inventor named in it and this is a non-transferrable right.&#8221;
1. &#8220;Only the original inventor as named in the patent can decide on the licensing terms and royalty rates and must state these a priori as part of the filing process.&#8221;

Whereas the original inventor MUST be a natural person.

Think about that for a second. There is a whole market of selling and reselling patents between patent trolls and other NPE (Non Practicing Entities) based on income from supposed infringement.

To stop this madness we can either hope for a reform in the patent system that leads to superior quality (has been tried for quite some years, has not worked so far) or, and this is the beter approach IMHO, we start to change the fundamentals by making patents inalienable and thus dry up the troll market completely.

The a priori licensing should be on a scale from limited, royalty bearing to royalty free at the other end. And changes to the licensing after the patent has been granted should only be in the direction of less restrictions, not the other way round. This is to make sure that a patent can be made more free over time but never more restricted.

Also by forcing an a priori licensing model that becomes part of the patent itself we could make sure that we return the patent system to its original goal &#8211; make knowledge available in exchange for a limited monopoly.


---
id: 1560
title: 6 things to know about patents, the short form.
date: 2012-09-11T19:29:25+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1560
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
**Basic Patent Knowledge #0:** The patent system has a long history. It&#8217;s fundamental task is to incentivize **sharing** of knowledge. Not protection of the innovator as many people assume. It is a deal with society. In **exchange** for full disclosure of your invention, you get a **limited** monopoly (20 years typically) to exploit your invention commercially. After the 20 years your knowledge enters the **public domain**.

**Basic Patent Knowledge #1**: A patent does **NOT** protect the **innovator**. It protects the one that **filed** the patent. It&#8217;s called the first-to-file doctrine and is used almost everywhere on this planet now.

**Basic Patent Knowledge #2:** A patent is a **regional** right. Typically limited by country borders. So to patent something globally you need to file a lot of patents at a lot patent offices in a lot of countries. That can become quite expensive.

**Basic Patent Knowledge #3:** A patent is presumed to be valid without a doubt. Even if you can prove that the patented thing existed before the patent was filed (in US 12 monthe before it was filed) you still need to go for an all-out invalidation process to overcome the presumed validity. This can take a long time and again is very expensive.

**Basic Patent Knowledge #4:** In some jusridictions (Europe as an example) the **non-commercial** use of patented technology is perfectly OK. Only infringement on a commercial scale causes damage.

**Basic patent knowledge #5:** A patent is an **exclusive** right. There is no rule that you must license it to others. There&#8217;s also no rule that you actually must produce something that uses the patented technology. Thus you can file a patent, sue anyone that infringes and **refuse to license** it at all.

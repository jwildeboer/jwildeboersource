---
title: "From iCloud to Nextcloud: Contacts"
categories:
  - Personal
tags:
  - iCloud
  - Nextcloud
published: true
header:
  image: mh/mh011.jpg
comments:
  host: social.wildeboer.net
  username: jwildeboer
  id: 114111281370930331
---

**Open Standards make many things possible. Today I want to show you how an Open Standard called [CardDAV](https://en.wikipedia.org/wiki/CardDAV) makes it really simple to move your address book from iCloud or just your iPhone/Mac to Nextcloud.**

This post is NOT about how to setup Nextcloud, either as managed service or selfhosted. I use a managed instance via an offering from [Hetzner](https://www.hetzner.com) called [Storage Share](https://www.hetzner.com/storage/storage-share/). For the rest of this post I expect you to have an account on a Nextcloud instance with the [Contacts](https://apps.nextcloud.com/apps/contacts) app installed and ready, that you have your username and password for that account next to you and that you are using an iPhone or Mac with your contacts in its contacts app.

**But first: backup your current contacts**

On the iPhone open the Contacts app, click on Lists at the top, long press your address book and you will see the "Export" option. This allows you to save all your contacts at once in a way you can imprt them again, should everything fail.

On the Mac, open the Contacts app, open the File menu, select Export, as vCard and now you also have a complete backup of all your contacts, just in case.

OK. Now for the complex part ;)

## 1. Add your Nextcloud Server as a CardDAV account to your iPhone or Mac

On your Nextcloud instance, go to the Contacts app and click on Contacts Settings at the bottom, then on the "..." next to your address book and finally on "Copy link" to get the link to your address book, which is the server you will need in the next step.

It looks something like this:

```
https://<YOUR.NEXTCLOUD>/remote.php/dav/addressbooks/users/hAdmin/<YOUR_USERNAME>/
``` 

![Contacts Settings in Nextcloud with the "Copy Link"](/images/2025/03/nextcloud01.png)
*Contacts Settings in Nextcloud with the "Copy Link"*

That was the most complicated step, BTW ;)

### iPhone, iPad

On your iDevice, go to Settings -> Apps -> Contacts. From there click on Contact Accounts -> Add Account -> Other -> Add CardDAV account.

![iphone/iPad add CardDAV account](/images/2025/03/Nextcloud02.png)
*iphone/iPad add CardDAV account*

Here you paste the server address you just copied from Nextcloud, your Nextcloud username and password and a nice looking name. That's it. Click on Next and your iDevice will check if it can connect and will start syncing your existing contacts in the background.

### Mac

On your Mac, open System Settings -> Internet Accounts -> Add account. From there click on -> Add Account -> Add Other Account ... -> CardDAV account. Click on Account Type and select Manual.

![Mac CardDAV account](/images/2025/03/Nextcloud03.png)
*Mac add CardDAV account*

Here you enter your Nextcloud username and password and the server address you just copied from Nextcloud. That's it. Click on Sign In and your Mac will check if it can connect and will start syncing your existing contacts in the background.

## 2. Final step: unlink iCloud

We are done. You should see all your contacts being synchronised to your Nextcloud automagically. Once that is done, you can unlink your contacts from iCloud (if you use iCloud sync that is).

### iPhone, iPad

Open Settings, iCloud, Apple Account, iCloud. Go to Saved to iCloud and select See all. Scroll down to Contacts and disable. That's it. Should it ask wjat to do with the contacts, select "keep on this iDevice".

### Mac

Open System Settings, iCloud. Go to Saved to iCloud and select See all. Scroll down to Contacts and disable. That's it. Should it ask what to do with the contacts, select "keep on this device".

### 3. That's it. That's all.

And there you have it. Your contacts are in your Nextcloud, where you can edit, export, backup them. They automagically sync with your iPhone, iPad, Mac in the background. Nice! Enjoy your new freedom!

And if something went wrong, you have all your contacts in a backup file you created as the very first step. This backup, that contains vCard entries according to the CardDAV standard, can be imported to Nextcloud with a few clicks. Feel free to think about why I know it works that simple ;)

## What about Android?

To be honest, I don't know. What I've gathered from my friends on Android is that Google doesn't fully support CardDAV the way Apple does and thus the migration isn't that simple. I'll leave it at that.
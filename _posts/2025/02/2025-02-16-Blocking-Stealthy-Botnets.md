---
title: "Those Stealthy Botnets"
categories:
  - Personal
tags:
  - eMail
  - Spam
  - Sysadmin
published: true
header:
  image: mh/mh014.jpg
comments:
  host: social.wildeboer.net
  username: jwildeboer
  id: 114013974790086537
---

It's one of those days again where botnets are hammering my little e-mail server with brute force attacks to send spam. This comes in waves, but is persistent and part of the [#SysAdminLife](https://social.wildeboer.net/tags/SysAdminLife). (They obviously fail ;) 

![IP addresses trying to send spam using my e-mail server but failing because they don't have working credentials](/images/2025/02/Botnet1.png)
*IP addresses trying to use my mailserver to send spam*

![IP addresses trying to guess passwords to mail accounts on my e-mail server so they can send spam through my server.](/images/2025/02/Botnet2.png)
*IP addresses trying to guess username/password to login to my mailserver to send spam*

I have recently [learned](https://sponsor.ajay.app/emails/) that there is a shady market out there where app developers are offered to include a little "peer to peer proxy" library in their app so that the users of said app "donate" 120-150 kbps of their network traffic to that company.

The app developer gets paid for including the library (one offer is 18 cents per active user). These shady companies claim that Apple and Google App stores are fine with their libraries. This is one sneaky way how botnets are created out there. There are many others, unfortunately.

![One of these botnet companies making an offer to a browser-plugin developer](/images/2025/02/Botnet3.png)
*One of these botnet companies making an offer to a browser-plugin developer ([source](https://sponsor.ajay.app/emails/))* 

These shady companies then sell access to these (IMHO) infected apps to execute commands that can be exactly the SASL/SMTP attacks my mailserver is seeing. The shady companies of course say that they only do legitimate stuff but that ultimately they cannot control all traffic caused by their customers.

These botnets are quite smart. As they have tens of thousands of (typically) residual or mobile IP addresses at their disposal, they use each one of them for exactly one single try per day to log in to my mail server or to try an SMTP connection. That way they stay under the radar of most simple solutions like [fail2ban](https://github.com/fail2ban/fail2ban) that count attempts per IP to make a block decision.

But as these botnets distribute their attempts across all of these IP addresses, they can do a quite sustainable distributed brute force password guessing attack quite effectively. Most sysadmins see these attempts as noise, anyway. I do not :)

So I simply block all of them on first sight for a long time. I have collected some 50k IP addresses this way that are currently blocked by my firewall. 98% are IPv4, BTW. IPv6 is still a rare sight :)

The IP addresses come from all over the world. Some people suggested I might be able to identify ASNs (Autonomous System Number) that are abused more frequently this way and block the whole thing instead of single IPs, so I took a look at that possibility

TL;DR no.

I took the IP addresses trying to do a brute force SASL login during the past 7 days and matched them to their respective ASN. It's a quite broad distribution with on average less than 4 IP addresses per ASN.

![Screenshot of attacker IPs, grouped by country and ASN](/images/2025/02/Botnet4.png)
*Screenshot of attacker IPs, grouped by country and ASN*

On average less than 4 IPs per ASN, so blocking ASNs wouldn't help that much. So I keep my simple solution running. A cronjob on my mailserver checks the logfiles for these attackers using a [shellscript](https://codeberg.org/jwildeboer/gists/src/commit/7e247ef89c627c409db8c41158830be9f5e6a293/2023/20230422LogCheck.md) and sends me an e-mail every morning with copy/paste commands to add these pesky critters to the firewall.

I prefer to do it this way. I could automate that part, obviously. But I like to know what's gong on with my little server and just by looking at the length of the e-mails I know when a new wave has started.

I made the raw data available in [this spreadsheet](https://www.icloud.com/numbers/07eRpSahhL5w27TQ39J-jQeuQ#ASN1w), in case you want to play with it. (you can export to CSV from there, I replaced the last octet of the IP addresses with NNN to anonymise the attackers a bit).
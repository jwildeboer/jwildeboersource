---
title: "The MRZ Part 1: A Relic With Potential — IMHO"
categories:
  - Personal
tags:
  - MRZ
  - MRTD
  - ICAO9303
published: true
header:
  image: mh/mh017.jpg
comments:
  host: social.wildeboer.net
  username: jwildeboer
  id: 113783614504252782
---

*During the holidays, I had this urge to program some stuff for fun and learn some new things. So I taught myself how to create nice looking, responsive forms and as an [example (TD1)](file:///Users/jwildeboer/repos/UTNR/MRTD/MRZ/TD1MRZ.html), I [implemented (TD2)](file:///Users/jwildeboer/repos/UTNR/MRTD/MRZ/TD2MRZ.html) [generators (TD3)](file:///Users/jwildeboer/repos/UTNR/MRTD/MRZ/TD3MRZ.html) for the so-called MRZ, the Machine Readable Zone, that you can find on your passport or, in quite some countries, on your credit-card sized identity document. The forms work and look nice, so that part is done. But why do I play with the MRZ? Well, that is a longer story, that I want to outline here without getting lost in too much details.*

## The MRZ — just the facts

From [ICAO9303, part 3](https://www.icao.int/publications/Documents/9303_p3_cons_en.pdf), chapter 4:

> The **MRZ** [Machine Readable Zone] provides a set of essential data elements in a format, standardized for each type of **MRTD** [Machine Readable Travel Document] that can be used by all receiving States regardless of their national script or customs.

I'll spare you the history on how the [ICAO](https://www.icao.int/Pages/default.aspx) became the authority to define the layout, size and content of Travel Documents like passports, laissez-passer, convention travel documents and visas. The main thing you need to know is that they are :) The rules and guidelines are published as documents called the [ICAO 9303 document series](https://www.icao.int/publications/pages/publication.aspx?docnum=9303). They are open to everyone, no license fees, no agreements, you can just download them in several languages. I do. Since years. Every time they update, I do too.

![A MRZ from a TD1 sized travel document](/images/2025/01/TD1MRZ.png)
*The MRZ of a TD1 (credit card) sized travel document*

So what does the MRZ do? Its main task is to be read by machines at border control or at other places where identity checks are needed. These scanners can read the MRZ and check the various contained fields, check the correctness of the check digits and use the collected data to do an extended lookup to verify the presented document is valid. But yu can also simple read the three lines yourself as they are simple characters, not a bar- or QR-code. 

## Technical details, please

OK. In this post I will solely focus on the [TD1 (credit card)](https://www.icao.int/publications/Documents/9303_p5_cons_en.pdf) sized travel documents. Mainly because they contain the most data in the MRZ compared to the other two sizes ([A7 sized TD2](https://www.icao.int/publications/Documents/9303_p6_cons_en.pdf), used in the past for the german Personalausweis and the [B7 sized TD3](https://www.icao.int/publications/Documents/9303_p4_cons_en.pdf) that is used for the MRTD page in your passport), which "only" offer two lines and less optional data fields.

But they all share similar requirements, to make it easier to build MRZ scanners that can read all 3 sizes.

![The MRZ doimensions on different sized travel documents](/images/2025/01/MRZScan.png)
*Specifications of the positioning and size of the MRZ*

There are general rules about the MRZ. The typeface used **MUST** be [OCR-B](https://en.wikipedia.org/wiki/OCR-B), originally designed by [Adrian Frutiger](https://en.wikipedia.org/wiki/Adrian_Frutiger), back in 1968, and standardised in [ISO1831](https://www.iso.org/standard/6480.html), which is not an open standard and costs you CHF 177, if you want it. The size and dimensions of the MRZ are also specified with minimal tolerances. Implementing those specifications is however not that complicated, I did it all with [Inkscape](https://inkscape.org), using the OCR-B version available at [rastislavcore/ocr-b](https://github.com/rastislavcore/ocr-b/tree/master/dist) as OTF and TTF.

## But what's in there and why is it useful?

Now we come to the meat of this blog post. People that know me also know that I am fascinated with analog technology. Vinyl records. Tape backups. Books. Film photography. The MRZ comes from this era too. It is a combination of human and machine readable information, perfectly usable in off-grid situations where the internet might not be available or in places where you don't **want* to share personal identifiable information (PII) over the net.

So a MRZ could be useful outside of Travel/identity documents. You could print them on conference passes, concert tickets, access badges for companies/buildings. You can store unique IDs in here that are human and machine readable, giving you a fallback (or default) solution to check validity on a basic level.

Let's look at what the TD1 MRZ can store in its three standardised lines. And how we can creatively use that in use cases like the ones I just described.

![The TD1 MRZ explained in ICAO9303 part 5](/images/2025/01/TD1MRZFields.png)
*The TD1 MRZ explained in ICAO9303 part 5*

Uff. That's a lot. And trust me, a lot of knowledge, experiences and negotiations went into this definition. Every single character has an important function and the experts at ICAO invested a lot of time and effort in getting this right, though it is not without problems — it's a global compromise with limitations that go back a long time.

### The first MRZ line for a TD1 document

The first line start with "what kind of document is this?" and the answer can be A, C, or I (typically I for Identity Card) and can be followed by a second character with some rather weird limitations ([ICAO9303, part 5](https://www.icao.int/publications/Documents/9303_p5_cons_en.pdf), note k) on page 17):

> k) The first character shall be A, C or I. Historically these three characters were chosen for their ease of recognition in the OCR-B character set. The second character shall be at the discretion of the issuing State or organization except that i) V shall not be used, ii) I shall not be used after A (i.e. AI), and iii) C shall not be used after A (i.e. AC) except in the crew member certificate.

After that comes the 3 letter code of the issuing organisation, which can be a country (in that case the 3 letter code is defined in [ISO8166-3](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3)) or from any other officially recognised organisation as listed in [ICAO 9303, part 3](https://www.icao.int/publications/Documents/9303_p3_cons_en.pdf), chapter 5. That list contains some weird entries like XOM, the [Sovereign Military Hospitaller Order of Saint John of Jerusalem, of Rhodes and of Malta](https://en.wikipedia.org/wiki/Sovereign_Military_Order_of_Malta), a fascinating entry that I won't go into detail here.

![List of official issuing organisations beyond countries](/images/2025/01/IssuingOrg.png)
*ICAO recognised issuing organisations beyond countries*

Next we have the document number, which can be up to 9 characters but limited to 0-9 and A-Z. Followed by a check digit, calculated according to the algorithm defined in [ICAO9303 part 3](https://www.icao.int/publications/Documents/9303_p3_cons_en.pdf), chapter 4.9.

How do you calculate that check digit? Well, here's my working version in Javascript (no license attached, feel free to copy and use in any way you need):

```javascript
/* The ICAO9303-3 Check Digit algorithm

- Take the input, character by character.
- 0-9, A-Z and '<'' are the only characters allowed
- Transform the character into a number between 10 (A) and 35 (Z),
  '<' becomes 0
- Multiply the number with a weight, starting with 7 for the first
  character, 3 for the next character, 1 for the next, repeat
- Add the results
- The remainder of modulo 10 on the sum is the check digit

Example: 123AF is the string. The respective numbers thus are
1,2,3,10,15. 1*7=7, 2*3=6, 3*1=3, 10*7=70, 15*3=45.
7+6+3+70+45=131. The remainder of 131 modulo 10 is 1.

The MRTD check digit for 123AF is 1

*/

function check_digit(str) {
  let weights = [7,3,1];
  let sum = 0;
  for (let i=0; i < str.length; i++) {
    let c = str[i];
    let ord = str.charCodeAt(i);
    if (c == '<')
      c = 0;
    else if (ord >= 65 && ord <= 90)
      c = ord - 55;
    sum += c * weights[i%3];
  }
  return sum % 10;
}
```

After that we have the Optional Data Element 1 (ODE), which can be up to 14 characters, again, only 0-9 and A-Z are allowed. If the ODE is shorter than 14 characters, it is padded with the '<' character.

At the very end of line 1 always add a final '<'.

### The second MRZ line for a TD1 document

Here we start with the date of birth of the holder of the document, followed again by a check digit. This is formatted as YYMMDD, so only two digits for the year. Interpreting this back to the correct year (1925 or 2025?) is technically impossible. This is why you don't even try here. Normally the birth date will be on the front page of the document in the full 4 digit year format.

Next we have the gender of the holder. It can be M for male, F for female or X for "not specified". This is the first time we see something that strikes me as quaint in these modern times. But it is the way it is. I am just describing, not judging the specs (though I have a very strong personal opinion on this field and how it should allow for more options). The standard allows for some freedom though, see [ICAO9303, part 5](https://www.icao.int/publications/Documents/9303_p5_cons_en.pdf), note f) on page 17:

> f) Where an issuing State or organization does not want to identify the sex, the filler character (<) shall be used in this field in the MRZ and an X in this field in the VIZ.

The "good" thing is that the gender of the document holder is not used in the overall check digit calculation, so it technically can be any character of the 0-9 and A-Z allowed ones, though many MRZ checkers will complain if you don't use M, F or X. But we can have an open source library that is more flexible :)

Next we have the expiry date of the document, again in YYMMDD and again followed by a check digit.

After that the nationality as (again) 3 letter code of the holder. Now here are some more exceptions. It doesn't have to be a country code like NLD for The Netherlands. This can also be one of the X series for example for stateless persons. See [ICAO9303 part 3](https://www.icao.int/publications/Documents/9303_p3_cons_en.pdf), chapter 5 for more details.

And finally the second optional data element (ODE), which can be up to 10 characters, again only 0-9 and A-Z, padded with "<" if it uses less than 10 characters.

This leads to the overall check digit, again using the algorithm defined in [ICAO9303, part 3](https://www.icao.int/publications/Documents/9303_p3_cons_en.pdf), chapter 4.9 but the input this time is a string consisting of the document number and its check digit, the content of the Optional Data Element (ODE) 1, the "<" character, the date of birth and its check digit, the expiry date and its check digit, the 3 letter nationality (or not) code of the holder and the content of the Optional Data Element (ODE) 2. As you can see, neither the document type, gender or name are included. This gives a lot of opportunities to use the MRZ in more creative ways for other use cases!

### The third MRZ line for a TD1 document

This line "only" contains the name of the holder. But transliterated according to the rules defined in [ICAO9303 part 3](https://www.icao.int/publications/Documents/9303_p3_cons_en.pdf), chapter 6 and appendix B for Arabic characters, so it only uses the A-Z characters. No latin-1, no accents, no circumflex, no Cyrillic, Arabic or other non A-Z characters.

The transliteration rules defined in part 3 are a fascinating insight into mapping non-ASCII characters in some acceptable way. And these rules fail at a lot of places. They simplify, cause ambiguity and are quite ignorant of the non-western, non-latin characters world but try hard to make it suck less, IMHO.

Also, as you only have a limited amount of characters, long names will be cut off in unexpected ways that are not exactly defined, leaving a lot of room for interpretation to those trying to implement all of this.

But, as this line isn't part of the overall check digit calculation in line 2, one can technically simply use a line consisting of just the filler character "<" and be done with all of this. You can always print the name in whatever way you want on the card outside of the MRZ.

![A TD1 MRZ without a holder's name](/images/2025/01/TD1MRZNoName.png)
*A MRZ without a name*

Again, most of the MRZ libraries out there might bail at the third line being empty, but we can always fork/create variants for our needs, can't we? That's the power of open source!

## OK, OK. And now?

I think this post is long enough already. Let me put my thoughts on how to use a MRZ in conference, concert tickets, building/company access badges in part 2, which will come soon.

Some teasers: We could use a different document identifier like "E" instead of I, A or C to create a new realm of documents for E(vents) or E(ntry). We can use the total of 33 possible characters (document number, ODE 1 and 2) to store unique IDs. We can use the date of birth field as start of validity for event passes. We could use the MRZ in combination with a QR code as simple second factor solution.

I will add a link to part 2 right here once done. Thank you for your interest thus far and on to the fun part!
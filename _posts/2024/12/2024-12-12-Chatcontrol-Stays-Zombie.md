---
title: Hungary's Bluff Is Called. Chatcontrol Stays Zombie
categories:
  - Personal
tags:
  - chatcontrol
published: true
header:
  image: mh/mh012.jpg
comments:
  host: social.wildeboer.net
  username: jwildeboer
  id: 113641478574348873
---

*Commission and Council have just announced the 4th season of [#Chatcontrol](https://social.wildeboer.net/tags/Chatcontrol)! Right after the cliffhanger of the season 3 finale called "too many abstains" was aired. [#SarcasmButOnlyHalf](https://social.wildeboer.net/tags/SarcasmButOnlyHalf)*

## The TL;DR

Hungary desperately wanted to have this topic on the agenda. They really tried to force a vote. But it became quite clear that there is no majority for the current proposal. So the vote was postponed (again).

## Slightly longer

[#Chatcontrol](https://social.wildeboer.net/tags/Chatcontrol) was on the [agenda](https://www.consilium.europa.eu/en/meetings/jha/2024/12/12/) of the council in its Home Affairs configuration today. There was no vote. Procedure 2022/0155(COD) stays in Zombie mode and is pushed to the incoming council presidency of Poland. It remains to be seen if they bring it up again in the first 6 months of 2025. In the council the countries that would vote at least ABSTAIN declared why. That is all that happened today.

The opposing countries (AT, DE, SI, LU, NL, CZ, PL, EE, FI, BE) more or less agreed: Fighting child abuse is important and needed. But the proposed detection regime is deemed to violate fundamental rights in unacceptable ways. So either that is solved in an acceptable way or the whole thing stays in Zombie mode (and should be buried, IMHO).

At the [press conference](https://video.consilium.europa.eu/event/en/27764) after the council meeting, the outcome is explained. Statement by the Hungarian representative: No agreement was reached on teh current compromise proposal, so no negotiations with parliament. He expresses hope that the incopming polish presidency will get the proposal to an agreeable state during their presidency in January-June 2025. Does not mention the main reason given by opposing member states — privacy and fundamental rights.

Commissioner Brunner: Regrets that council has not achieved an agreement on fighting Child sexual abuse based on th current compromise proposal. Insists we must do everything to protect children and thus must find a solution. Does not mention the main reason given by opposing member states — privacy and fundamental rights.

During the question time at the press conference, only questions on border/syria/refugees topics are raised. That's also a reality check, IMHO. [#Chatcontrol](https://social.wildeboer.net/tags/Chatcontrol) isn't that big of a topic for the media. That's unfortunately quite normal.

There were rumours that Poland and/or Czechia would switch their vote but that was just that. Rumours. Now Poland has inherited this ugly package for their council presidency and we will see what they do with it. Let's hope they make the call to end the procedure and start a new one.

## Public Council Session Report

The video recording of the 2024-12-12 Public Session of the Justice and Home Affairs Council is at [https://video.consilium.europa.eu/event/en/27778](https://video.consilium.europa.eu/event/en/27778) (the CSAM/Chatcontrol part starts at 17:54:23).

Sándor Pintér, Hungarian Minister for Home Affairs, opens with the generic "we all want to protect the children! So much needs to be done! 36 million victims last year! 100 million cases! Grooming rises at 320%!" style of languag (he doesn't give any sources for these huge numbers).

Commissioner [Magnus Brunner](https://commission.europa.eu/about/organisation/college-commissioners/magnus-brunner_en) also has the expected main talking points "poor children can't speak for themselves, parents can be evil too, so we must enable companies to report abuse". Next step should be Trilogue between commission, council and parliament to get to a solution that member states can support. It's all super urgent. Decide now or else!

But it is already clear. There will be no vote today. Instead the opposing ministers are asked to explain their position..
 
- Austria: We support the goals, but criticise risking fundamental rights, so abstain.
- Germany: Must protect children! Fight the abusers! But: Chatcontrol, esp encryoted comm not protected in current proposal, possible violation of fundamental rights. Abstain.
- Slovenia: Have always been for measures on risk minimisation, full support for that. But general suspicion on all communications is disproportionate. Problematic. Abstain.
- Luxemburg: Have to strengthen laws, full support. But: Cannot accept current proposal. Risk of surveillance, current proposal disproportionate. Could lead to courts dismissing proof. So abstain.  
- Netherlands: Want to fight abuse. But: need to look at implications. especially fundamental rights. Abstain.
- Czechia: Very urgent topic. As father of 3 children. Has been focus during CZ presidency too. End to end encryption important topic. Thx to Hungary presidency. But: abstain.

"France? No statement? So Poland"
 
- Poland (rumoured to switch but stays strong): Parliament position: Protect privacy and children. We had problems with breaking into privacy. Current proposal not good enough. Abstain.
- Estonia: Abstain. Need to keep working on this. Privacy must not be sacrificed. Abstain.
- Finland: Agree hat important, but abstain. Proposal goes too far.
- Belgium: Thanks for new ideas and corrections, but abstain.

So here we are. Chatcontrol isn't dead. But it became clear what the problem is. The bluff has been called. You cannot keep on trying to hide attacks on fundamental rights like privacy in a piracy/terrorism/child absue context. This gives me a bit of hope. On to the next season of this series!
---
# title: 
categories:
  - Personal
tags:
  - Woodpecker
  - Codeberg
published: true
header:
  image: mh/mh010.jpg
comments:
  host: social.wildeboer.net
  username: jwildeboer
  id: 113650386723289071
---

*This blog was hosted by [Codeberg](https://codeberg.org/) and whenever I post or update, some things happen in the background to give you, my dear readers the newest and freshest content to enjoy. I explained (and just updated) in excruciating detail how this all works in [Using Woodpecker CI for my static sites](https://jan.wildeboer.net/2022/07/Woodpecker-CI-Jekyll/) a while ago.*

The moving parts to generate the static pages that form my blog are [a git repository](https://codeberg.org/jwildeboer/jwildeboersource) where the sources are maintained, a [Woodpecker](https://woodpecker-ci.org) instance run by codeberg that takes my [YAML](https://codeberg.org/jwildeboer/jwildeboersource/src/commit/7eadd90b743a619f8ca4cd6dfc1455fc9c8f1786/.woodpecker.yml) file to run a container that generates the static pages and then pushes the generated files to [codeberg pages](https://codeberg.page).

To be able to do all that automagically, a token is needed that allows the CI to access and update the got repos. That token should be kept secret, obviously. Now in the past this worked like this (abbreviated for clarity):

```yaml
# .woodpecker.yml
pipeline:
  build:
    image: jekyll/jekyll
    secrets: [ cbtoken, cbmail ]
    commands:
      - git config --global user.email "$CBMAIL"
      - git remote set-url origin https://$CBTOKEN@codeberg.org/jwildeboer/jwildeboer.git
```

In woodpecker I added the two secrets, `cbtoken` and `cbmail`, which were then made available as environment variables `$CBTOKEN` and `$CBMAIL`.

## BUT

Woodpecker likes to change things and sometimes they do that in ways that break backward compatibility. A while ago they decided that `pipeline:` wasn't a good name, so I had to change all instances across all my repos to switch to the new syntax called `steps:`. I only learned about this breaking change as my pipelines failed and I had to search around for what was causing these errors.

Oh, and I also had to add `- name:` to the build. And `when:`. Because that's now needed too. Le sigh.

## AND AGAIN

So yesterday I noticed that again some kind of change was made that will break backward compatibility. This time the Woodpecker team decided that the `secrets:` syntax had to be replaced with something slightly more complicated. It started with me seeing obscure warnings like this when I updated this blog:

![Woodpecker Warning](/images/2024/12/woodwarn.png)
*Woodpecker warns that `secrets:` is now deprecated.*

So yeah. The syntax for secrets is changing and the "old" method will soon be deprecated. So. Let's fix this.

The new syntax is slightly more complicated. Not only do you now have to declare secrets in a totally different way, you also need to change the way you use the secrets in your pipeline, err, steps. No more simple `$CBTOKEN` but now you have to use `$${CBTOKEN}` instead to get the same result as before. 

I had a hard time of guessing and trying to get it all back to work, as you can see in the history of the [woodpecker.yml](https://codeberg.org/jwildeboer/jwildeboersource/commits/branch/master/.woodpecker.yml) file. But I had generous help from codeberg people and other friends in the community! Thank you all! You can read the "drama" on me struggling but finally making it [in this thread](https://social.wildeboer.net/@jwildeboer/113641515179786589) over on the Fediverse :)

![Trial and error](/images/2024/12/woodpeckerstuff.png)
*Trying, failing, trying again until it works*

The [documentation](https://woodpecker-ci.org/docs/next/usage/secrets#use-secrets-in-settings-and-environment) does explain the new way, but either I am getting too old or the text isn't that helpful, it took me some time to understand what is now the working solution and how I need to change my wokflows. 

## TL;DR THE BEFORE AND AFTER

So we went from [this](https://codeberg.org/jwildeboer/jwildeboersource/src/commit/247d6f17c26bd08b5aa2ada8739290403472daf7/.woodpecker.yml):

```yaml
# .woodpecker.yml
pipeline:
  build:
    image: jekyll/jekyll
    secrets: [ cbtoken, cbmail ]
    commands:
      - git config --global user.email "$CBMAIL"
      - git remote set-url origin https://$CBTOKEN@codeberg.org/jwildeboer/jwildeboer.git
```

to [this](https://codeberg.org/jwildeboer/jwildeboersource/src/commit/7eadd90b743a619f8ca4cd6dfc1455fc9c8f1786/.woodpecker.yml):

```yaml
# .woodpecker.yml
steps:
  - name: BuildAndPublish
    build:
      image: jekyll/jekyll
      when:
        - event: manual        # workflow should run if you execute it manual
        - event: push          # workflow should run on each push ( = on each commit)
      environment:             # 2024-12-13 New syntax that replaces deprecated secrets approach
        CBTOKEN:
          from_secret: cbtoken # Auth token to push site to Codeberg Pages
        CBMAIL:
          from_secret: cbmail  # Correct mail for commit messages
      commands:
        - git config --global user.email "$${CBMAIL}"
        - git remote set-url origin https://$${CBTOKEN}@codeberg.org/jwildeboer/jwildeboer.git
```

and now my blog is updating again. Until the next breaking change ...

I guess the next step is to switch my static sites to [Forgejo](https://foprgejo.org) actions and self-host on one of my public servers. But that will take some more learning, testing, failing, cursing and finally getting it to work in a way that I can share it in another blog post :)
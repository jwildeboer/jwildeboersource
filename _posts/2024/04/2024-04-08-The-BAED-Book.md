---
title: The BÆD books, my little trick
categories:
  - Personal
tags:
  - MentalHealth
  - Dune
published: true
header:
  image: mh/mh007.jpg
comments:
  host: social.wildeboer.net
  username: jwildeboer
  id: 112236741123382947
---
My personal little mental health trick. I have a notebook and a pen at my bed. It's called the BÆD book. All the negative things, the moments where I felt bad for internal or external reasons, I collect them during the day and put them in the "will write this down this evening" bucket, while I continue to do my work, discussion, or whatever else I am engaged in at that moment. In the evening I take my notebook and already 90% of what I thought belonged there has disappeared.

The few things that make it are safely stored there. Every 31st of December I burn the book, unread. and replace it on the 1st of January with a new BÆD book.

It's my version of what my grandma once told me. She said she has a bucket sitting right outside of the bedroom. Every evening she looked into the bucket and put all her negative thoughts in, so she could go to sleep without them. In the morning she would look in the bucket and see it was always empty. She shrugged and moved on. It's a simple trick, but it works for me.

(important note: my bedroom is free of mobile phones and computer screens. I had quite severe insomnia for many years and keeping the bedroom reserved for sleeping has really helped me deal with it)

It's called the BÆD book because that ligature, though used in a wrong way, I know. But somehow it feels nice to me that is it there, every evening :)

Anyway, take this idea as it is meant to be. It helped me, I share it, maybe it'll help someone out there too. No need to tell me, no need to ask me more questions about MY mental health. I am fine :) Sharing is caring!

The important part is the mental "will write this down this evening" bucket. It means that I don't let out my anger in the moment. This allows me stay focused and react in good ways. It does NOT mean that I simply swallow everything that hurts me. I make myself aware that I DO feel hurt or negative. That's what the mental bucket is for. Over the years (yes, I had many BÆD books) it helped me a lot in staying focused in the moment, knowing that my anger has a place to land.

I am more than happy to admit that it is a combination of my grandmas wisdom and the Litany Against Fear in the Dune books :)
```
  I must not fear.
  Fear is the mind-killer.
  Fear is the little-death that brings total obliteration.
  I will face my fear.
  I will permit it to pass over me and through me.
  And when it has gone past, I will turn the inner eye to see its path.
  Where the fear has gone there will be nothing. Only I will remain.
```
That's all. May you find something useful in me sharing this rather personal thing. What helps me, might help someone else out there. I hope it does!

ADDENDUM: OK, this is a bit creepy. One day after I wrote this, a paper gets published with the title ["Anger is eliminated with the disposal of a paper written because of provocation"](https://www.nature.com/articles/s41598-024-57916-z)[^1] by which supports my intuitive approach with data from science.

[^1]: Kanaya, Y., Kawai, N. Anger is eliminated with the disposal of a paper written because of provocation. Sci Rep 14, 7490 (2024). https://doi.org/10.1038/s41598-024-57916-z
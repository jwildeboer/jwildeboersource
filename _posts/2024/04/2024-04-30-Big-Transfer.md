---
title: The Big Transfer — Moving Away From gandi.net
categories:
  - Personal
  - OpenKnowledge
tags:
  - Hosting
  - Domains
published: true
header:
  image: mh/mh006.jpg
comments:
  host: social.wildeboer.net
  username: jwildeboer
  id: 112362503008329303
---

[gandi.net](https://gandi.net) was my domain registrar for many years (20+). They were wonderful. No nonsense domain management with full control over even the weirdest settings. Extremely reliable. Fast and friendly support with deep technical knowledge. I really loved them and promoted the hell out of them, whenever someone asked where to put his/her/their domains. But it is time to move on.

![The Gandi Web Interface, my old friend](/images/2024/04/gandi.png)
*The gandi web interface, ah, my old friend*

Last year, in February, what was [announced](https://your.online/press-release/) as a merger between gandi.net and Total Webhosting Solutions from the Netherlands, resulting in [Your.Online](https://your.online) (a web page that hasn't seen much updates since the announcement) turned out to be exactly what many predicted.

**Prices for domain registrations and renewels shot up. BIG time.**

Renewals for `.com` and `.org` domains went from 13€ back in 2015 to 19,99€ (+53,8%) now. For `.net` the jump was even bigger: from 14€ back in 2016 to 24,99€ now! That's 78,5% more! I admit, I didn't really care about the price changes for many years, but now I do.

I decided to wait a bit. Maybe they would come back to reasonable prices. But now that some of my domains have been renewed, I experienced the massive price hike again. The (automatic) renewal of a CNO (`.com`, `.net`, `.org`) combo, 3 domains, resulted in an invoice of 77,00 € including VAT. Now that is a **LOT** of money for the simple act of renewing 3 domains that I "own" since 10+ years.

## Time to pack up stuff and go

So I went looking and compared prices, based on my domain portfolio, limited to EU-based registrars (with some exceptions), because I definitely won't take my domains to a US based company. The [fediverse](https://social.wildeboer.net/@jwildeboer/112356137849601796) was really helpful in sharing candidates after I asked, thank you to all who helped!

I took my domain list and counted how many I have in which TLD. Created a table and some formulas to see how much I could save. Without further ado, here's the table.

Prices are renewal prices, without VAT, I have added links to the pricelists, so you can check for yourself[^1], which you should as these prices change often and in weird ways.

| TLD | | .com | .de | .eu | .net | .org | .se | Total/Year |
| :--- | :---: | ---: | ---: | ---: | ---: | ---: | ---: | ---: | 
| **# of domains** |  | **4** | **1** | **0** | **8** | **6** | **1** | **20** |
| [gandi.net](https://www.gandi.net/en/domain/tld) | FR | 19,99 € | 11,47 € | 19,99 € | 24,99 € | 19,99 € | 24,99 € | **436,26 €** |
| [netcup.de](https://www.netcup.de/bestellen/domainangebote.php?art=global) | DE | 14,76 € | 5,04 € | 9,48 € | 25,32 € | 18,00 € | 41,04 € | **415,68 €** |
| [joker.com](https://joker.com/domain/prices) | DE | 18,86 € | 5,00 € | 8,33 € | 22,15 € | 18,74 € | 26,74 € | **396,82 €** |
| [vimexx.nl](https://www.vimexx.nl/domeinnaam/domeinnaam-extensies) | NL | 16,49 € | 13,99 € | 7,99 € | 19,99 € | 19,99 € | 35,79 € | **395,60 €** |
| [tecspace.de](https://www.tecspace.de/domain-preisliste) | DE | 17,95 € | 5,00 € | 15,00 € | 17,95 € | 21,95 € | 30,00 € | **382,10 €** |
| [hetzner.de](https://www.hetzner.com/de/domainregistration/) | DE | 15,47 € | 11,90 € | 12,50 € | 16,66 € | 17,85 € | 55,93 € | **370,09 €** |
| [mythic-beasts.com](https://www.mythic-beasts.com/domains) | UK | 16,97 € | 11,70 € | 12,29 € | 20,48 € | 14,04 € | 36,86 € | **364,52 €** |
| [netim.com](https://www.netim.com/en/domain-name/extensions-list) | FR | 14,00 € | 12,00 € | 6,00 € | 18,00 € | 18,00 € | 24,00 € | **344,00 €** |
| [ovhcloud.com](https://www.ovhcloud.com/en/domains/tld/) | FR | 15,98 € | 11,98 € | 10,19 € | 18,09 € | 16,99 € | 17,24 € | **339,80 €** |
| [inwx.de](https://www.inwx.de/en/domain/pricelist) | DE | 15,95 € | 4,65 € | 10,12 € | 17,26 € | 14,17 € | 25,00 € | **316,55 €** |
| [cloudns.net](https://www.cloudns.net/domain-pricing-list/) | BG | 12,45 € | 7,99 € | 7,43 € | 15,15 € | 14,22 € | 18,50 € | **282,81 €** |
| [mijn.host](https://mijn.host/domeinnaam/prijzen/) | NL | 12,99 € | 5,99 € | 5,99 € | 15,99 € | 12,99 € | 17,99 € | **281,80 €** |
| [domainname.shop](https://domainname.shop/pricelist) | NO | 13,99 € | 6,49 € | 5,99 € | 13,99 € | 13,99 € | 13,99 € | **272,30 €** |
| **unnamed** |  | 11,60 € | 4,70 € | 6,25 € | 13,60 € | 10,60 € | 12,80 € | **236,30 €** |

Now **that** is a list full of weird differences. Look at the fluctuations of the prices for `.se` or `.de` domains! And after all the checking and comparing I knew — I could save a lot of money.

Here as a nice graph, just to show you expensive gandi has become. It makes me sad.

![Graph of total prices, ranging from 436,28 € down to 236,30 €, with an average around 345 €](/images/2024/04/domainGraph.png)

## So let's move to unnamed

And sure, you will wonder: Who is "unnamed"? Well, it is a small but friendly hosting company, who decided to make me a special personal offer with very attractive prices. But the deal is that I cannot disclose the name of said company. And I respect that. So trust me when I say that this company is EU-based and super nice :)

With that special offer I can go from an average of 22€/year per domain at gandi to now just 12€/year. Win!

AND NO, JAN, THIS DOESN'T MEAN YOU CAN BUY MORE DOMAINS!1!! ;)

I just transferred the first domain to test everything and it took merely a few minutes and all seems to work. Yes, even dns :)

## Goodbye and thanks for all the fish, gandi.net!

Gandi was a real good registrar for many, many years. But all good things come to an end. Now to clean up all dns entries and other stuff before I can initiate the Big Transfer in the next few days :)

## Update 2024-05-04 All is good

I have moved the big majority of my domains away from gandi to my new provider. As I could preload my [dns config](https://jan.wildeboer.net/2022/07/DNS-done-my-way/), the domain transfers went mostly unnoticed to the outside world. At some point the nameservers changed. That's all. I am cleaning up a bit too, documenting my setup and I have already added 2 new domains to my portfolio. Damn ;)

[^1]: Prices as of 2024-04-30, they can get updated fast, so take this as a snapshot only. Transfer of a domain typically causes additional costs, which I have left out of this table.
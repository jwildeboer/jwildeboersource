---
title: The 2024-02 Spam Wave on the Fediverse and what we can learn (unfinished)
categories:
  - Personal
tags:
  - ActivityPub
  - Fediverse
  - Mastodon
  - Spam
published: true
header:
  image: mh/mh006.jpg
comments:
  host: social.wildeboer.net
  username: jwildeboer
  id: 111970936732206974 
---

[\#UnpopularOpinion](https://social.wildeboer.net/tags/UnpopularOpinion) The current spam wave supports one of my suspicions that federated networks should be built as a web of trust, Friends of a Friend style. Open registrations invite abuse and there's only so much algorithmic stuff you can throw at that. An invitation based system is also not a perfect solution as it creates artificial scarcity. A solution somewhere in-between is needed but I am still pondering how that could look like.

So let's set the stage a bit.

The first thing I want to note is that in a federated world, the admin(s) of an instance play a decisive role in defining rules for "their" instance and making sure these rules are actively enforced. We often ignore their role, treat them as invisible and in the background, mostly making sure stuff works.

I have my problems with that notion.

We often purely focus on "bad" users but ignore the role of the instance. Now there is IMHO a direct relationship between admins and size of the instance. The more users an instance has, the less the admins are visible in the daily life of "their" users. One could use the quotient of total number of (active) users on an instance divided by the (active) admins as a metric. On my instance that quotient is 2 users and 1 admin, so 2. On a big instance this can be something like 50.000 users on 12 admins, so 4.166. This number can be seen as distance between users and admin. The lower the distance, the more trust I would have in that instance. 

But regardless. The web of trust for me at least starts with the admin(s). They ultimately run the instance and approve new users, either through a priori decision making (when approval is activated) or by a posteriori decision making (open registration) when the admin(s) feel something is wrong, either through reports or other means.

Second. In a federated world, the main mean of limitation of interaction is embedded in the protocol itself. What the protocol allows, WILL be used. By some or by many. With good or bad intentions. Everything on top of the protocol itself is negotiable. That's a simple truth that has big ramifications. That's why protocol design is more of an art and less of a craft.

ActivityPub didn't come out of nowhere. It is based on years and years of experience, iterating on what works and what doesn't.  (this thread will not be finished soon.

This second thing is also often ignored. Give too much freedom in a protocol and things WILL be abused. Limit it too much and the protocol will not gain traction. The working balance can, IMHO, not be anticipated. That's why protocols need to adapt and be modified, even in breaking ways, when needed.

That is one of the things I am missing in ActivityPub but there is hope that that will change and the evolution of AP will continue.

I would even go as far as saying that the lack of evolution of ActivityPub has already caused problems. Example: using the "subject" qualifier for CW (Content Warning) by Mastodon (who may not have introduced this, but definitely popularised it). I am NOT saying this is right or wrong, but it definitely could cause interoperability problems and would be better solved in the protocol itself. This is just an example to support my argument on evolution of a protocol.

OK. I hope I could set the stage by sharing some of my main thoughts on federated systems. Now let's move to the problem I started the thread with. How can we make the fediverse both insanely attractive and abuse-resistant? The current spam wave is IMHO NOT caused by a fundamental mistake on the protocol level. ActivityPub, the protocol, does neither demand open registration or exclusive solutions. So this problem is on the implementation level, combined with the grown culture of the fediverse.

And that's where I come back to my first point. The role of instance admins. This spam wave has uncovered an ugly truth. Many small instances out there have open registration and a high latency on admins doing their admin tasks on their instance. Simply blocking all those instances attacks the core philosophy of the fediverse, IMHO, where I **want** to see many small instances instead of an oligopoly of a few big ones. This tendency towards big instances is, again, not a new thing.

The Mastodon Devs are working on mitigating this problem. The new default when installing a Mastodon instance will be to have new users needing admin approval. And when admins fail to log in for more than 7 days, open registration will automagically be reduced to admin approval. While these are useful mitigations, it will not solve the problem on the fediverse level, IMHO.

[To Be Continued]
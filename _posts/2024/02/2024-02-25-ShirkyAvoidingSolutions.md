---
title: On actively avoiding solutions to keep problems alive
categories:
- Personal
tags:
- CoP
- Philosophy
- 
published: true
header:
  image: mh/mh003.jpg
comments:
  host: social.wildeboer.net
  username: jwildeboer
  id: 111992811885666266 
---

**TIL that this observation I make over and over again actually has a name.**

**Observation:** Some people are so defined/obsessed by The Problem that any solution proposal to The Problem is (un)consciously treated as a personal attack, leading to a fight to defend and preserve The Problem instead of working towards a solution. And it is called the Shirky principle, named after Clay Shirky: [https://effectiviology.com/shirky-principle/](https://effectiviology.com/shirky-principle/)

I'll add it to my list of guiding principles, just like [Conway's Law ("The outcome you produce looks like your organization")](https://en.wikipedia.org/wiki/Conway's_law), [Amdahl's law ("The more you need consensus, the less work you can do")](https://en.wikipedia.org/wiki/Amdahl%27s_law), [Zipf's law ("20% of any system always has 80% of the power")](https://en.wikipedia.org/wiki/Zipf%27s_law) and a few more.

(And yes, I have some very specific examples of problems that are being kept protected from any solution proposal in mind, but as I know from experience that naming them attracts exactly these obsessed people that will claim the weirdest arguments and counterarguments to keep their beloved problem alive, I flat-out refuse to go there. I am happy to discuss this in person, but not on a public platform. I've learned my lessons over many years.)

And while I come to the same observation as Clay Shirky did, there is a big difference. Shirky argues from an organisation/public agency/corporate perspective, I come from the community world of Free Software and Open Source, combined with political experience, mostly in the more leftish part of that galaxy. But also from discussions on Open Standards. Hell yeah. A lot of that is happening in that world ...

The result of this principle/observation is quite depressing. I call it negative CoPs (Communities of Practice). These communities simply fear deep down that their "wonderful" community and the sense of worth they get out of their work will simply disappear once they accept a solution to The Problem. But that should always be the main motivation, IMHO. Solve problems.

And that is what a positive Community of Practice does. Collect positive energy through solving problems. This is always my first question when I think of doing or sharing something with other people. Am I looking at a positive or a negative Community of Practice? Guess which one I will join/help foster and which one I will loudly ignore? ;)

(Communities of Practice is BTW an interesting field of research, for a basic introduction to the field see for example [https://en.wikipedia.org/wiki/Community\_of\_practice](https://en.wikipedia.org/wiki/Community_of_practice) There is also a wonderful book created om these ideas and principles by my friends at [https://www.theopensourceway.org](https://www.theopensourceway.org) and for a more realistic perspective on the practice you should read Social Architecture by the late Pieter Hintjens [https://hintjens.gitbooks.io/social-architecture/content/preface.html](https://hintjens.gitbooks.io/social-architecture/content/preface.html) also as printed book at the usual suspects) 
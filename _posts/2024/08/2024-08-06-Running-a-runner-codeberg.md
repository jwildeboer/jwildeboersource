---
title: Running a runner for codeberg/forgejo on RHEL9 as user
modified: 2025-02-21
categories:
  - Blog
  - HOWTO
tags:
  - codeberg
  - forgejo
  - CI/CD
  - RHEL
published: true
header:
  image: mh/mh006.jpg
comments:
  host: social.wildeboer.net
  username: jwildeboer
  id: 112915289860473140 
---

As many of you know, I have moved almost all my repositories from [Github](https://github.com/jwildeboer) to [Codeberg](https://codeberg.org/jwildeboer) a while ago. Now I want to take it to the next level: Build my own little CI/CD engine!

Now, codeberg offers CI/CD with their [Woodpecker](https://woodpecker-ci.org) instance at [ci.codeberg.org](https://ci.codeberg.org), which I used in the past for example to [build this blog](https://codeberg.org/jwildeboer/jwildeboersource/src/branch/master/.woodpecker.yml) but as codeberg is based on [forgejo](https://forgejo.org), you can have an even more interesting way to build your stuff — [Forgejo Actions](https://forgejo.org/docs/latest/user/actions/). So let's explore that road!

## What do we need to use Forgejo Actions?

Ultimately, Actions works in a quite simple way (if you know Github Actions, you'll feel quite at hime). You create a [runner](https://code.forgejo.org/forgejo/runner/releases) on your build machine. This runner registers itself to forgejo (or codeberg) and will wait for forgejo (or codeberg) to give it something to do.

Here's my approach, that we will set up in this blog post:

- On a RHEL9 (Red Hat Enterprise Linux) server, we will setup `podman` so we can run containers
- We will install the `forgejo-runner`
- We will create a user called `actions`.
- This user will run the runner as a user service with systemd.
- The runner will be registered to codeberg
- We will create a sample workflow in a codeberg repo and see what happens :)

So, let's dive into all of this, step by step. Follow along!

## Prepare the RHEL machine

We need to make sure that we can run containers on our RHEL9 machine, so we do a

```console
# dnf install podman
# dnf install container-tools
```
Next we want to get the `forgejo-runner` binary, which is really simple as the wonderful [ne0l](https://copr.fedorainfracloud.org/coprs/ne0l/forgejo/) has created the software package for RHEL9.

```console
# dnf copr enable ne0l/forgejo 
# dnf install forgejo-runner
```

## Create the user actions

We want to run everything under a user, not as `root`, because it's 2024 and we simply prefer it that way :)

```console
# useradd actions
# loginctl enable-linger actions
```

This way we have a user that can start services without being logged in. The user also doesn't need a password as we will never login as that user.

Now we become that user to make sure the user can work with containers:

As we work with podman, we need to first add this snippet to the `.bashrc` file in the users home directory:

```bash
if [ -z "${XDG_RUNTIME_DIR}" ]; then
  XDG_RUNTIME_DIR=/run/user/$(id -u)
  export XDG_RUNTIME_DIR
fi
```

To do that, we become the user, edit the `.bashrc` file and reload the changed settings.

```console
# su - actions
$ vi .bashrc
  <Copy/paste the snippet and save the file>
$ source .bashrc
$ systemctl --user start  podman.socket
$ systemctl --user enable podman.socket
$ mkdir cbrunner
```

## Setup the runner

Back to being root. We now need to configure the basic configuration for the runner and the systemd service that we will use to, well, run the runner.

First, we create the config file for `forgejo-runner`:

```console
# cp /etc/forgejo-runner/config.dist.yml /etc/forgejo-runner/config.yml
```

And edit it. I have slowed down the `fetch_interval` to 20s, activated the cache and added some basic container images that we will offer to codeberg. You only see the **changed** lines of the config file, not the whole thing!

**Important:** Should you have problems with getting checkout of the repo working, try changing the `network: ""` setting to `network: "host"`. That fixed the problem in my setup.

```yaml
[...]
  # The interval for fetching the job from the Forgejo instance.
  fetch_interval: 20s
[...]
  # The labels of a runner are used to determine which jobs the runner can run, and how to run them.
  # Like: ["macos-arm64:host", "ubuntu-latest:docker://node:20-bookworm", "ubuntu-22.04:docker://node:20-bookworm"]
  # If it's empty when registering, it will ask for inputting labels.
  # If it's empty when execute `deamon`, will use labels in `.runner` file.
  labels: [
    "rhel-9-ubi:docker://registry.access.redhat.com/ubi9/ubi",
    "podman:docker://quay.io/rockylinux/rockylinux:9",
    "ubuntu-latest:docker://ghcr.io/catthehacker/ubuntu:act-latest",
    "act-runner:docker://node:20-bullseye",
    "centos-stream-9:docker://quay.io/centos/centos:stream9"
  ]
[...]
  # Enable cache server to use actions/cache.
  enabled: true
[...]
  # Specifies the network to which the container will connect.
  # Could be host, bridge or the name of a custom network.
  # If it's empty, create a network automatically.
  network: ""
```

Now let's create a systemd unit! As we will run this as user `actions`and not as root, we need to store the config in `/etc/systemd/user/forgejo-runner.service` and it's contents are:

```systemd
[Unit]
Description=Forgejo Runner

[Service]
Type=simple
WorkingDirectory=%h/cbrunner
Environment=XDG_RUNTIME_DIR=/run/user/%U
Environment=DOCKER_HOST=unix:///run/user/%U/podman/podman.sock
ExecStart=/usr/bin/forgejo-runner --config /etc/forgejo-runner/config.yml daemon
RestartSec=20
Restart=on-failure

[Install]
WantedBy=default.target
```

As you can see, not that complicated, but some little explanations are needed, I guess. As we run the service as user `actions`, we will use the `cbrunner` directory we created above, which resides in the user's home directory. The `WorkingDir` entry makes sure that systemd knows where to look. The two `Environment` entries make sure that `forgejo-runner` finds the podman socket it needs.

## Setting up and registering the runner with codeberg

We are getting to the final stages of this process. We need to tell codeberg that our runner exists and is ready to accept workflows. For this we need a token from codeberg.

So on codeberg, go to your settings then actions, click on runners and create a new token:

![Create new runner token](/images/2024/08/CreateToken.png "Creating a new token for a runner in the codeberg settings page")
*Create the runner token*

Now you will only get to see the token once, so copy/paste it and use it immediately to register the runner.

```console
# su - actions
$ cd cbrunner
$ forgejo-runner register --no-interactive --name jhwrunner \
  --token <TOKEN>  --config /etc/forgejo-runner/config.yml \
  --instance "https://codeberg.org"
```

This should result in the runner getting registered at codeberg and a `.runner` file in the `/home/actions/cbrunner` directory. Wonderful!

```
INFO Registering runner, arch=amd64, os=linux, version=v6.2.2. 
WARN Runner in user-mode.                         
DEBU Successfully pinged the Forgejo instance server 
INFO Runner registered successfully.
```
Also, we get the nice confirmation that, indeed, we are running the runner as a user service. Nice.

## Running the runner with systemd as a user service

We are almost done! Let's finally activate the service we have prepared, enable it, so it comes up after a reboot and we can start running workflows.

```console
# su - actions
$ systemctl --user enable forgejo-runner
$ systemctl --user start forgejo-runner
$ journalctl -f --user-unit forgejo-runner.service
```

Over at codeberg, we should now see the runner in state `idle`, ready to run workflows!

![The running runner](/images/2024/08/RunningRunner.png "The runner is running and in idle state")
*The runner is in idle state, ready to run workflows*

## Test the runner

We can now take a repository, go to its Settings, Repository units, Overview and enable actions:

![Repository Settings](/images/2024/08/EnableActions.png "Codeberg repository settings with actions enabled")
*Actions enabled in repository settings*

The final thing left to do is to add a workflow file to the `.forgejo/workflows/` directory of the repo. [This](https://codeberg.org/jwildeboer/febaex.eu/src/branch/main/.forgejo/workflows/test.yaml) is a simple one that acts on every **push** action and does a checkout of the code, lists the directory content and ends after that. It is a helpful workflow, as it tells me if the connection works in both directions and if I can checkout the code over the network to the build machine.

**CAVEAT** This is how I found out the hard way that Actions can only work with git repos that use the **SHA1** object format. If your repo uses the **SHA256** object format, the workflow will fail. I [opened a bug ](https://github.com/actions/checkout/issues/1843) at upstream, but I don't expect a fix coming down soon. So be warned!

```yaml
name: Codeberg Runner
run-name: ${{ gitea.actor }} is testing out Forgejo Actions on codeberg
on: [push]

jobs:
  Explore-Forgejo-Actions:
    runs-on: act-runner
    steps:
      - run: git --version
      - run: echo "The job was automatically triggered by a ${{ gitea.event_name }} event."
      - run: echo "This job is now running on a ${{ runner.os }} server hosted by Forgejo!"
      - run: echo "The name of your branch is ${{ gitea.ref }} and your repository is ${{ gitea.repository }}."
      - name: Check out repository code
        uses: actions/checkout@v4
      - run: echo "The ${{ gitea.repository }} repository has been cloned to the runner."
      - run: echo "The workflow is now ready to test your code on the runner."
      - run: sleep 20
      - name: List files in the repository
        run: |
          ls -lisa ${{ gitea.workspace }}                    
      - run: echo "This job's status is ${{ job.status }}."
```

Notice the `runs-on: act-runner`. That is the **label** that tells codeberg to find a runner that offers that label. If codeberg finds a runner that offers exactly that label, it will be sending the workflow there. Our runner offers that label, so it gets the job.

As soon as you commit this file (I called it `test.yaml`), you should see things starting to run under the actions tab of your repo. Remember that `journalctl -f` that we started on our build machine? That should also start producing lots of log lines :)

![A workflow at work](/images/2024/08/Workflow.png "The workflow at work, as seen by codeberg")
*The workflow being run by the runner*

## DONE

Alright. We have the runner running and can now dive really deep into what [forgejo actions](https://forgejo.org/docs/latest/user/actions/) can do. But not in this blog post. It's too long already!

## Shoutouts and references

* [François Kooman's Howto for Fedora](https://code.forgejo.org/forgejo/runner/src/branch/main/scripts/systemd.md)
* [ne0l](https://copr.fedorainfracloud.org/coprs/ne0l/forgejo/)'s copr with the forgejo-runner binary and more info
* [Forgejo Actions User Guide](https://forgejo.org/docs/latest/user/actions/)
* [Forgejo Actions Admin Guide](https://forgejo.org/docs/latest/admin/actions/)

Feel free to join the discussion in the comments section, that uses Mastodon!
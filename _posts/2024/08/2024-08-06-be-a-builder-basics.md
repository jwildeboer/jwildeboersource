---
title: Be a Builder of Decentralisation - codeberg, forgejo, runners
categories:
  - Blog
tags:
  - codeberg
  - forgejo
  - CI/CD
  - Decentralisation
  - FOSS
published: false
header:
  image: mh/mh003.jpg
#comments:
#  host: social.wildeboer.net
#  username: jwildeboer
#  id: 112915289860473140 
---

First: there is no dispute from my side — [Github](https://www.github.com) fundamentally changed how Open Source is developed and distributed. In good and not so good ways. But the one thing that always bothered me: it centralised what was meant to be decentralised — git repositories.

It did so by adding clever features as value-add that came with a bit of lock-in: you need a github account to open issues, participate in discussions, send pull requests. This all happened before, BTW. It was called [sourceforge](https://sourceforge.net).

I'll leave it at that, as the point I want to discuss today is a different thing, but it needed that bit of context. So. How can we bring more decentralisation to the table? And what does that have to do with this weird thing called [runners]({% post_url /2024/08/2024-08-06-Running-a-runner-codeberg %})?

## Can we do better?

That is the main question. And to explain how I think about all of this, let's reduce the perspective to users and developers of FOSS (Free and Open Source Software). There are a lot of other fields where more decentralisation is needed, but I'll leave that to other blogposts.

### Communication defines everything

That is a general rule, IMHO (In My Humble Opinion). Communication can happen between human beings, but also between machines and (more interstingly for this discussion) — both. So let's break it down for communication in FOSS development.

* People write code
* People put that code in a git repository
* Other people (and machines) work with said code
* Users download the code (or relases) and run the software
* Other people find problems, write patches, new features
* Users download the code (or relases) and run the software
* All of them discuss about new features and other development or project related topics (and yes, they sometimes go very off-topic, but let's ignore that for the moment, just as I do in real life :)

That's the human communication part. But you already see - there is also quite some human to machine and machine to human communication involved. And, in the background, a lot of machine to machine communication.

## My theory: all of that can and should be decentralised.

BAM. That's the whole story. **Decentralise** everything. Github, just like Sourceforge before, tried to do the opposite: **centralise** it all. If we want to decentralise it all, we must make sure that using all of that still is intuitive and easy. It can (and will) be a bit more complex than just using a cehtralised service, but we can make it easier and easier along the way. So let's take it step by step and asnwer the question: How to do this decentralised?

### People write code

Well, here the answer is quite easy. That is almost by definition decentralised. People write code. On their own. At home, on the road, in an office, in a notebook or just in their imagination. Decentralised by default. Let's move on.

### People put that code in a git repository

That repository is (most of the time) quite close the people that write the code. I keep my repos on my laptop, the same machine I use to write this blogpost. My blog actually *is* a [repo](https://codeberg.org/jwildeboer/jwildeboersource). So the answer here is again: Decentralised by default. Next!

Or is it? What typically happens is that your local repo has a [remote](https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes) that you sync with. That remote often is, you guessed it, a Github repo. So. Not really decentralised.

But that is more out of convenience, AFAICS (As Far As I Can See). Any git repo can have **multiple** remotes to which you can push and from which you can pull. It's a bit complex to manage that, so all too often any local repo only has one remote. A bit of a shame, but as I said, decentralistion isn't always easy.

### Other people (and machines) work with said code

And that (centralised) remote is where the action often happens. That's where users and devlopers fetch their updates from. That's where not only the sources but also the releases are made available to the rest of the world. So a typical (developer) workflow is that you clone that repo on your machine, do your work and commit your changes back (let's not make the picture too complicated by discussing branches etc). It's also often the one place you send Pull Requests to. Where issues are managed.

Machines are working in the background to react to changes in the repo. They can fire workflows that compile the code, run tests etc. 
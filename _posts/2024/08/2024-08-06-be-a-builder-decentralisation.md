---
title: Be a Builder of Decentralisation - codeberg, forgejo, runners
categories:
  - Blog
tags:
  - codeberg
  - forgejo
  - CI/CD
  - Decentralisation
  - FOSS
published: true
header:
  image: mh/mh003.jpg
#comments:
#  host: social.wildeboer.net
#  username: jwildeboer
#  id: 112915289860473140 
---

First: there is no dispute from my side — [Github](https://www.github.com) fundamentally changed how Open Source is developed and distributed. In good and not so good ways. But the one thing that always bothered me: it centralised what was meant to be decentralised — git repositories.

It did so by adding clever features as value-add that came with a bit of lock-in: you need a github account to open issues, participate in discussions, send pull requests. This all happened before, BTW. It was called [sourceforge](https://sourceforge.net).

I'll leave it at that, as the point I want to discuss today is a different thing, but it needed that bit of context. So. How can we bring more decentralisation to the table? And what does that have to do with this weird thing called [runners]({% post_url /2024/08/2024-08-06-Running-a-runner-codeberg %})?

## Can we do better?

That is the main question. And to explain how I think about all of this, let's reduce the perspective to users and developers of FOSS (Free and Open Source Software). There are a lot of other fields where more decentralisation is needed, but I'll leave that to other blogposts.

### Communication defines everything

That is a general rule, IMHO (In My Humble Opinion). Communication can happen between human beings, but also between machines and (more interstingly for this discussion) — both. So let's break it down for communication in FOSS development.

* People write code
* **People put that code in a git repository**
* **Other people (and machines) work with said code**
* **Users download the code (or relases)** and run the software
* Other people find problems, write patches, new features **and send them to the repo**
* **All of them discuss about new features and other development or project related topics (and yes, they sometimes go very off-topic, but let's ignore that for the moment, just as I do in real life :)**

Everything that is bold typically happens centralised on Github. Damn :)

## Again, can we do better?

Yes! We can. The functions that made Github big have been implemented by others. [Gitlab](https://www.gitlab.com), [Codeberg](https://codeberg.org), [Forgejo](https://forgejo.org), [Gitea](https://about.gitea.com), [SourceHut](https://sourcehut.org) and many, many more projects. So we can be more decentralised. But here's another little problem: While these services exist (and run well, trust me, I am mainly on codeberg and am running gitlab instances at work and forgejo instances at home for my own stuff) they still tend to keep all these functions centralised at one place. So defintely better, but not yet perfect.

## Github Actions

Back in 2018, Github added another cool, new feature: [Actions](https://techcrunch.com/2018/10/16/github-launches-actions-its-workflow-automation-tool/). Actions are a way to "do stuff when stuff happens". It's mostly (still) seen as a lightway CI/CD thing, but it was designed to be more universal. And it worked quite well, though in th ebeginning it only ran workflows on Githubs infrastructure.

## Runners

Now we come to the fun part — runners. Compiling, testing software and building releases is a complex process. It used to be completely (and in quite some cases still is) manual. This was not good. So quite some years ago automation was introduced. Continuous Intergration (CI), Continuous Delivery (CD). Let machines handle these tasks. When you commit code (and in other cases), the machines take over. They fetch the code, do as they are told (compile stuff, run tests, collect results, announce success or failure, rinse and repeat). And then containers came. Which made that whole process even easier and more reproducible. Github saw that. So they created Github Actions. And Githubs customers (not all of them are Open Source friendly) wanted to run these tasks on their own machines, not just on Githubs infrastructure. So Runners were created and Github saw they were good ;)

A runner is a service that runs on a (bunch of) (virtual) machines and receives a workflow from Github Actions, a recipe on what to do in which order, following the Github Actions syntax and "standard". In simple cases it runs a container that does stuff, in complex cases it runs many containers and/or scripts on various architectures, in order or parallel, doing a lot of different things.

What counts is the abstraction layer introducedby Actions. And Github made Actions Open Source. That's big. And deserves kudos.

So forgejo, codeberg, gitlab et al started implementing similar features and they reused (or simply mirrored) the github reference implementation.

So, inadvertently, Github Actions and runners created a kind of universal language and toolchain to automate the shit out of software development, combining a lot of existing technologies in a very flexible way.

So flexible, that I am now running my own runners on my own server. One for codeberg, one for my forgejo instance.

## Decentralisation with runners

And that's where it hit me. We could have a shared pool of runners all over the internet. Run by individuals. Companies. Hardware manufacturers. Cloud providers.

Imagine this: you are a hardware company that just started producing a new architecture and you want to make sure that as many software as possible runs on it. You offer runners to projects. That will try to run their workflows. Ideally, they just work and you can add a new entry to your "supported software" list. Just as the upstream project can add your architecture to their "supported environment" list. Win-win! If it fails, you can inform the upstream project and offer direct help in fixing the issues. The upstream project can also chip in, if they are interested. Maybe send them some dev boards? Developers **love** toying with new gadgets (well, most of them, me included :). So: offer runners!

But also: You, as a company, have spare capacity and would like to "donate" that to Open Source projects? Maybe just to see how well your infrastructure scales? Offer runners!

You are a company offering hardware architectures that are very expensive, so almost no FOSS project can afford to have one in their homelab to test on? Say, mainframes? Offer runners!

You get the idea. Runners could create more communication, more supported solutiuons, identify more bugs, make things better.

And, almost as a happy little accident, create more decentralisation.

## Conclusion

Runners are a defacto Open Standard to do a lot of things in a lot of strange places. And I think we should explore the possibilities. Right now! Happy hacking!
---
title: Vacuum Robot Microfiction
modified: 2024-10-06
categories:
  - Personal
tags:
  - MicroFiction
published: true
header:
  image: mh/mh004.jpg
comments:
  host: social.wildeboer.net
  username: jwildeboer
  id: 112853538052191793
---

“So wait. You got access to the camera streams of vacuum robots?”

“Sure have”

“and you’ve coupled that with some AI thing that recognises objects and knows for how much those things sell on eBay?”

“Yep. And it all runs directly on the vacuum robot itself”

“and you sell that intel to thieves that go there and steal those things?”

“Yep, with maps and data on when the houses will be empty. They love it. And I get 10% of the eBay total”

“you could go legit and sell this to insurance companies!”

“Or combine both things — maybe I already have, my friend”

We ordered another round of beer and drank it in silence. Nothing left to say.

[#MicroFiction](https://social.wildeboer.net/tags/MicroFiction)

**2024-10-06 UPDATE** When my fiction turns into reality ...

[ABC News: Insecure Deebot robot vacuums collect photos and audio to train AI](https://www.abc.net.au/news/2024-10-05/robot-vacuum-deebot-ecovacs-photos-ai/104416632)

![Insecure Deebot robot vacuums collect photos and audio to train Al
By Julian Fell](/images/2024/07/VacAI.png)

> Ecovacs robot vacuums, which have been found to suffer from critical cybersecurity flaws, are collecting photos, videos and voice recordings – taken inside customers' houses – to train the company's AI models.
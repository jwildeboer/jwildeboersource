---
title: On Field Of Use In Free and Open Source Software
categories:
  - Personal
tags:
  - Opinion
  - FOSS
  - Licenses
published: true
header:
  image: mh/mh010.jpg
comments:
  host: social.wildeboer.net
  username: jwildeboer
  id: 113561257665840035 
---

*You are working since many years on an open source project called **SaveTheKittens**. Your code does what it should and thousands of cute kittens have been saved. Another open source project called **StealTheKittens** starts using parts of your code, because it's in the same field of use, though their goal is the exact opposite of what you want to do. But also, they have some code that you could really use in your project.*

(This post was originally a thread on Mastodon, which you can read [here](https://social.wildeboer.net/@jwildeboer/113556681306551128))

After a very tough discussion last week, I want to explain my position on a fundamental rule of FOSS (Free and/or Open Source Software). It is called the **"no field of use restriction"** rule. What it means is that when you publish code under an accepted Free Software or OSI approved Open Source license, every downstream recipient is free to use the code for whatever they want to use it for. It is a very important but often attacked rule.

If you would restrict the use of your code, you make a moral decision. But licenses are in the opinion of both the FSF and the OSI not the best place to codify that choice. Licenses are defined and limited by the laws and rules on copyright/droit d'auteur. Moral choices are simply out of scope for that regime. Like it or not, it's a fact, IMHO.

The Free Software Foundation explains it [this way](https://www.gnu.org/philosophy/free-sw.html#run-the-program):

> **The freedom to run the program as you wish** [...]
In this freedom, it is the user's purpose that matters, not the developer's purpose; you as a user are free to run the program for your purposes, and if you distribute it to other people, they are then free to run it for their purposes, but you are not entitled to impose your purposes on them.

Here is the wording used by the [Open Source Initiative](https://opensource.org/osd):

> **6. No Discrimination Against Fields of Endeavor**
The license must not restrict anyone from making use of the program in a specific field of endeavor. For example, it may not restrict the program from being used in a business, or from being used for genetic research.

Software is universal. Your solution to recognising objects in a video stream with low latency can be used, as you intended, to help with sorting good from bad vegetables. Or it could be used to identify targets by a missile flying at high speed. These are extreme examples of Field of Use. The question becomes: Should a license allow the one but not the other? Of course most of us want that to be possible. 

## But.

Is the code really open and free if you make that kind of restriction? Both the FSF and OSI, after long and emotional discussions, decided no. That decision cannot be made on the copyright level that licenses are based upon. It was a tough decision.

But it was a wise decision, IMHO. Think of the faux open licenses that exist out there. Licenses that give you access to the source code but tell you that you cannot use it to build a service competing with the project owners. That's the other side of the choice. If use gets limited, Open looses. So field of use restrictions will always lead to less code being available to be re-used. That's why the "no field of use restriction" rule exists.

It is a high price to pay. And if you think it was the wrong choice, I totally understand. But in the bigger picture, abuse of code must be limited by different means, outside of copyright laws that licenses are built upon. You can always refuse to support use of your code in fields you do not want to support. But limiting access to the code should not happen. You share, you care. But your care has limits. That is YOUR decision.

## So.

To sum it up. Licenses are defined and limited by laws on intellectual property. Moral decision on who you accept and support in your community are a very different thing. Never conflate the two, if you want to be a good FOSS citizen. If you feel your code gets used in ways you cannot accept, exclude those that do it from your community. Let them fork, let them find their own community. It's the price you have to pay. Just be clear and open to your people on that.

Books can (and should) be written about this dilemma. My interpretation is very clear and a working solution. My code is free to all. My support is limited. If you want to use my code to do something I deem to be unacceptable, I will never stop you. But I will be free to ignore your bug reports and pull requests.

I know my position is not accepted by all. Again, I respect that. I came to my conclusion based on many years in FOSS. I know it is a tough decision. But I prefer to be honest, put on my flamewar vest and deal with the attacks. What I really want is for you to think. Come to your own conclusion. And stand by it. Is all.

## TL;DR

Free and Open Source Software wants to spread code. They want code to be used and re-used. As a developer, you want to do good. But part of the deal is that you should not limit the field of use of your code, even if it really hurts. Licenses are not the right place to codify moral decisions on the limits of use of your code. That is better done on the community level.

## Ethical licenses

Neil Brown wrote a [good blogpost](https://decoded.legal/blog/2024/02/an_introduction_to_ethical_licensing/) on a slightly different way to look at this complex field by going through the topic of so-called ethical licenses, that try to put moral decisions in license texts.
---
title: My Digital Life After Twitter (G+, Facebook, Instagram etc)
tags:
  - Bluesky
  - ATProto
  - ActivityPub
  - ActivityStream
  - Decentralised
published: true
header:
  image: mh/mh014.jpg
comments:
  host: social.wildeboer.net
  username: jwildeboer
  id: 113504285308484716
---

*FTR (For The Record): I left Facebook 10+ years ago. I left Instagram 3 years ago. I left Twitter almost 2 years ago. And I always had accounts in what is now known as the Fediverse. I was on identi.ca, status.net, pump.io and since 2018 I have my own Mastodon instance which is my main presence in the social network arena.*

**Why am I on the Fediverse and not on Bluesky or Threads?**

Because on the Fediverse I am on [my own Mastodon instance](https://social.wildeboer.net), hosted in the EU by the wonderful [masto.host](https://masto.host), and not on a centralised service from the US (which both [Bluesky](https://bsky.app) and [Threads](https://threads.net) are), where soon a Trump government is installed that can easily force both Bluesky and Threads to hand over full access to all my data (I am quite sure Musk is already offering the X/Twitter social graph to Trump). That's my personal risk calculation. Yours might be very different. And that's perfectly fine!

Another reason for staying on the Fediverse is (the lack of) metadata. Trust me, not many organisations out there really care about the content of your posts. They focus far more on the connections and frequency of data on likes, boost, follow/unfollow etc.

In a centralised service that metadata is, uhm, centralised too and easy to access and share. In a federated network things aren't that easy, especially when, like me, you live on your own instance and not on a big one like mastodon.social.

> Decentralisation makes abuse expensive as the cost for building a complete social graph across all Fediverse instances is exponentially higher.

And yes, I still treat every single post and message on the Fediverse as ultimately being stored and analysed by adversaries because for me the Pub in ActivityPub means public.

My private communication happens via Signal or old fashioned pen and paper. Or, even more often, by direct communication over a coffee, beer or a walk outside :)

## Some more thoughts on Bluesky

**IMHO: [#BlueSky](https://social.wildeboer.net/tags/BlueSky) isn't decentralised or federated. Not yet. Not in the near future. And most likely it will never be.**

The [outage on 2024-11-14](https://www.bbc.com/news/articles/c04l15lndrpo) is obvious proof. They may try hard to make it *seem* to be decentralised and they definitely love to outsource traffic/storage costs by claiming that running your own PDS (Personal Data Server) or Relay is possible and just that somehow makes it federated, but that's all smoke and mirrors.

You have to dive a bit deeper into their [architecture](https://bsky.social/about/blog/5-5-2023-federation-architecture) to find things like "networking through Relays instead of server-to-server" as their current implementation choice. This simply means that THEY run the main services you need to connect to. No one else.

It's well hidden, but in Bluesky the Relay system, AppView and the PLC identity layer PLC are centralised parts.

The posts you see on Bluesky come from the AppView layer. The AppView layer gets its data from the Relay layer. The Relay layer gets its stuff from the PDSes. The Labeler does the moderation. The PLC manages your identity and keys. So you always go through a bunch of their systems. No direct connection from AppView to PDS. Yes, they promise that in future you might be able to run your own relay. And if you are willing to, you can. But that needs a fat internet connection, 4-5TB of storage with a 20G/day growth rate and you'll end up with some 200-300€/month for servers that ar powerful enough to run your relay. So right now and since its inception, Bluesky runs the relay system you connect to. I call that centralised.

![Architecture view of BlueSky. It shows how the Relay sits in-between data storage and app view and thus for a centralised element.](/images/2024/11/bsky-arch.png)
*An incomplete picture of the Bluesky architecture, Source: [Federation Architecture Overview](https://bsky.social/about/blog/5-5-2023-federation-architecture)*

The more I look at [ATproto](https://atproto.com/specs/atp), the "standard" behind Bluesky, the more I see vague promises of full decentralisation and federation that "currently" are implemented in centralised ways, because "right now" they "unfortunately" have to do it that way. No clear roadmap on when these centralised elements will be removed/replaced. Now if a "standard" allows for such fundamental deviations, I cannot take it serious. That's just me, though.

I also fail to see a lot of community uptake on creating alternative implementations of ATproto. Compared to the creative chaos around ActivityPub/ActivityStreams it definitely feels more like a walled garden.  

## TL;DR

So, in conclusion and IMHO, BlueSky is a successful, centralised Twitter alternative.

The Fediverse including Mastodon is not and never aimed to be that. The Fediverse is a fundamentally different and bigger thing, due to its truly federated nature. 

Both can have their place in the market of attention.

One ultimately is a commercial endeavour with a limited scope while the other is aiming bigger. But for the general audience that simply is not the decisive factor. One can find that A Bad Thing, or one can shrug and move on in ones preferred direction :)
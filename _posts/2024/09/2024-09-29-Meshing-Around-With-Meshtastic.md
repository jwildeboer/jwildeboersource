---
# title: 
categories:
  - Personal
tags:
  - TAG
published: false
header:
  image: mh/mh006.jpg
# comments:
#  host: social.wildeboer.net
#  username: jwildeboer
#  id: XXX 
---

So I’ve been experimenting with LoRa and meshtastic. [https://meshtastic.org](https://meshtastic.org) and I have some ideas on how to use it as a neighbourhood sensor and chat network. Inline with my thoughts on decentralising (power) grids. But also for generic neighbourhood chats that by definition stay local. Lots of opportunities to explore. Next: a cheap, solar powered Meshtastic node that you can just throw in trees etc. LoRAnarchy ;)

![A meshtastic node in a 3D printed case ](https://cdn.masto.host/socialwildeboernet/media_attachments/files/113/191/545/559/514/756/original/45192cc9e831f5f8.jpg) ![A meshtastic “phone” with a blackberry keyboard and touchscreen. ](https://cdn.masto.host/socialwildeboernet/media_attachments/files/113/191/545/868/258/785/original/fa3155a28e03dadc.jpg) ![The inside of a meshtastic node with a Heltec V3 board and SMA antenna connector, waiting for a 2000 mAh battery to be added. ](https://cdn.masto.host/socialwildeboernet/media_attachments/files/113/191/547/489/982/355/original/89cbf29a25bdb8e8.jpg)

So instead of positioning LoRa the „traditional“ way — a solution for off-grid, remote areas — I want to explore its potential as a kind of „whisper network“ for neighbourhoods, local communities. In densely populated areas. With the very low bandwidth as a feature, not a bug.

Current cost of a simple node, DIY: Heltec V3 board, ca 20€. Decent antenna, 4€. 2000 mAh battery, 7€. 3D printed case, 1€. Add a solar cell and some charging circuits and we should be at around 50€ per node as artisanal product. A bigger run of, say, 500 nodes could be made at less than 15€ per node, I guess.

With a typical range of 1-2 km per node when positioned outside, you can do the math on how big an area you can cover with a dozen nodes. Hint: quite a lot.

What I’m now trying to source: a simple board like the Heltec V3 but with a nrf52 instead of the ESP32. Uses far less power and the lack of WLAN is actually a Damn Good Thing for a simple node to avoid abuse. The Lilygo TTECHO comes closest, AFAICS. UPDATE: The Heltec T114 seems to be an even better option. Thx [@stickus](https://mstdn.ca/@stickus) for the tip!

Another weird idea: A simple, cheap messenger device, like a pager with a screen and a T9 keyboard. Should attract both the young(er) and older generations :) Working title: The Hood Whisperer. Or the LORIA :) ![A Nokia 3110 phone with a T9 keyboard](https://cdn.masto.host/socialwildeboernet/media_attachments/files/113/191/882/064/360/598/original/c59e886b663654ae.png)

I will need to patch/extend meshtastic though, I guess. Add some sort of challenge/response thingy that allows you to bind a node to you as a person, so you can have encrypted p2p chats. Identity ownership in a mesh network, truly decentralised.

(and to the nay-sayers that are popping up as expected I say: I am free to play around and try to find people that are also interested. You trying to shoot me down at every single step instead of offering help or productive criticisms: that just flows off of me like if I'm plated with teflon ;) [#WBSB](https://social.wildeboer.net/tags/WBSB)

I really like meshtastic because it allows for innovation on top of their base solution. And it is GPLv3 licensed :) [https://github.com/meshtastic](https://github.com/meshtastic)

But first: upgrades :) The two Heltec nodes now have batteries, so I can take them outside! ![One Heltec V3 mesh node with its case opened after I installed a 2000 mAh battery](https://cdn.masto.host/socialwildeboernet/media_attachments/files/113/192/908/437/958/366/original/127800e03e9aef9f.jpg) ![The orange Heltec V3 mesh node running from the new battery](https://cdn.masto.host/socialwildeboernet/media_attachments/files/113/192/908/717/440/841/original/b42886868ba8b3ca.jpg)

And why not? Printed two stands so the nodes can sit around in style :) Big thanks to Alley Cat for sharing her beautiful case and stand at [https://www.printables.com/model/936437-heltec-lora-32-v3-ht-slimpro-cases-by-alley-cat](https://www.printables.com/model/936437-heltec-lora-32-v3-ht-slimpro-cases-by-alley-cat) ![An empty stand and one Heltec node clipped in to a second stand on the print bed of my 3D printer. ](https://cdn.masto.host/socialwildeboernet/media_attachments/files/113/193/483/474/704/857/original/6b1d2f6c06a10fc0.jpg)

Tomorrow I will get a 6W USB-C solar panel so I can put one of my nodes outside on the balcony and hopefully extend its reach with the big antenna that should also arrive tomorrow :) A solar powered meshtastic node with backup battery. Total cost around 50€. ![25 cm antenna for 868 MHz LoRa](https://cdn.masto.host/socialwildeboernet/media_attachments/files/113/194/799/637/263/127/original/1498fbf57167f6f8.png) ![6W solar panel with USB-C ](https://cdn.masto.host/socialwildeboernet/media_attachments/files/113/194/799/754/634/340/original/a1dad02efb264485.png)

After a few days of letting stuff work in the background, adding a better antenna to one node (still working on making it weatherproof so it can be mounted outside) I can see quite a bunch of nodes happily building a mesh. But still no messages being exchanged. Seems meshtastic users don’t like to talk ;) Or I don’t (yet) know which channels I need to configure …

![Node map as seen by my router node, showing a lot of other meshtastic nodes, most of them clustered around the Munich city centre and the following the river Isar. ](https://cdn.masto.host/socialwildeboernet/media_attachments/files/113/214/354/335/410/966/original/2e1868484fd61a0e.jpeg) ![The node list shows 50 known nodes. And I filter on online nodes to get a realistic view. ](https://cdn.masto.host/socialwildeboernet/media_attachments/files/113/214/354/441/924/753/original/e69dc96490304098.jpeg)

So if you see JHW Orange Node e99c (ORA2-e99c) in your Meshtastic node list, feel free to send a direct message :) This is the node I carry around with me wherever I go, so it could show up at a lot of places, far away from [#Munich](https://social.wildeboer.net/tags/Munich)

![Screenshot of the Meshtastic app on my phone, showing the details of my orange node. ](https://cdn.masto.host/socialwildeboernet/media_attachments/files/113/214/535/472/582/822/original/018879f3d7d89dd6.jpeg) ![The orange node. A meshtastic device in orange, with display, battery and antenna. Based on a Heltec V3 board](https://cdn.masto.host/socialwildeboernet/media_attachments/files/113/214/544/537/343/716/original/2a1c755f93df66bc.jpg)

Some thoughts on next little projects:

- add CO2 sensor to node and collect data on air quality.
- add IR sensor for electricity meter and collect data on electricity usage.
- build 10 solar powered nodes and distribute them in the hood for a local mesh network (with support/permission from local authorities)
- stoically do all of that, share experiences, find people that are also interested and ignore the nay-sayers that love to demotivate other people :)

Hm. The 2000 mAh battery is not enough to get the Heltec V3 through the night. So. Let's make it a 10000 mAh battery. But that needs a bigger case. And I really like Alley Cat's case. So after some cutting, copying, pasting I hope I have a solution ;) We will know if it works in 2 hours after the print is done :)

![The Alley Cat Slim Pro case for the Heltec V3. On the left the original design, on the right my modified version that should hold a 10000 mAh battery](https://cdn.masto.host/socialwildeboernet/media_attachments/files/113/220/557/280/267/064/original/5451b5e198b11b1a.png) ![Top view of the print plate with my modified version of the Alley Cat case. Neatly fits on my Bambu Lab A1 mini.](https://cdn.masto.host/socialwildeboernet/media_attachments/files/113/220/561/677/336/653/original/7a83c64103e3e5c9.png)

It's really satisfying to be able to do all of that with a few mouse clicks and some simple calculations. Should this case work, I will upload it to [https://makerworld.com](https://makerworld.com) and tell Alley cat, who might make it far better as she knows her model the best :) I just didn't want to bother her with my extravagant idea :)
---
title: Do The Right Thing
categories:
  - Personal
tags:
  - DoTheRightThing
published: true
header:
  image: mh/mh006.jpg
comments:
  host: social.wildeboer.net
  username: jwildeboer
  id: 112637562266579218
---

I created a (hidden) lost+found page on my blog and am now printing little stickers with the QR code that points to that page.

Gonna put them on all my devices and gizmos and when I lose/forget them someplace, I trust the finder will Do The Right Thing and contact me. (picture does NOT have the real QR Code)

![Do The Right Thing Sticker](/assets/dtrt.png)
*The DTRT sticker (not the real QR code)*


Inspired by [Casey Neistat](https://en.wikipedia.org/wiki/Casey_Neistat), who explained this idea and the underlying optimism that people are good back in 2013 in [this clip](https://www.youtube.com/watch?v=Uan9DWR2n0Q).

I printed the first few on 12 mm tape with my Brother QL-500, which is really small, but they work flawlessly, so I am putting them everywhere now :) Here's one on the back of my remote! (bonus points if you can deduct which remote this is. Hint: older than some of you around here, but still in daily use by me :)

![My remote with the DTRT sticker](/images/2024/06/dtrtremote.jpeg)

(for the real nerds: the sticker was designed with Inkscape, the QR code was generated with Inkscape too. The font used is DIN Alternate Bold. I exported the final design as png in the correct dimensions/resolution to fit on the 12mm label)
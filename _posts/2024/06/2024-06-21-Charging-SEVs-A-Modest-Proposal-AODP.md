---
title: Charging SEVs — A Modest Proposal called AOPD
categories:
  - Personal
tags:
  - Innovation
  - SEV
  - Infrastructure
  - Carless
  - AOPD
published: true
header:
  image: mh/mh002.jpg
comments:
  host: social.wildeboer.net
  username: jwildeboer
  id: 112655662052018459
---
(This blog post is based on a thread I started on [Mastodon](https://social.wildeboer.net/@jwildeboer/112653447416893211))

There is a lot of talk since many years on the charging infrastructure for electric vehicles (EV). But during the last few years, a silent revolution is happening, especially in cities — the rise of SEV (Small Electric Vehicles) like electric bicycles, scooters, mobility solutions for the disabled like electric wheelchairs.

But it seems that when it comes to charging these SEV, nobody really cares. Most SEV come with charger bricks that you either have to carry around in case you need to charge (and good luck finding a socket you can use for that) or when you (like me) commute with an SEV, you get a second charger brick that you take to the workplace. Not an ideal situation.

![My commute SEV](/images/2024/06/Myebike%20.jpeg)
*My electric bicycle that I use to commute*

## Can we do better?

So I did a non-representative check of how SEVs of various kinds are charged. (I simply checked my inventory. Several e-Scooters, my e-bike, electric skateboards ;) Turns out that most of these (modern) SEVs are quite comparable. Typically they get fed something between 36-42V at 2-3A. So we are looking at 70-140W most of the time. Exceptions exist. Especially electric wheelchairs used to come with now rather outdated 6V or 12V lead batteries. But the 36-50V range at, say 150W max seems to be the sweet spot that can charge a lot of SEVs out there.

## What is the ultimate goal here, Jan?

The TL;DR: Create an open standard for charging SEVs in public and private spaces, with a robust and efficient solution, using as many existing standards and components to reduce costs and be able to implement fast.

Create standardised charging units like bike sheds with solar panels as its roof that can collect (and with an optional battery) store electricity off-grid. They could be put up as the new bus stop, offering a place to sit while waiting for the bus/tram and double as free charging station for SEvs.

Bike racks that can be installed at supermarkets or in parks and residential streets. You get the idea.

## Big words. How?

This is where it gets interesting. How about an existing standard that can deliver power from 5 to 48V, with up to 240W, using max 4 pins? With cheap controllers that can negotiate the exact details? That already exists and is widely deployed? So that it can be added as default charging port to new SEVs and with a cheap adapter (cable) can be used with millions of existing SEvs?

## USB PD (Power Delivery) Revision 3.1

It is called [USB-PD](https://www.usb.org/usb-charger-pd) and does all of that. It is quite open (though not completely open source/open hardware) and several implementations of PD negotiation on [cheap STM microcontrollers like Bluepill](https://github.com/manuelbl/usb-pd-arduino) (as Open Source) and dedicated, cheap chips are on the market too. Here's what USB-PD offers:

| Specification | Voltage | Current | Max power |
| :-- | :---: | :---: | :---: |
| Fast Charging | 5V | 2.4A | 12W |
| Quick Charge | 5V, 9V, 12V | 1A, 2A | 27W |
| PD 3.1 SPR | 5V | 3A | 15W |
| PD 3.1 SPR | 9V | 3A | 27W |
| PD 3.1 SPR | 12V | 3A | 36W |
| PD 3.1 SPR | 15V | 3A | 45W |
| PD 3.1 SPR | 20V | 3A | 60W |
| PD 3.1 SPR | 20V | 5A | 100W |
| **PD 3.1 EPR** | 28V | 5A | 140 W |
| **PD 3.1 EPR** | 36V | 5A | 180 W |
| **PD 3.1 EPR** | 48V | 5A | 240 W |

Now here is the fun part: USB-PD is a quite universal protocol that doesn't need a USB(C) connector. To get USB-PD working, you can get along with 3 pins and ground. So you **could** implement PD on much simpler and more robust connectors. Like my personal favourite, the XLR connector. Which also exists in a 4 pin version. XLR is standardised in [IEC 61076-2-103:2004](https://webstore.iec.ch/publication/4420), which unfortunately is your typical, expensive ISO standard. But as we can buy these sockets and plugs everywhere, that's not a big problem. 

![The robust and well-known XLR plug](/images/2024/06/XLR-4.jpg)
*The venerable XLR 4 pin plug with shield/ground*

## Putting It All Together — AOPD

So here is my modest proposal: **AOPD** — Almost Open Power Delivery. Cheap units that can be mass produced and become the universal charger for SEVs. For outdoor/public use with a robust XLR or similar plug (but it MUST be one plug). As a compulsory standard for all SEV manufacturers. So we can put up charger stations off- and on-grid everywhere. And use cheap adapter cables for the millions of SEVs already out there.

## Make AOPD an Open Project and an Open Standard

It's a win-win situation. If the EU, for example, decides to make AODP an EU wide standard which can be done in a short time period, we create a market and an opportunity to make SEVs a working alternative to cars in cities but also beyond. That is my ultimate goal and I hope this short presentation inspires you to think about it and maybe help create the ecosystem that I envision. Free electricity for SEVs all over the world :)

## First Steps: DIY

This whole thing can be kickstarted right now. USB-PD power bricks that implement PD 3.1 are available, [Framework has one](https://frame.work/de/en/products/16-power-adapter?v=FRANCR000F) that does 36V/5A, so 180W and [they say](https://frame.work/de/en/blog/framework-laptop-16-deep-dive---180w-power-adapter) that 48V/5A is just around the corner. Building a simple adapter cable that negotiates the 36-48V at up to 5A for your electric bicycle/scooter can be done with an [arduino](https://github.com/ryan-ma/PD_Micro) or [STM microcontroller](https://github.com/manuelbl/usb-pd-arduino). Building an AOPD charger brick that can be put on a DIN rail is a simple project. So. Let's go!

![A sink board](/images/2024/06/SinkBoard.jpeg)
*A prototype board that uses STM microcontrollers like a Bluepill board to ask for specific voltages/current from a USB-C charger*

## The future

Who knows, maybe in the not-so-far future we can have free electricity for e-bikes, e-scooters and e-wheelchairs with a simple and robust plug, as a side-effect of having more bike racks that allow us to park our small electric vehicles in safer and better ways :)

![Bike stall with solar panels](/images/2024/06/bikeshedsolar.jpg)
*Bike shed with solar panels, source: [FromAtoB](https://fromatob.nl/en/r-net-bicyble-parking-facility/)*
---
id: 1593
title: Edward Snowdens letter to German government/Authorities
date: 2013-11-01T13:33:05+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1593
categories:
  - OpenMisc
---
Edward Snowdens [letter](http://www2.stroebele-online.de/upload/brief_snowden_englisch.pdf) to german government/authorities 

**Background:** German member of parliament, Hans-Christian Ströbele (Greens) met Edward Snowden in Moscow on 2013-10-31. They talked for 3 hours and Mr. Ströbele came back to Germany with this letter.

* * *

&#8220;To whom it may concern,

I have been invited to write to you regarding your investigation of mass surveillance.

I am Edward Joseph Snowden, formerly employed through contracts or direct hire as a technical expert for the United States National Security Agency, Central Intelligence Agency, and Defense Intelligence Agency.

In the course of my service to these organizations, I believe I witnessed systemic violations of law by my government that created a moral duty to act. As a result of reporting these concerns, I have faced a severe and sustained campaign of persecution that forced me from my family and home.I am currently living in exile under a grant of temporary asylum in the Russian Federation in accordance with international law.

I am heartened by the response to my act of political expression, in both the United States and beyond. Citizens around the world as well as high officials &#8211; including in the United States &#8211; have judged the revelation of the unaccountable system of pervasive surveillance to be a public service. These spying revelations have resulted in the proposal of many new laws and policies to address formerly concealed abuse of the public trust. The benefits to society of this growing knowledge are becoming incresingly clear at the same time claimed risks are being shown to have been mitigated.

Though the outcome of my efforts has been demonstrably positive, my government continues to treat dissent as defection, and seeks to criminalize political speech with felony charges that provide no defense. However, speaking the truth is not a crime. I am confident that with the support of teh international community, the goivernment of the United States will abandon this harmful behavior. I hope that when the difficulties of this humanitarian situation have been resolved, I will be able to cooperate in the responsible finding of fact regarding reports in the media, particularly in regard to the truth and authenticity of documents, as appropriate and in accordance with the law.

I look forward to speaking with you in your country when the situation is resolved, and thank you for your efforts in upholding the international laws that protect us all.

With my best regards,

Edward Snowden
  
31 October 2013&#8221;

* * *

---
id: 1577
title: 'That HP/Microsoft &#8220;study&#8221; on the Linux Migration in Munich? Hm.'
date: 2013-01-23T15:09:05+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1577
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
With the help of Google translate and my infinite wisdom I translated the statement of IT@M, the IT service company owned by the city of Munich. DISCLAIMER: This NOT an official translation. This is my personal translation. The original text in german is linked from here.

I published (with permission) the original statement [here](https://jan.wildeboer.net/2013/01/munich-that-hpmicrosoft-study-has-a-few-problems-we-guess/).

* * *

**HP study produced on behalf of Microsoft on the LiMux migration**

(01/22/2013) Under the intriguing title &#8220;[Mayor of Munich] Ude has wasted millions on Linux machine?&#8221; Focus Money Online reported on a study that HP made on behalf of Microsoft. The study allegedly proves that the city didn&#8217;t save in the tens of millions Euro by switching to OpenOffice and LiMux, but actually paid far more.

Karl-Heinz Schneider, head of the municipal IT service IT@M:&#8221;Of course we want to deal with this criticism. I have asked Microsoft to share the study with us. What I could gather so far from press articles however raises a considerable amount of doubt on the validity of the study and its findings.&#8221; The study does not take into account the licensing costs that would be incurred for using Microsoft products. Schneider: &#8220;This simply drops seven million into the void &#8211; which is quite the biggest saving we had.&#8221;

The claim that no new versions of Windows and its application would have been needed is simply not true. Schneider: &#8220;A major trigger for the decision to put our operating system architecture to the test was precisely the announcement by Microsoft to drop support for Windows NT &#8211; the operating system that was used as a standard at the city of Munich at that time. A migration to a new operating system was therefore inevitable. &#8221;

The claim that the city would have compared the cost of a current Windows 7 with a ten year old version of Linux is also simply wrong. Schneider: &#8220;Of course we have been gradually optimizing LiMux over time. The current version is far away from the original version and can stand a comparison with Windows 7.&#8221;

The study also falsely claims that one in four city computers still run on Windows as none of the specialized procedures can be migrated to Linux. Schneider: &#8220;It is true that not all business applications can be migrated to Linux. But that is &#8216;not all&#8217; and not &#8216;none&#8217;. All web-based business applications can be used without any migration costs under LiMux and most of the procedures that are tightly integrated with Microsoft can be accessed with standard technologies that are also used by the Linux client.

Finally the number of remaining Windows machines in Munich that the study claims is too high. Instead of the claimed 75 percent, we have already moved 13,000 of the planned 15,000 machines to LiMux &#8211; that&#8217;s almost 87 percent. &#8221;

* * *

Original german version published in the <a href="http://www.muenchen.info/pia/RSS/015.pdf" target="_blank">2013-01-22 edition of Rathaus Umschau</a> &#8211; Page 8 and 9

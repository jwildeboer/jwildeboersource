---
id: 1569
title: 'Munich: That HP/Microsoft study has a few problems, we guess.'
date: 2013-01-23T13:35:38+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1569
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
**[UPDATE: My english translation of the statement now [here](https://jwildeboer.github.io/2013/01/that-hpmicrosoft-study-on-the-linux-migration-in-munich-hm/)]**

Reproducing the statement of [IT@M](http://www.muenchen.de/rathaus/Stadtverwaltung/Direktorium/IT-Beauftragte/Wir-ueber-Uns/ITatM.html) &#8211; with permission from the press office of the city of Munich.

* * *

**IT@M in eigener Sache**

**HP-Studie untersucht im Auftrag von Microsoft LiMux-Umstellung**

(22.1.2013) Unter dem reißerischen Titel „Hat Ude Millionen für Linux-Rechner verschleudert?“ berichtet Focus Money online über eine Studie, die HP im Auftrag von Microsoft angefertigt haben soll. Die Studie belege angeblich, dass die Stadt durch die Umstellung auf LiMux und OpenOffice keinen zweistelligen Millionenbetrag gespart, sondern sogar draufgezahlt hätte.

Dazu erklärt Karl-Heinz Schneider, Chef des städtischen IT-Dienstleisters IT@M: „Selbstverständlich werden wir uns mit dieser Kritik gerne auseinandersetzen. Ich habe deshalb Microsoft sofort aufgefordert, uns diese <span style="line-height: 1.714285714; font-size: 1rem;">Studie zur Verfügung zu stellen. Was ich bislang der Presse entnehmen konnte, wirft allerdings erhebliche Zweifel an der Aussagekraft der Studie auf.“ So lasse die Studie die Lizenzkosten, die beim Einsatz von Microsoft-Produkten angefallen wären, von vorneherein unberücksichtigt. Schneider: „Damit lässt die Studie den Löwenanteil der Einsparung in Höhe von fast sieben Millionen Euro einfach unter den Tisch fallen.“</span>

Auch die Behauptung, beim Verbleib auf der Windows-Schiene wären überhaupt keine neuen Versionen erforderlich gewesen, trifft nicht zu. Schneider: „Ein wesentlicher Auslöser für die Entscheidung, die BetriebssystemArchitektur auf den Prüfstand zu stellen, war ja gerade die Ankündigung von Microsoft, den Support für das damals als Standard bei der Stadt eingesetzte Windows-NT-Betriebssystem einzustellen. Eine Migration auf ein neues Betriebssystem war also unvermeidlich.“

Unzutreffend ist auch die Behauptung, die Stadt hätte die Kosten einer aktuellen Windows-7- mit einer zehn Jahre alten Linux-Version verglichen. <span style="line-height: 1.714285714; font-size: 1rem;">Schneider: „Selbstverständlich ist der LiMux-Client sukzessive optimiert worden. Die aktuelle Version ist mit dem ursprünglichen Client zu Projektstart nicht mehr zu vergleichen und braucht einen Vergleich mit Windows 7 nicht zu scheuen.“</span>

Falsch ist darüber hinaus die Darstellung der Studiie, jeder vierte StadtRechner laufe noch auf Windows-Basis, da „alle Fachverfahren nicht auf Linux migrierbar“ seien. Schneider: „Richtig ist, dass nicht alle Fachverfahren auf Linux umgestellt werden können. Da wurde offensichtlich aus einem „nicht alle“ ein „alle nicht“ gemacht. Alle web-basierten Fachverfahren können ohne Umstellungsaufwand unter LiMux genutzt werden und die meisten Verfahren, die eng mit Microsoft integriert sind, können über andere Standardtechniken ebenfalls vom Linux-Client aus benutzt werden.

Auch die Zahl der verbleibenden städtischen Windows-Rechner ist zu hoch gegriffen. Statt der in der Studie behaupteten 75 Prozent haben wir bereits jetzt 13.000 der geplanten 15.000 Arbeitsplätze auf LiMux umgestellt – das sind knapp 87 Prozent.“
* * *

Source: <a href="http://www.muenchen.info/pia/RSS/015.pdf" target="_blank">Rathaus Umschau, 2013-01-22</a> &#8211; Page 8 and 9

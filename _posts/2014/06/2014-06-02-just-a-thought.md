---
id: 1612
title: Just a thought
date: 2014-06-02T13:59:21+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1612
categories:
  - OpenMisc
tags:
  - Bugs
  - Security
---
I don&#8217;t fear the bugs that get fixed (in OpenSSL and now GnuTLS) in an open, transparent way we open source people do. I fear the bugs in proprietary stuff where I can never be sure if they get fixed and how. ﻿

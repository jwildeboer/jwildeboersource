---
title: GPG, subkeys, the genius of it!
date: 2014-01-12T13:16:02+00:00
author: jwildeboer
layout: single
categories:
  - Tips & Tricks & Fixes
tags:
  - GPG
  - YubiKey
---

**#geekporn Today it made &#8220;click&#8221; in my head. I finally understood the use of subkeys in GPG. The genius of it!**

You create your GPG-key on a truly safe machine (ideally a machine that has no connection to the network, runs a minimal and trusted OS (Linux, I personally use Fedora) and create subkeys for signing and encryption. You export your key to a safe device (USB Stick, SD card) and store it offline.

Now you can safely delete the master key from your keyring and carry ONLY the subkeys in your keyring on machines or mobile phones that you use to encrypt/decrypt/sign. So should your keys become compromised, you can revoke the subkeys, take out your masterkey again on a truly safe machine and generate a new set of subkeys for encryption and signing.

**NOTE:** You cannot sign other keys with this stripped down keyring as you need the master key for that.

And these new subkeys are still identified via the same keyID as that comes from the masterkey. So you can keep the masterkey longterm and swap out the subkeys every year or whenever you think they are compromised.

[Source](http://www.void.gr/kargig/blog/2013/12/02/creating-a-new-gpg-key-with-subkeys/")
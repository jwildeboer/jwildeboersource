---
title: 'Paper task management - the Corbinizer as A4'
date: 2014-01-28T12:27:05+00:00
author: jwildeboer
layout: single
categories:
  - OpenMisc
tags:
  - TaskManagement
---
I have always been struggling with task management. Always. And will always do. I have tried a lot of things, paper based, digital (starting on my Apple Newton, over to the Palm III, V, various mobile phones, Blackberry and now iPhone and Android.

But somehow I have learned over the years that I need the totally simple stuff or else it will fail.

Seems I am not alone 😉 I stumbled across the [Corbinizer](http://corbinizer.com/ "Corbinizer"), a very simple but nice approach. More explanations [here](http://icorbin.com/the-best-free-task-management-application-ever/1794).

However, the usual Le Sigh after downloading it. It doesn&#8217;t fit on A4, it uses Letter. So I opened my Inkscape, fiddled around a bit and created the A4 version which is attached to this blog entry.

Remember, the Corbinizer is Copyright 2014, All Rights Reserved by Brandon Corbin.
  
The Corbinizer is released under the Creative Commons Attribution-NonCommerceial License.

Let&#8217;s give it a try. Print it at 100% and make sure &#8220;shrink to fit&#8221; is NOT selected.

[CorbinizerA4](/images/2014/01/CorbinizerA4.pdf)

---
id: 1285
title: 'Oracle&#8217;s next target? Power switches #notkidding #irony'
date: 2010-08-17T16:23:16+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1285
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
**THIS BLOG ENTRY IS FULL OF SARCASM AND IRONY. IT IS MY PERSONAL OPINION AND IN NO WAY REFLECTS WHAT MY EMPLOYER THINKS. THOU HAST BEEN WARNED.**

I will not comment on the Oracle v Google case. But I would like to inform the world of a growing risk of using infringing technology that is formerly owned by SUN microsystems. Oracle might be sitting on a fortune here. Take a look at this fine piece of &#8220;intellectual property&#8221;:

[The &#8216;464 Patent](http://patft.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&#038;Sect2=HITOFF&#038;p=1&#038;u=%2Fnetahtml%2FPTO%2Fsearch-bool.html&#038;r=44&#038;f=G&#038;l=50&#038;co1=AND&#038;d=PTXT&#038;s1=%22gosling,+james%22.INNM.&#038;OS=IN/%22gosling,+james%22&#038;RS=IN/%22gosling,+james%22):

**Method and apparatus for providing dynamically configurable electrical switches** 

> A digital wiring configuration comprises a switch control allowing a user to select a function to control a corresponding electrical device. A control unit couples electrical power to the electrical devices through power outlets. The control unit allows an operator to dynamically configure the switch controls to operate electrical devices at specified power outlets. Upon selection of a function on the switch control, the switch control transmits both a switch state, indicative of the function selected, and a switch identification that uniquely identifies that switch control. The control unit receives the switch state and the switch identification and generates a device identification uniquely identifying the power outlet corresponding to the control switch. The control unit transmits the device identification and the switch state to the power outlets. The corresponding power outlet is selected through the device identification and executes the function in accordance with the switch state. 

Let&#8217;s look at the first claim:

> 1. A digital wiring system to control electrical power coupled to a plurality of electrical devices, said digital wiring system comprising:
> 
> a plurality of switch control means for controlling said electrical devices, each one of said switch control means comprising a switch for selecting among a plurality of switch states, and a transmitter means for transmitting a switch command, said switch command uniquely identifying said selected switch state and said switch control means;
> 
> control unit means for generating a device command in response to said switch command, said control unit means comprising:
> 
> receiver means for receiving said switch command transmitted from said switch control means,
> 
> map means coupled to said receiver means for generating a device identification, said map means specifying those electrical devices which are controlled by each one of said switch control means, and said map means generating said device identification so as to identify each electrical device that is controlled, according to said specification, by the switch control means identified in said switch command,
> 
> operator interface means not restricted by predetermined selection rules for dynamically configuring said map means in response to an operator selection, so as to modify said specification of which ones of said electrical devices are controlled by each one of said switch control means,
> 
> encoder means coupled to said identification map means and said receiver means for generating a device command, said device command comprising said device identification and further specifying said selected switch state; and
> 
> a plurality of power outlet means coupled to said control unit means for providing said electrical power to said electrical devices in accordance with said device command, each one of said power outlet means being associated with one of said electrical devices, and each one of said power outlet means comprising:
> 
> decoding means for receiving and decoding said device command, and
> 
> device control means coupled to said decoding means for providing said electrical power to said associated electrical device in accordance with said selected switch state when said associated electrical device corresponds to said device identification of said device command. 

And now we start showing some infringing technology:

[LOOK NO FURTHER TO AVOID TREBLE DAMAGES](http://www.google.com/products?q=remote+power+switch&#038;oe=utf-8&#038;rls=org.mozilla:en-US:official&#038;client=firefox-a&#038;um=1&#038;ie=UTF-8&#038;ei=b6VqTKK9CYKL4QbckLS-AQ&#038;sa=X&#038;oi=product_result_group&#038;ct=title&#038;resnum=1&#038;ved=0CEAQrQQwAA)

Yes. Remote power switches. James Goslin filed for a patent on remote power switches. And he/SUN got it. What originally was a joke, as the &#8220;inventor&#8221; explains [here](http://nighthacks.com/roller/jag/entry/quite_the_firestorm):

> There was even an unofficial competition to see who could get the goofiest patent through the system. My entry wasn&#8217;t nearly the goofiest. 

is now potentially a fortune for Oracle! If the play their cards right, millions and millions of infringing power switches must be destroyed!

I would really love the see the even more goofier patents mentioned by James Gosling, but I guess Oracle will not tell us. They have to &#8220;defend&#8221; their assets. I do wish them luck. Eagerly waiting for more FWP (Fun With Patents).

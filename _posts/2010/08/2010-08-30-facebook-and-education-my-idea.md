---
id: 1294
title: 'Facebook and education &#8211; my idea'
date: 2010-08-30T23:10:23+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1294
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
How about this: History teachers, students across the world can &#8220;adopt&#8221; a Facebook account that represents a historical person. These historical persons have friends from their time, reflecting the connections they had. Status updates give historically correct information. 

So everyone can become a friend of a historical person, follow their updates, read about their interests etc.

Pros: Educate in a very natural way, raise interest in history.

Cons: Needs fact checking, could be abused to &#8220;rewrite&#8221; history.

But IMHO it is a wonderful idea to combine history and social networks. What do you think? Worth pursuing? Please comment &#8230;.

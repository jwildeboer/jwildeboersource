---
id: 1300
title: '#possesa &#8211; day 2 &#8211; patching, translating, concentration'
date: 2010-10-05T14:45:34+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1300
aktt_tweeted:
  - 1
categories:
  - OpenMisc
tags:
  - POSSE
---
So here we go again. Day 2 of POSSE SA as outlined in our [schedule](http://teachingopensource.org/index.php/POSSE_South_Africa) has just moved into the reflection and blogging phase.

So what did we do? I kicked off the day with explaining the galaxy that is Open Source and Free Software. the multitude of projects, the different governance models, how to find out about maturity and sustainability. I showed [gource](http://code.google.com/p/gource/) in [action](http://www.youtube.com/watch?v=lP3JaOFy-qM) -. I love the visualization of open source projects over time that it generates.

We then went to our first round of checkount &#8211; build &#8211; modify &#8211; commit using [git](http://git-scm.com/), which was fun and rewarding. People could actually learn how stuff works with immediate results.

We went on with translation, explaining the [gettext](http://www.gnu.org/software/gettext/) workflow, with po, pot and mo files and how [transifex](http://www.transifex.net/) makes all of this so extremely simple. We ended with doing some actual translation to [Afrikaans](http://en.wikipedia.org/wiki/Afrikaans) (af) using the fedora transifex [instance](https://translate.fedoraproject.org/). I hope our little community will continue to work on this.

So overall, another succesfull POSSE SA day! Let&#8217;s prepare for day 3 tomorrow. And relax with a beer in sunny Capetown.

---
id: 1302
title: '#possesa day 3 &#8211; let me fix your Ubuntu, Michael'
date: 2010-10-07T08:22:47+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1302
aktt_tweeted:
  - 1
categories:
  - OpenMisc
tags:
  - POSSE
---
Day 3 of POSSE is where the actual nwork in Open Source communities takes the front of the stage. So we discussed which projects to pick on, what to do. This lead to a more general discussion on South Africa and Open Source. What can we do, what is needed? We captured everything in a nice looking whiteboard &#8211; a true collaborative work!

So let me present our result:

![PosseSADay3](/images/2010/10/PosseSADay3WhiteBoard.jpg)
*Whiteboard PosseSA*
  
The picture nicely catches the specific problems faced in South Africa. Especially the way internet access is organised deserves a blog post of its own &#8230;

At the end of the day however, I returned to good ole tech support. Michael installed Fedora alongside Ubuntu on his laptop and due to some strange cosmic rays he ended up with a broken partition table and could not fire up his Ubuntu any longer. Oopsie. But we managed to get him back to Ubuntu with Fedora sitting next to it 🙂

---
id: 571
title: Denmark goes ODF. Only ODF. Sorry, OOXML
date: 2010-01-29T14:32:08+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=571
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
Breaking news. If your danish is good enough, read [here](http://politiken.dk/tjek/digitalt/computer/article890130.ece), [here](http://www.version2.dk/artikel/13690-breaking-odf-vinder-dokumentformat-krigen#post56157), [here](http://www.version2.dk/artikel/13693-er-ooxml-doemt-ude-her-er-kravene-til-en-offentlig-dokumentstandard), [here](http://www.version2.dk/artikel/13696-dokumentation-her-er-hele-aftalen-om-aabne-dokumentstandarder).

The [Konlusionspapiret](http://www.version2.dk/modules/fsArticle/download.php?fileid=31) has all the details.

So from April 2011 all intergovernmental documents will be in ODF. If this will also mean a change to OpenOffice remains to be seen however.

OOXML however is out. It is considered to not be good enough to be treated as an open standard. And trust me, Microsoft really tried to convince the danish politicians. But as it seems their arguments in support of OOXML (and also their numerous attempts to discredit ODF) have failed.

Will other countries learn? Tell them. Ask them. This is a good day for open standards.

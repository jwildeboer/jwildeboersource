---
id: 559
title: Oracle/SUN Merger OK by EU commission
date: 2010-01-21T12:15:45+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=559
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
From the [decision](http://europa.eu/rapid/pressReleasesAction.do?reference=IP/10/40&#038;format=HTML&#038;aged=0&#038;language=EN&#038;guiLanguage=en):
  
wrt MySQL:

> Given the open source nature of MySQL, the Commission also assessed Oracle&#8217;s ability and incentive to remove the constraint exerted by MySQL after the merger and the extent to which this constraint could, if necessary, be replaced by other actors on the database market.
> 
> The Commission&#8217;s investigation showed that another open source database, PostgreSQL, is considered by many database users to be a credible alternative to MySQL and could be expected to replace to some extent the competitive force currently exerted by MySQL on the database market. In addition, the Commission found that &#8216;forks&#8217; (branches of the MySQL code base), which are legally possible given MySQL&#8217;s open source nature, might also develop in future to exercise a competitive constraint on Oracle in a sufficient and timely manner. Given the specificities of the open source software industry, the Commission also took into account Oracle&#8217;s public announcement of 14 December 2009 of a series of pledges to customers, users and developers of MySQL concerning issues such as the continued release of future versions of MySQL under the GPL (General
  
> Public Licence) open source licence. Oracle has already taken action to implement some of its pledges by making binding offers to third parties who currently have a licensing contract for MySQL with Sun to amend contracts. This is likely to allow third parties to continue to develop storage engines to be integrated with MySQL and to extend the functionality of MySQL. 

So Neelie Smit-Kroes and her people are smart. They know what they are talking about and they did what I expected: thorough research. I fully agree with the findings. The Open Source ecosystem can produce competing solutions. It doesn&#8217;t need a Dual License special approach.

wrt JAVA:

> It found that Oracle&#8217;s ability to deny its competitors access to
  
> important IP rights would be limited by the functioning of the Java
  
> Community Process (JCP) which is a participative process for developing and revising Java technology specifications involving numerous other important players in the IT industry, including Oracle&#8217;s competitors.
> 
> The Commission also found that Oracle would not have the incentives to restrict its competitors&#8217; access to the Java IP rights as this would
  
> jeopardise the gains derived from broad adoption of the Java platform
  
> and therefore the proposed transaction would raise no competition
  
> concerns in respect of the licensing of IP rights connected with Java. 

This is going to be more important IMHO. The freedom of Java is really important.

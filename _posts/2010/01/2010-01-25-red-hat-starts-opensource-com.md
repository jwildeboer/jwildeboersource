---
id: 565
title: Red Hat starts opensource.com
date: 2010-01-25T16:13:14+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=565
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
Finally. A place to discuss, learn, promote everything open. With 5 channels:

  * [Business](http://opensource.com/business)
  * [Education](http://opensource.com/education)
  * [Government](http://opensource.com/government)
  * [Law](http://opensource.com/law)
  * [Life](http://opensource.com/life)

We started thois community to allow people from all across the world, regardless of job and skills, to communicate and foster more Open. Let&#8217;s look (far) beyond software. Lets talk about the Open Everything that this world so desperately needs.

Talk to you at opensource.com!

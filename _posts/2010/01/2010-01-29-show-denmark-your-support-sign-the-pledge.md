---
id: 573
title: Show Denmark your support! Sign the pledge!
date: 2010-01-29T16:24:25+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=573
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
Please all, spread the word and sign my pledge!

Let us show Denmark how much we support their brave decision to use ODF as the onyl document format.

Sign here:

<http://www.pledgebank.com/DenmarkODF>

[<img border="0" src="http://www.pledgebank.com/flyers/DenmarkODF_A7_flyers1_live.png" alt="Sign my pledge at PledgeBank" />](http://www.pledgebank.com/DenmarkODF)

Spread the word!

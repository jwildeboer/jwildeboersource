---
id: 592
title: WRT FOSDEM Beer Event
date: 2010-02-04T16:45:56+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=592
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
Lots of comments on my last blog entry. Let me make some points clear before you all turn the flamewar to full force.

**FIRST** I am NOT proposing you turn the beer event into a political demonstration. I do strongly encourage you to go there to have FUN. That&#8217;s what I will do. I will be there. Wearing my Fedora. So if you want to flame me for what I say, let&#8217;s do it face to face.

**SECOND** The fact that the developers working on the Android kernel parts seemingly (as pointed out by Greg Kroah-Hartman) have problems with upstreaming their patches is an alarming signal for the Linux community. I hope that Google shows us that I am wrong and they are willing to upstream their changes.

**THIRD** My call for saying &#8220;No, Thanks&#8221; to the beer is a pun directing to RMS famous quotes on comparing free beer and free speech.

So in closing: **Geez, peeps! FOSDEM is about having fun \*and\* exchanging opinions. We should have both.** 

The unique opportunity that a lot of Open Source and Free Software minded people meet at FOSDEM could be a strong signal towards Google that we are there to \*help\* making things better.

So grab your beer, have fun and flame me if needed. If you want to throw your frustration and anger on me &#8211; fine. But I would prefer if you use that energy to get more Android patches upstream.

Deal?

**UPDATE**

This [mail](http://thread.gmane.org/gmane.linux.kernel/905490/focus=907382) shows you that Google wants to get stuff upstream but ATM (that was in october last year) is limited in doing so..

> We (Google) definitely want to get back in sync with mainline (as I&#8217;ve mentioned earlier in this or a related thread), and we&#8217;re planning on snapping up our kernel trees to 2.6.32+ once we get past various deadlines in the near future.

---
id: 585
title: Is Google forking the Linux kernel?
date: 2010-02-03T12:42:58+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=585
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
**DISCLAIMER: This post reflects my personal opinion and is in no way related to the company I work for.**

LWN [tells](http://lwn.net/Articles/372419/) us what is happening with the android kernel patches in upstream.

The short version: They are [gone](http://git.kernel.org/?p=linux/kernel/git/torvalds/linux-2.6.git;a=commit;h=b0a0ccfad85b3657fe999805df65f5cfe634ab8a)

As Greg Kroah-Hartman [explains](http://www.kroah.com/log/linux/android-kernel-problems.html):

> So, what happened with the Android kernel code that caused it to be deleted? In short, no one cared about the code, so it was removed.

This is a normal process, Microsoft was there [before](http://www.linux-magazine.com/Online/News/Kroah-Hartman-Remove-Hyper-V-Driver-from-Kernel).

So far nothing special. However, as Greg correctly points out:

> The Android kernel code is more than just the few weird drivers that were in the drivers/staging/android subdirectory in the kernel. In order to get a working Android system, you need the new lock type they have created, as well as hooks in the core system for their security model.

Now adding new features to the kernel is quite OK. If the changes are sound, stable and help Linux in general, the kernel community will gladly accept such changes upstream. I was sure Google was well aware of this process and according to their promises, I was also sure that they clearly understood that Upstream Is King.

This seems to be wrong. It is up to Google to fix this problem. Google hackers should make sure this situation is solved for the best of upstream ASAP.

If this doesn&#8217;t get solved, the kernel running Android is a fork. An incompatible fork, as Greg concludes:

> Because of this, Google has now prevented a large chunk of hardware drivers and platform code from ever getting merged into the main kernel tree. Effectively creating a kernel branch that a number of different vendors are now relying on.

Forking the linux kernel is the worst possible outcome. Tell Google what you think of this. Use your contacts inside Google to make sure they understand the magnitude of this problem (however, I am quite sure that the smart Google peeps know this already, which makes this even more strange).

[UPDATE] In october last year we already heard a bit about Google trying to work better with upstream, as described [here](http://lwn.net/Articles/357658/) (thanks to @haypo in the comments):

> Linus asked: why aren&#8217;t these patches upstream? Is it because Google is embarrassed by them, or is it secret stuff that they don&#8217;t want to disclose, or is it a matter of internal process problems? The answer was simply &#8220;yes.&#8221; Some of this code is ugly stuff which has been carried forward from the 2.4.18 kernel. There are also doubts internally about how much of this stuff will be actually useful to the rest of the world. But, perhaps, maybe about half of this code could be upstreamed eventually. 

One can only wonder. They **know** there is a problem, they **promise** to change this as supporting forks is expensive and not really sustainable (quelle surprise!) but still, it seems they are not walking the walk.[/UPDATE]

**FLASH-MOB Call**

In case you are going to the FOSDEM [Beer Event](http://fosdem.org/2010/beerevent) this friday, join the flashmob. Google will sponsor free beer &#8211; refuse it. Don&#8217;t take the free beer. Insist on the integrity of the kernel community.

---
id: 1260
title: 'Switzerland &#8211; 0wn3d by Microsoft'
date: 2010-07-06T14:39:39+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1260
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
[NOTE &#8211; this is my PERSONAL opinion and not the official opinion of my employer]

[Source in german](http://www.bernerzeitung.ch/digital/computer/OpenSourceAnbieter-vor-Gericht-gescheitert/story/25654319)

Background on the case: [Asking Switzerland for more Neutrality](https://jan.wildeboer.net/2009/05/asking-switzerland-for-more-neutrality/)

The court has decided. A tender would be disruptive to the Microsoft-only world of the swiss public. One of five judges disagrees and mentions that this effectively means there is no alternative to Microsoft and that this might not be what a free market is about.

I can only say &#8220;Congratulations&#8221; to Microsoft. Feel free to cash in the 47 million SFR of swiss taxpayers money and look forward to a perpetuum mobile. You now own the swiss public sector. This is what competition and free market is about?

The trick used here was to say that the BBL wanted to buy a defined &#8220;technology&#8221;. The &#8220;technology&#8221; however was the Microsoft range of products. By using this interpretation it was obvious that Open Source solutions cannot compete and therefore a tender was not needed.

This argumentation disrespects a fundamental fact IMHO &#8211; this &#8220;technology&#8221; is not available in a market &#8211; it is \*only\* available from a \*single\* vendor who sets the price, the features and the scope. With this fundamentally flawed argument Microsoft now effectively is even free to ask \*any\* price for its &#8220;technology&#8221;. A single-vendor market is typically not seen as a free market. But I guess in Switzerland they have a different definition in place. Is this a wise use of taxpayers money? I respectfully disagree.

In all european countries there is a concept called vendor-neutral procurement for public authorities. This court decision is a HOWTO on disabling that requirement in Switzerland. And THAT is the dangerous precedent set here.

I am shocked but I do respect the courts decision. Feel free to add your comments here. I will post a more indepth article as soon as I have received more information.

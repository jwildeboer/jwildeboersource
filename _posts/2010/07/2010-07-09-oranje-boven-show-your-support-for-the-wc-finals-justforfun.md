---
id: 1269
title: 'Oranje Boven! Show your support for the WC Finals! #JustForFun'
date: 2010-07-09T16:47:07+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1269
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
I just changed my avatar picture on Facebook, Twitter, identi.ca to this very minimalitsic design:

![Oranje](/images/2010/07/FBProfile.png)

If you are dutch or if you support the dutch against spain in the finals, feel free to use this pic! And spread the word to all your friends who might feel the same urge to show support!

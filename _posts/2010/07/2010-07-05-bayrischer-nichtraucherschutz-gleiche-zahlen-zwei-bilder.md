---
id: 1234
title: 'Bayrischer Nichtraucherschutz &#8211; Gleiche Zahlen, zwei Bilder'
date: 2010-07-05T11:24:02+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1234
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---

![BAYVE](/images/2010/07/BAYVE.png)
*BAYVE Version*

Und daneben die offizielle Darstellung von http://www.volksentscheid2010.bayern.de/taba2990.html

![Volksentscheid](/images/2010/07/990_ve273_0407_2021017096_dia.png)
*Offizielle Version*

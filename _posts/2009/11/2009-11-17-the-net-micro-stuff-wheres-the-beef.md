---
id: 473
title: 'The .NET Micro stuff &#8211; Where&#8217;s the beef? [updated]'
date: 2009-11-17T14:33:37+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=473
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
So I guess I was a bit too carried away in my previous post by assuming that the .NET Micro Framework is really something big. After doing my research, let me focus a bit more on the product itself and its importance to Microsoft.

Let&#8217;s read this [article](http://www.webcitation.org/5ghMm8Xgu) again. On may, 6th 2009 this journalist states that the .NET Micro framework is effectively obsolete stuff that Microsoft doesnt want to support anymore, so in an act of generosity, they will give it to the community:

> Microsoft is turning the source code for its embedded .Net Micro Framework over to the community and slowly withdrawing from that
  
> business, company officials are confirming.
> 
> (Update on May 7: Microsoft disagrees with my characterization of this
  
> move as “withdrawing from the business.” But I’m standing by what I
  
> said, while making it clear company officials didn’t say they are
  
> withdrawing. To me, if you cut a bunch of a team and turn your source
  
> code over to external parties, you are not signaling that you’re
  
> continuing to stand firmly behind a product.

Today we see this plan [taking shape](http://port25.technet.com/archive/2009/11/16/microsoft-to-open-source-the-net-micro-framework.aspx) (a bit delayed, isn&#8217;t it?).

> I have great news to announce. Today, at the Microsoft Professional
  
> Developer Conference (PDC) here in Los Angeles, we announced not only
  
> the release of version 4.0 of the.NET Micro Framework, but also that we
  
> are open sourcing the product and making it available under the Apache
  
> 2.0 license, which is already being used by the community within the
  
> embedded space.

So no real news so far, in fact just an update. The only (possible) good news is that Apache 2.0 License forces MSFT to give the community a

> perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable (except as stated in this section) patent license to make, have made, use, offer to sell, sell, import, and otherwise transfer the Work,

But I do not see a list of patents yet.

Now depending on what parts of .NET are implemented in the Micro
  
Framework this might be partly good news for MONO users. But that is about the only interesting thing I can see.

In closing, let&#8217;s take a look at what this stuff [is all about](http://en.wikipedia.org/wiki/.NET_Micro_Framework) on a
  
more technical level:

> The .NET Micro Framework is currently supported on ARM architecture processors (including ARM7 and ARM9) and on Analog Devices Blackfin.

This means no support fro MMUs etc etc. So it is really a very basic thing.

> The .NET Micro Framework has its roots in Microsoft&#8217;s SPOT initiative and was used in MSN Direct products such as smart watches before being made available to third-party developers early in 2007. It is a common platform for Windows SideShow devices and is beginning to see adoption in other markets, such as home automation and sensor networks.

Funny thing is however that the .NET Micro Framework comes without a
  
TCP/IP stack (third party stuff) and without a crypto library.

So what do we have here? A runtime environment for very basic stuff
  
(about 70 classes with about 420 methods), some presentation layer
  
loosely based on WPF but that&#8217;s about it.

It might help you in creating cool, non-connected gadgets with nice
  
bluescreens &#8211; but nothing more 😉

So where&#8217;s the Beef in this announcement?

[UPDATE]

[So I guess I was a bit too carried away in my previous post by assuming that the .NET Micro Framework is really something big. After doing my research, let me focus a bit more on the product itself and its importance to Microsoft.

Let&#8217;s read this [article](http://www.webcitation.org/5ghMm8Xgu) again. On may, 6th 2009 this journalist states that the .NET Micro framework is effectively obsolete stuff that Microsoft doesnt want to support anymore, so in an act of generosity, they will give it to the community:

> Microsoft is turning the source code for its embedded .Net Micro Framework over to the community and slowly withdrawing from that
  
> business, company officials are confirming.
> 
> (Update on May 7: Microsoft disagrees with my characterization of this
  
> move as “withdrawing from the business.” But I’m standing by what I
  
> said, while making it clear company officials didn’t say they are
  
> withdrawing. To me, if you cut a bunch of a team and turn your source
  
> code over to external parties, you are not signaling that you’re
  
> continuing to stand firmly behind a product.

Today we see this plan [taking shape](http://port25.technet.com/archive/2009/11/16/microsoft-to-open-source-the-net-micro-framework.aspx) (a bit delayed, isn&#8217;t it?).

> I have great news to announce. Today, at the Microsoft Professional
  
> Developer Conference (PDC) here in Los Angeles, we announced not only
  
> the release of version 4.0 of the.NET Micro Framework, but also that we
  
> are open sourcing the product and making it available under the Apache
  
> 2.0 license, which is already being used by the community within the
  
> embedded space.

So no real news so far, in fact just an update. The only (possible) good news is that Apache 2.0 License forces MSFT to give the community a

> perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable (except as stated in this section) patent license to make, have made, use, offer to sell, sell, import, and otherwise transfer the Work,

But I do not see a list of patents yet.

Now depending on what parts of .NET are implemented in the Micro
  
Framework this might be partly good news for MONO users. But that is about the only interesting thing I can see.

In closing, let&#8217;s take a look at what this stuff [is all about](http://en.wikipedia.org/wiki/.NET_Micro_Framework) on a
  
more technical level:

> The .NET Micro Framework is currently supported on ARM architecture processors (including ARM7 and ARM9) and on Analog Devices Blackfin.

This means no support fro MMUs etc etc. So it is really a very basic thing.

> The .NET Micro Framework has its roots in Microsoft&#8217;s SPOT initiative and was used in MSN Direct products such as smart watches before being made available to third-party developers early in 2007. It is a common platform for Windows SideShow devices and is beginning to see adoption in other markets, such as home automation and sensor networks.

Funny thing is however that the .NET Micro Framework comes without a
  
TCP/IP stack (third party stuff) and without a crypto library.

So what do we have here? A runtime environment for very basic stuff
  
(about 70 classes with about 420 methods), some presentation layer
  
loosely based on WPF but that&#8217;s about it.

It might help you in creating cool, non-connected gadgets with nice
  
bluescreens &#8211; but nothing more 😉

So where&#8217;s the Beef in this announcement?

[UPDATE]

](http://blogs.msdn.com/netmfteam/archive/2009/11/15/net-micro-framework-version-4-0-ships-open-source-community-development-and-more.aspx) is nice:

> We do not include the source code for these libraries for several reasons – the TCP/IP stack is licensed from EBSNet(http://ebsnetinc.com/) and the Crypto libraries are used in other products besides the .NET Micro Framework.

So just because crypto is used also in other products, it cannot be opensourced? I guess we need to educate MSFT a bit on OS licensing &#8230;

> The Microsoft engineers will continue to focus on some of the things that only they can do – deep coordination with the rest of the .NET team

So not THAT open, I guess &#8230;

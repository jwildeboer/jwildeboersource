---
id: 486
title: Good news? The EU declaration on eGovernment
date: 2009-11-23T12:12:57+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=486
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
We all remember the discussions around the leaked EIFv2 draft. We know there is a lot of lobbying efforts in the background to make sure the principles of EIFv1 are kept and extended.

So today we see the [Ministerial declaration on eGovernment policy](http://www.se2009.eu/polopoly_fs/1.24306!menu/standard/file/Ministerial%20Declaration%20on%20eGovernment.pdf). And it does contain some indications on what will likely happen. It&#8217;s good news combined with some vagueness.

Here is the part I do like (page 4):

> 21. Pay particular attention to the benefits resulting from the use of open specifications in order to deliver services in the most cost-effective manner. We will ensure that open specifications are promoted in our national interoperability frameworks in order to lower barriers to the market. We will work to align our national interoperability frameworks with applicable European frameworks. The Open Source model could be promoted for use in eGovernment projects. It is
  
> important to create a level playing field where open competition can take place in order to ensure best value for money.

Now this IS good news. With some vagueness. The good news is that Open Source is recognized as a valid method of developing good solutions. The vagueness is &#8220;open specifications&#8221;. Why not call for open standards? We will have to wait what exactly is considered an &#8220;open specification&#8221;. I can tell you what I would like to see as a definition for this:

  1. Developed and maintained in an open process where anyone can particpate. So no membership fees, expensive travel requirements etc.
  2. The specification is available to anyone for free, eg by offering it for free as download on the web. Ideally at a central place, so anyone can see and get all open specifications at a one-stop-shop.
  3. Whatever patents or other so-called &#8220;intellectual property&#8221; is available on a royalty-free basis to all users and implementors by default and this &#8220;IP&#8221; is not limited to the specification. This is key. No lock-in based on patents or licenses.
  4. The implementation and usage must be unlimited. So no per-player or pay-per-view models.
  5. There must be at least two implementations available, ideally at least one of them Open Source. This to make sure that we have interoperability that can be verified. Single-vendor implementations with no competitive implementation are NOT allowed.
  6. There must be a validator and test-suite to garantuee conformance, validity, usability and correct interoperability. This test suite etc. must be available under open source license, so that anyone can not only use it but also examine it for correctness.

So now that we now what the ministers think, let&#8217;s help them in making EIFv2 the defining framework. It&#8217;s up to all of you to assist our politicians to not only talk the talk but also walk the walk.

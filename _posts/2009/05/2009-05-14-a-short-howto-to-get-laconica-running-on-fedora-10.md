---
title: A short HOWTO to get laconi.ca running on Fedora 10
date: 2009-05-14T12:09:35+00:00
author: jwildeboer
layout: single
categories:
  - OpenMisc
  - Tips & Tricks & Fixes
tags:
  - OpenKnowledge
---
Simple, if you know how to do it 🙂

Prerequisites:

  * Up and running Fedora 10 with web server and mysql server installed (httpd, mysql-server RPM)

1. Get the tarball from http://www.laconi.ca

2. You need to install some RPMs, I ended up with adding these (you might already have most of them installed)

```
# yum install php-gd t1lib php-mbstring php-xml php-xmpphp \
php-oauth php-pear-Net-SMTP php-pear-Mail php-pear-DB-DataObject \
php-pear-Auth-SASL php-pear-MDB2 php-pear-DB \
php-pear-Net-Socket php-pear-Auth-OpenID php-pear-Net-Curl \
php-pear-Validate php-pear-Date php-pgsql php-bcmath php-pear \
```

3. You might also need markdown.php which unfortunately doesn&#8217;t yet exist as RPM. So get it from http://michelf.com/projects/php-markdown/ while it is hot 😉 The normal version is sufficient. Unzip the file and place markdown.php in your PHP search path (mine lives in /usr/share/php)

4. I placed my laconica installation in /var/www/html/laconica, so just untar the tarball in that directory, rename the directory if needed/wanted.

5. Create the database, database user and set the rights accordingly:

```
# mysql -uroot -p -e 'create database laconica';
# mysql -uroot -p -e "grant all privileges on laconica.* to laconica@localhost identified by 'PASSWORD'";
```

6. Now call http://localhost/laconica/install.php and finish the installation.

Done. Have fun with microblogging.

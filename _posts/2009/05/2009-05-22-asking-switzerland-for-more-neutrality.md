---
id: 118
title: 'Asking Switzerland for more neutrality &#8230;'
date: 2009-05-22T08:21:19+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=118
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
&#8230; sounds ironical, doesn&#8217;t it? However, it is true. So what is going on?

On the 23th of february 2009 the swiss [BBL](http://www.bbl.admin.ch/) awarded Microsoft a contract for client/server licenses, applications and 3rd level support. This was published on the 1st of May [here](https://www.shab.ch/shabforms/servlet/web/PdfView?DOCID=367121). The 

The contract is worth 14 million Swiss Francs per year and as it is running for 3 years, the total amount is 42 million SFR. Now usually such investments are made after public tendering to make sure the most cost effective solution is chosen. In this case however, the BBL decided that they could use a specific article in the law to avoid the tendering process. You can look up [&#8230; sounds ironical, doesn&#8217;t it? However, it is true. So what is going on?

On the 23th of february 2009 the swiss [BBL](http://www.bbl.admin.ch/) awarded Microsoft a contract for client/server licenses, applications and 3rd level support. This was published on the 1st of May [here](https://www.shab.ch/shabforms/servlet/web/PdfView?DOCID=367121). The 

The contract is worth 14 million Swiss Francs per year and as it is running for 3 years, the total amount is 42 million SFR. Now usually such investments are made after public tendering to make sure the most cost effective solution is chosen. In this case however, the BBL decided that they could use a specific article in the law to avoid the tendering process. You can look up](http://www.gimap.admin.ch/hilfe/faq/d/au05.htm) what the reasons are.

Let me translate that in a rough way for you:

**What prerequisites are needed to call Art. 13, 1 lit.c Voeb (technical or artistic particularities of the order) into action?**

_It needs to be shown that due to technical or artistic particularities of this order or for reasons of protection of intellectual property only one supplier can be selected and due to this **no sufficient alternative** is available_

(emphasis added by me)

These are the facts. BBL says there is absolutely no alternative to Microsoft, for servers, for clients, for applications. And due to this they can only select Microsoft. So no need for public tendering. Deal done. Microsoft happy.

And it doesn&#8217;t stop there. According to [NZZ](http://www.nzz.ch/nachrichten/schweiz/fragwuerdige_auftragsvergabe_des_bundes_an_microsoft_1.2502044.html) Ms. Lunau of BBL told NZZ that &#8220;due to the specifics of the Bundesverwaltung. competition between vendors will not exist short- and mid-term.&#8221;

Would you agree? Would you support sending 42 mio SFR to Microsoft without even evaluating alternatives? Especially when in the very same country, Switzerland, the government asks for equal treatment of Open Source? Knowing that the Kanton Solothurn and several courts are using exactly those alternatives that according to BBL simply do not exist?

Well &#8211; a lot of people in Switzerland and almost 20 companies are not really happy. Among these companies is Red Hat. So Red Hat is [joining the official appeal](http://press.redhat.com/2009/05/21/red-hat-challenges-microsoft-lock-in-and-seeks-open-competition-in-switzerland/) that asks the court to stop this contract, force BBL to evaluate the alternatives that exist and are in active use, even in Switzerland in the offical way of public tendering.

Effectively the appeal asks the BBL to defend competition. In a vendor neutral way. Based on technical merits. I think it is fair to ask. Let the court decide.

And ofcourse the irony of asking Switzerland of all countries to act in a (vendor-) neutral way is quite funny.

[DISCLAIMER] This blog contains my personal opinion and does not reflect the opinion of Red Hat.

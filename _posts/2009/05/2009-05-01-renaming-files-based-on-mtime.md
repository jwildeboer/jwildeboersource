---
title: Renaming Files based on mtime
date: 2009-05-01T19:26:35+00:00
author: jwildeboer
layout: single
categories:
  - OpenMisc
  - Tips & Tricks & Fixes
tags:
  - OpenKnowledge
---

So I have this cool little camera, the JVC GZ-MS100R, a SD camera that generates simple MPEG-2 files. The filenames are however a bit uncool:

```
MOV001.MOD
MOV002.MOD
```

This doesn’t really help when you are looking for the movies you made on christmas eve.

So I checked the files a bit closer and noticed that the mtime information is the time that the clip was recorded. So I wrote two simple shell scripts. One that loops through all entries in my directory that contains the clips and calls the second script that renames the file to the mtime content and changes the extension to .mpg. So the new filenames look like this:

```
20081224-151412.mpg
20081228-105122.mpg
20081228-155136.mpg
```

So here the first simple script, mover.sh:

```
#!/bin/bash

for i in $(ls *.MOD); do
./renamer.sh $i
done
```
Wow 🙂 As you can see, it simply looks for files ending with .MOD and calls the second script, renamer.sh:

```
#!/bin/bash

echo “Working on $1”

YEAR=`ls -l –full-time $1 | awk ‘{print $6 }’ | cut -d ‘-‘ -f 1`
MONTH=`ls -l –full-time $1 | awk ‘{print $6 }’ | cut -d ‘-‘ -f 2`
DAY=`ls -l –full-time $1 | awk ‘{print $6 }’ | cut -d ‘-‘ -f 3`

HOUR=`ls -l –full-time $1 | awk ‘{print $7 }’ | cut -d “:” -f 1`
MINUTE=`ls -l –full-time $1 | awk ‘{print $7 }’ | cut -d “:” -f 2`
SECOND=`ls -l –full-time $1 | awk ‘{print $7 }’ | cut -d “:” -f 3 | cut -d “.” -f 1`
NEWNAME=`echo “$YEAR$MONTH$DAY-$HOUR$MINUTE$SECOND”`

mv $1 renamed/$NEWNAME.mpg
```

This one extracts the mtime information from the file to construct the new file name. It moves the file from its current place to the directory renamed from where I copy it to my media server.

Some more details. The –full-time option to ls gives me the ISO time representation I need. The date part looks like this: 2008-12-24. Now with cut and “-” as the delimiter I can simply collect the YEAR, MONTH and DAY. The time is similar, however it uses “:” as the delimiter and I need to strip the seconds down to just that.

Done 🙂

Next step: The camera gernerates MPEG-2 with AC3 audio. My mediaplayer (MythTV) doesn’t like that. So lets use ffmpeg to convert the audio to plain simple mp2 stereo. In my next post 🙂

HTH
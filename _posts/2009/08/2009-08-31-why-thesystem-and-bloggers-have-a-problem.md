---
id: 384
title: Why TheSystem and bloggers have a problem
date: 2009-08-31T08:44:22+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=384
aktt_tweeted:
  - 1
categories:
  - OpenMisc
tags:
  - OpenKnowledge
  - OpenMinds
  - OpenSociety
---
[Glyn Moody](https://twitter.com/glynmoody) pointed me to this article:

[&#8216;Bloggers&#8217; vs &#8216;Audience&#8217; is over? or, Will the word &#8216;blogger&#8217; disappear?](http://scienceblogs.com/clock/2009/08/bloggers_vs_audience_is_over_o.php)

and it lead me to jot down why I think the term writer, author, reader in such discussions really miss the point.

TheSystem, which means the copyright system of publishers, news agencies etc. have a vested interest in keeping us in two worlds. Either you are a producer or a consumer. This makes the media world go round and has been their source of revenue. It all works because in the past it was costly to distribute your product. You had to print books, newspapers, transport them to book stores, little kiosks etc. And so it was natural that the consumer had to pay for it.

Fast forward to the Web Age. With distribution costs close to zero, this old value chain is struggling. And as if that isn&#8217;t enough, the consumer does stuff he is not allowed to do &#8211; he distributes his own content by using this new system of zero-cost distribution.

So we nowadays have not only a simple top-down market, from producer to consumer, we now have a system with three levels.

We still have producers, stuck in their old value chain, and we still have consumers &#8211; willing to pay for the work of the producers, but we now also have **ProSumers**, people that consume AND produce.

Bloggers are ProSumers. They live in networks of other ProSumers, and they also consume from producers. And this is where the problem starts for the old-.school producers.

As ProSumers remix content they gather, deem interesting, create themselves and distribute that under the same principles &#8211; knowing that other ProSumers will remix, the old value chain of Consumer-pays-Producer is under attack.

The old system defends itself with DRM (Digital Restriction Management), putting ProSumers in the same group as pirates and add to that that only real producers (journalists etc.) should be allowed to deliver content to consumers.

And slowly but surely the ProSumers distance themselves from this dying system. With Creative Commons, with Open Source and simply by ignoring the old value chain, they create a parallel system of content creation and distribution.

And this is the real problem. It is as simple as that. It is the value chain

**Producer &#8211;> Consumer**

versus

**Producer <--> Prosumer <--> Consumer.**

And as long as you try to defend the old chain while combining it with the new system you **will** fail. Funny thing is that quite some bloggers still see themselves more a producers where they effectively are prosumers.

And the so-called producers of the past have always been prosumers themselves. So as soon as TheSystem accepts the new flow, we will see more creativity and less restriction. If TheSystem fails to adapt, it will die.

---
id: 30
title: 'YouTube &#8211; Amazing sheep LED art'
date: 2009-03-25T10:13:15+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=30
aktt_tweeted:
  - 1
categories:
  - OpenFun
tags:
  - OpenMinds
---
[YouTube &#8211; Amazing sheep LED art](https://www.youtube.com/watch?v=D2FX9rviEhw).

Enjoy! LED mantled sheeps performing PONG, fireworks etc.  🙂

---
id: 28
title: 'Kadushin &#8211; Open Design'
date: 2009-03-24T15:29:41+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=28
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
 [Ronen Kadushin](https://www.ronen-kadushin.com/open-design), famous designer in Berlin decides to do something quite interesting. He puts some of his designs under Creative Commons license.

So you can download the Autocad files and building instructions directly from his website. You can build, change, hack his designs. However &#8211; no commercial usage. Ask the designer if you want to do that.

If you ever were in doubt about this Open Source movement thingy, here you get another example of how to do it right.

Furniture hobbyists &#8211; unite! High quality designs for everyone!

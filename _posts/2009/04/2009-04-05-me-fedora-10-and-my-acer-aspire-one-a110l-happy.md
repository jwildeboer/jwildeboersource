---
id: 48
title: 'Me, Fedora 10 and my Acer Aspire One A110L &#8211; Happy'
date: 2009-04-05T20:53:40+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=48
aktt_tweeted:
  - 1
categories:
  - OpenMisc
tags:
  - AAO
  - Fedora
  - OpenKnowledge
---
So I did it. I took 199 € and got myself a Netbook. I went shopping in the Munich Bermuda Triangle, where you find something like 10 shops, all fighting with eachother over the lowest price.

Walking thorugh all shops, comparing price, performance, design, I finally settled on the Acer Aspire One A110L &#8211; 512 MB RAM, 1024&#215;600 screen, 8 GB of SSD, in white.

It gets delivered with [Linpus](http://www.linpus.com), a [Fedora](http://www.fedoraproject.org) derivative. Ofcourse I didn&#8217;t like it. So I put a standard Fedora 10 on the box. With the help of [some](http://fedoraproject.org/wiki/Acer_Aspire_One) [pages](http://jorge.fbarr.net/2008/11/10/fedora-10-on-the-acer-aspire-one/) across the internet, some studying and some use of my own knowledge, I am now quite happy with the setup.

Suspend/resume is a snap out of the box. Close the lid &#8211; sleep. Open the lid, press the power button, login after three seconds. I like it. I tweaked the font size and some other stuff, but in fact I did nothing special.

Upcoming mods:

  * RAM upgrade to 1.5 GB
  * Bigger battery
  * Find a cool sleeve
  * Mythtv and Totem, so I can watch my recordings

I am happy, finally a cool netbook that is mine and I can take everywhere 🙂

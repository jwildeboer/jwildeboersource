---
title: Working Dual Screen for my Aspire One with Fedora 10
date: 2009-04-07T11:19:34+00:00
author: jwildeboer
layout: single
categories:
  - OpenMisc
  - Tips & Tricks & Fixes
tags:
  - AAO
  - Fedora
  - OpenKnowledge
---
OK, so I wanted to use my Aspire One to do presentations. However &#8211; getting dual screen to work was quite a lot of work.

I found a simple solution, which I am going to outline step-by-step for your convenience. This is all based on Fedora 10, 32bit, fully updated.

Result will be:

  * External VGA runs with 1024&#215;768 @ 60 Hz, good enough for most projectors out there, with a single click
  * The display of the Aspire stays fully usable
  * The GNOME panels stay on the laptop

So step by step.

First &#8211; open a root shell because we will do some evil hacks.

We need to create an xorg.conf file, which normally doesn&#8217;t exist. so type

`# Xorg -configure :1`

Don&#8217;t be surprised. Your screen will flicker around. After a few seconds you will find a new file in your directory called xorg.conf.new. Copy tis file to the X11 directory:

`# cp xorg.conf.new /etc/X11/xorg.conf`

Now we need to make a few changes. First we need to switch down to 16 bit colors, due to the limited graphic memory of my Aspire One. Second we need to create a bigger virtual screen.

So find the section called

`Section "Screen"<br />
  Identifier "Screen0"<br />
  Device     "Card0"<br />
  Monitor    "Monitor0"`

Right there add a line with

  `Default Depth 16`

This forces X to use 16bit colors, making sure the extended display will fit in the 8 MB of graphic memory.

Now go down in xorg.conf and find the section

`SubSection "Display"<br />
  Viewport   0 0<br />
  Depth     16<br />
EndSubSection`

And add a line with

`Virtual 2048 2048`

so the whole section looks like this:

`SubSection "Display"<br />
  Viewport   0 0<br />
  Depth     16<br />
  Virtual 2048 2048<br />
EndSubSection`

OK. Done with that part. Now the next part. We want to make sure that we can switch the layout in a very simple way. I am pragmatic. So no fancy GUI or stuff. What I need is to be able to present. So when I hook up a projector, I want to switch the VGA to a fixed 1024&#215;768 resolution at 60 Hertz. This is compatible with most projectors. So I wrote a little shellscript that also makes sure that the top and bottom panels are at the right place &#8211; on my laptop.

I stored this under `/usr/local/bin/vgaswitch`:

`#!/bin/sh<br />
/usr/bin/xrandr | grep "VGA connected"<br />
if [ $? = 0 ] ; then<br />
  echo "VGA connected, setting VGA to 1024x768 @ 60 Hz"<br />
  /usr/bin/xrandr --output LVDS --auto --output VGA --mode 1024x768 --rate 60 --right-of LVDS<br />
  # Now move the panels back to the laptop screen<br />
  gconftool-2 --set "/apps/panel/toplevels/bottom_panel/monitor" --type integer "1"<br />
  gconftool-2 --set "/apps/panel/toplevels/top_panel/monitor" --type integer "1"<br />
else<br />
  echo "No VGA connected, back to single screen"<br />
  /usr/bin/xrandr --output LVDS --auto --output VGA --off<br />
  # Now move the panels back to the laptop screen<br />
  gconftool-2 --set "/apps/panel/toplevels/bottom_panel/monitor" --type integer "0"<br />
  gconftool-2 --set "/apps/panel/toplevels/top_panel/monitor" --type integer "0"<br />
fi`

Make the script executable with a `chmod a+x /usr/local/bin/vgaswitch`

The final step is to add a new panel launcher that calls the script. Done.

So now when I hook up a projector, I click on the icon that calls the script and &#8211; TADAAA! Remove the VGA cable, click it again, all back to normal. I am happy now 🙂

---
title: Short encrypted SD Card HOWTO for Fedora 10
date: 2009-04-15T11:58:35+00:00
author: jwildeboer
layout: single
categories:
  - OpenMisc
  - Tips & Tricks & Fixes
tags:
  - Fedora
  - OpenKnowledge
---
Just in case you also want to use encryption and you have a SD card. Here&#8217;s what I did, step-by-step, to create a simple setup with:

  * 16 GB SD Card
  * 2 partitions, one unencrypted for simpla data exchange, one encrypted for secure storage

So here we go. Insert the SD Card. It probably is preformatted with FAT. Unmount it the usual way and open a root shell. Using fdisk, create two partitions, save the new partition table.

First thing is simple. Assuming your SD-Card is recognized as **mmcblk1**, format the first partition with either VFAT or ext2 &#8211; I use ext2, since I have no Microsoft boxes to exchange data with anyway:

`# mke2fs /dev/mmcblk1p1`

Cool. Now we need to make the second partition an encrypted block device with:

`# cryptsetup luksFormat /dev/mmcblk1p2`

However, this only creates the container. Next we need to open it with the password we just gave it to be able to put a filesystem in there:

`# cryptsetup luksOpen /dev/mmcbkl1p2 NAME`

This creates a device mapper entry using NAME, so now we can finally format the encrypted partition with:

`# mke2fs /dev/mapper/NAME`

Now we shutdown the LUKS entry as we will take the card out:

`# cryptsetup remove NAME`

Done. Remove the card, insert it again. A password dialog should pop up and when you enter the correct password, you can access the device. The final step is to change the permissions on the filesystem to make sure you can always write to it.

`# chmod -R 777 /media/disk-1`

Happy hacking with secure storage!

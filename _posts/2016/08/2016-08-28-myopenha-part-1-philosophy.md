---
id: 1637
title: '#MyOpenHA Part 1 - Philosophy'
date: 2016-08-28
author: jwildeboer
layout: single
guid: https://jwildeboer.github.io/?p=1637
categories:
  - OpenMisc
tags:
  - Arduino
  - ESP8266
  - Home Automation
  - Philips Hue
  - MQTT
  - Node-RED
---
**Home Automation. The holy hipster and geek grail. I have played with it. I have tried. I have failed. But today I am proud to have a solution I can truly endorse. So join me on this journey. This series will explain my solution, in excruciating detail. In the hope that I can learn from you while I am explaining. This series will be filled over time with more and more articles. But now, let&#8217;s talk about philosophy. The Why. Soon you will see the What and How. One promise, or the TL;DR: It is all 100% Open Source.**

Well, almost. I have integrated some quite non-open things but always in an Open Source Way.

To wet your appetite, Here&#8217;s the simple thing you will see on a display hanging on the wall in my apartment:

![Home Automation Welcome Screen](/images/2016/08/image.jpeg)
*NodeRED UI on a tablet*

What you see here is the main screen of my Dashboard. From here you can switch Light Scenes, roll up or down the projector screen, send my Roomba to clean my place and observe the current outside temperature. This all runs from a [Raspberry Pi 2](https://www.raspberrypi.org/products/raspberry-pi-2-model-b/) with [FedBerry](http://fedberry.org/) (23 ATM), the [Mosquitto MQTT Broker](http://mosquitto.org/), a [Node-RED](http://nodered.org/) instance with the [node-red-dashboard](https://github.com/node-red/node-red-dashboard), controlling my [Philips HUE lights](http://www2.meethue.com/de-DE), a [Nanode](https://wiki.hackerspaces.org/Nanode), an [ESP8266](https://espressif.com/en/products/hardware/esp8266ex/overview) with a [Dallas DS1820 temperature sensor](http://www.jerome-bernard.com/blog/2015/10/04/wifi-temperature-sensor-with-nodemcu-esp8266/). Too much Techno-Babble? OK. That&#8217;s all going to be explained in the other parts of this series, so relax!

**Philosophy. Or the Zen of HA**

Home Automation truly is a fascinating field. But it&#8217;s also filled with land mines, frustration, lack of interoperability, competing standards, non-maintained cuteness and hype. A lot of hype.

Here are the guiding principles for what I consider to be a good Home Automation (HA) solution.

  * **Locality** Everything happens on the local network. There is no need at all for any external connections or services. This is where I protect my privacy. And guarantuee (well, shift the blame 😉 ) that I and only I am in control. Remember the [NEST fiasco](http://www.nytimes.com/2016/01/14/fashion/nest-thermostat-glitch-battery-dies-software-freeze.html?_r=0)? Not going to happen here.
  * **Simple** The solution must be simple to use, maintain, extend and install.
  * **Open** The parts of the solution I interact with must be 100% open. Ideally Open Hardware and Open Source. But at least accessible using a 100% Open Source solution.
  * **Sustainable** This means two things. It uses modules and parts that are actively maintained and simple to use for a long time. And it means that the solution doesn&#8217;t need my daily attendance. That I don&#8217;t have to be bothered with updates, changes, downtime and frustration. This is the unsolved part, to be honest.
  * **Rock Solid** The solution should be extremely stable. And in case of problems there must be a (manual) fallback solution for all parts.

Packed with failed experiments, the lessons I&#8217;ve learned at Red Hat about long-term stability and sustainability of Open Source and the vibrant community of fellow geeks sharing their experiences and solutions, I came up with the following approach:

  * **Low Level** Use &#8220;things&#8221; in the widest sense that can always be physically controlled. With switches, buttons or by simply pulling the plug.
  * **Communication** All parts talk [MQTT](http://mqtt.org/). Period. They send status updates and state changes via MQTT. They receive commands via MQTT. If they cannot do that directly, there must be an open source solution that does the translation between whatever protocol it uses and MQTT.
  * **Configuration** Now that we only have MQTT messages to send and receive, we use [Node-RED](http://nodered.org/) as the rules engine. Where we plug the parts together and come up with interactions as needed.
  * **Simplicity** With a nice UI using [node-red-dashboard](https://github.com/node-red/node-red-dashboard) in a browser, I can use whatever device that has a browser to control my apartment. Cheap Android tablets mounted to the wall, my smartphone or tablet. Or some voice controlled device, maybe? [Mycroft](https://www.kickstarter.com/projects/aiforeveryone/mycroft-an-open-source-artificial-intelligence-for)?

So there you have it. The blueprint for my truly open HA solution with flexibility and agility on all layers. Sounds too good to be true? I thought the same. And in some parts it still is. But for geeks with a bit of time, I can offer quite a working solution. Stay tuned! On to the next parts, where I will dive into the how and what.

Comments? Always welcome!

Google+: [#MyOpenHA Part 1](https://plus.google.com/+jwildeboer/posts/Fve2y6ejGRa?)
  
Twitter: [#MyOpenHA Part 1](https://twitter.com/jwildeboer/status/769960922483924992)

Or right here (but I&#8217;m more responsive on G+ and Twitter).

---
title: "PeerTube Video Embedding in a Jekyll Blog"
layout: single
categories:
    - Tips & Tricks & Fixes
tags:
    - Jekyll
    - PeerTube
author_profile: true
header:
  image: mh/mh005.jpg
comments:
  host: social.wildeboer.net
  username: jwildeboer
  id: 108510891285249247 
---

Below you see an embedded (rather stupid and pointless) video clip from my [PeerTube](https://vid.wildeboer.net/videos/watch/2567a1f0-d805-482f-93b5-0b3e38005d2b) instance.

{% include video host="vid.wildeboer.net" id="2567a1f0-d805-482f-93b5-0b3e38005d2b" provider="peertube" %}

I had some problems getting it to play reliably. So I asked the wonderful **Mastodon** community to [help me](https://social.wildeboer.net/@jwildeboer/108510073119374822).

![Help me](/images/2022/06/PTHelp.png)
*Asking for help*

And boy, did they help! In a matter of minutes! Some had my exact problem (spinning, never playing), some could get it to play when clicking on the timeline. It was quite a mess. Along comes [Chocobozzz](https://framapiaf.org/@Chocobozzz/108510271560335574) with the solution. Switch my PeerTube instance to HLS[^1]transcoding and try again.

![HLS Transcoding](/images/2022/06/HLSTranscoding.png)
*Set transcoding to HLS*

I still need to fix the CSS on this blog for a nice looking caption, but the more important part, the embedding, works.

And just like that - it works! You all don't know what misery you brought upon yourself. I might start sharing moving pictures on my blog now. You have been warned :)

So I thank my wonderful community over on **Mastodon** for again delivering immediate, friendly and solution driven feedback! Now to go record some videos ...

[^1]: HLS is [RFC 8216](https://datatracker.ietf.org/doc/html/rfc8216), an open standard, introduced by Apple back in 2009, the RFC was published in 2017 and it is now widely supported, according to the [Wikipedia page](https://en.wikipedia.org/wiki/HTTP_Live_Streaming)
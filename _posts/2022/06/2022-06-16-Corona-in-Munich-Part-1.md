---
title: Corona In Munich - numbers and problems
modified: 2022-06-17
categories:
  - OpenData
tags:
  - COVID19
  - Corona
  - Munich
  - Open Data
published: true
header:
  image: mh/mh008.jpg
comments:
  host: social.wildeboer.net
  username: jwildeboer
  id: 108488655098885694
---

On my 50th birthday, back in January 2020, the first official PCR diagnosis of a new Corona virus in Germany was made here in Munich. This became later known as the [Webasto Cohort](https://www.webasto-group.com/en/press/press-releases/press-release/coronavirus-two-more-webasto-employees-infected-and-diseased-122-colleagues-tested-negative/). Little did we know what it would cause. I immediately switched to prepared mode. Ordered masks, hand desinfection and prepared for more changes to my and all our lives. My gut feeling was right, as we definitely know now.

Quite soon after this all started, I noticed how difficult it was to get good numbers on cases, infections, deaths and other factors, specifically for Munich, where I live. This bothered me. I was not alone with that. My good friend [Georg](https://twitter.com/georgzoche) felt the same and started working on a spreadsheet to get better insights into what was happening around us.

We discussed back and forth about what was missing, what is needed. I wanted to try a few different things. So Georg shared his data with me and I started to collect numbers from more sources and put them in my own [spreadsheet](https://www.icloud.com/numbers/0tPTegqlj4Q2SZ7PysUbTt-gQ#MUCNumbersCorona)[^1], which I share with the public. I'm an Open Source guy after all :) 

![Graphs](/images/2022/06/Graphs.png)
*Sample graphs from my spreadsheet*

Since then, every morning I go to the [RKI dashboard](https://corona.rki.de) to fetch the newest numbers and update my data. I also get data on vaccinations from [here](https://github.com/robert-koch-institut/COVID-19-Impfungen_in_Deutschland/blob/master/Aktuell_Deutschland_Impfquoten_COVID-19.csv), hospital numbers from the bavarian [LGL](https://www.lgl.bayern.de/gesundheit/infektionsschutz/infektionskrankheiten_a_z/coronavirus/karte_coronavirus/index.htm#) and data from the [City of Munich](https://stadt.muenchen.de/infos/corona-fallzahlen-muenchen.html).

I build some nice graphs, shared them daily to complement the rather sparse data the City of Munich officially published, but things were going quite nicely most of the time. I felt comfortable, knowing enough to decice which risks were around me and how to act. We3aring masks, getting vaccinated was obvious. But the numbers gave me a good feeling about being able to visit a museam, a party - sucht things.

At times, I noticed weird things happening. I automatically compare the daily new numbers with the corrections added typically a day later and at one point, the difference became substantial. More than 100%. The official numbers were far too low. This caused a bit of a scandal, as the City of Munich had to admit the numbers were delayed and thus the official incidence was far too low. This happened at exactly the time (2021-09-07 - 2021-09-12) when the big [IAA car show](https://www.iaa-mobility.com) was in town. Coincidence? They promised to fix it. I kept on checking. Just to be sure.

![Differences](/images/2022/06/diff.png)
*Oops. So many delayed corrections*

But now it's 2022. People are tired of dealing with the risks. Big events are announced, like our famous [Oktoberfest](https://www.oktoberfest.de/en) here in Munich. After 2 years of no Wiesn. Let's party again!

So I start to notice that my data sources are drying up. Test numbers, previously updated daily, only get a weekly update. On thursday a week after. No new case/infection numbers on weekends and bank holidays. It becomes harder to maintain my data.

![Changelog](/images/2022/06/changelog.png)
*My changelog*

And this monday, 2022-06-13, the City of Munich announced they were more or less completely stopping with publishing their own numbers. Now we don't get to see those differences anymore. No more numbers on Activev Cases. No more daily tweets with numbers. The [page](https://stadt.muenchen.de/infos/corona-fallzahlen-muenchen.html) that gave us much needed data is now effectively deserted.

This doesn't look nor feel good. This pandemic is far from over. And just because the numbers disappear, the virus won't go away.

### Solutions.

I won't accept this. So I created [@CoronaMUC](https://twitter.com/CoronaMUC) and an alternative [dashboard](https://infogram.com/coronamuc-1h8n6m35mnndz4x?live) where I try to pick up where the officials have left us alone.

![Dashboard](/images/2022/06/dashboard.png)
*My dashboard, updated daily*

And it seems it is appreciated, more than 450 followers in just a week. But zero answers thus far by the City of Munich on my various questions. Well, sometimes you just have to do what has te be done, I guess :) I'll continue to update my data and make the results available to everyone for the time being :)

### Update 2022-06-17

We are getting a bit more followers on [@CoronaMUC](https://twitter.com/CoronaMUC) now that one of the local newspapers, the [Abendzeitung](https://www.abendzeitung-muenchen.de), has reported on the disappearing numbers and our work to keep interested citizens informed to the best of our capabilities in this [article](https://www.abendzeitung-muenchen.de/muenchen/blackbox-corona-die-stadt-muenchen-meldet-immer-weniger-zahlen-art-822732). Nice.

[^1]: Yes, it's Apple Numbers, it's iCloud. But it gives you CSV download too and works in the browser so you can check my formulas. Not perfect, not really open, but it works.
---
title: (OBSOLETE) A Quick Fix For The Verified Status for Github on your Mastodon Profile
categories:
  - Tips & Tricks & Fixes
tags:
  - Quick Fix
  - Pro Tip
  - Mastodon
  - Github
published: true
comments:
  host: social.wildeboer.net
  username: jwildeboer
  id: 108492990050298422
header:
  image: mh/mh006.jpg
---

**2023-02-18 UPDATE: This hack/fix is not needed anymore as [Github](https://github.com) now allows up to 4 social accounts on user profiles which set the correct link with the `rel="me"` attribute needed for Mastodon verification! Thank you [@skhg@mastodon.ie](https://mastodon.ie/@skhg/109882780524489841) for sharing this important change!**

![Github social accounts](/images/2023/02/githubsoc.png)

[Mastodon](https://joinmastodon.org) has a very light-weight method of proving that you are the owner (or, at least an editor) for the links on your profile page. Technically you add a `<a rel="me" href="https://link.to/YourProfile>Verification</a>` to the page you want to be verified.

You can see this on [my profile page](https://social.wildeboer.net/@jwildeboer), where I have a checkmark for this blog and for my [Github profile](https://github.com/jwildeboer).[^1]

![Verified](/images/2022/06/MastodonProfile.png)
*My Mastodon Profile*

Adding the link to my blog was simple. But Github? I just couldn't get it to work. When I added the link to my README.md, it showed up just fine.

![Link added](/images/2022/06/LinkGH.png)
*The link is added*

Except — it didn't work. No green checkmark ever appeared over on Mastodon. What did I do wrong? Turns out - nothing. Github decided to replace the `rel="me"`in the link with a `rel="nofollow"` for no appearant reason.

![Mangled](/images/2022/06/LinkMangled.png)
*Mangled link, no dice*

Why does Github do that? I don't know. But I am not the only one asking for a fix - or at least an explanation. [satchlj noticed](https://github.com/orgs/github-community/discussions/6248) the same thing. Unfortunately no answer has been given as of today.

### The Solution

Is there a way to solve this? Yes! It's a bit weird, but it works. You edit your Github profile and put the link to your profile in there, at this very place:

![Solution](/images/2022/06/GHSolution.png)
*Add link to to the link symbol*

Save your profile and check again on your Mastodon profile. **BAM**. Green checkmark! And now you can go back to Github, change the link back to something different, as the checkmark will stick. I honestly don't know why or how this works, but it does and now I have two checkmarks. Nice!

The green checkmark might not show up immediately for you on your Mastodon profile page. In that case, you can give your Mastodon server a little hint to check again:

- Go to your Mastodon profile
- Delete the link to GitHub
- Save your profile
- Add the link back
- Save your profile again.

That should force a new check and make the checkmark appear and stay around :)

Shoutout to [Florian Schwalm](https://digitalcourage.social/@FlorianSchwalm/108489231954003943) and [Michael](https://social.okoyono.de/@mezzo/108489245887174587) who explained this little workaround to me and [Peter Cock](https://fediscience.org/@pjacock/109314139942423389) for the tip on forcing a check on Mastodon.

### Update

In the comments I have heard that my profile doesn't show the verified badge everywhere. This is a caching issue. You are seeing my cached profile on your instance, not the current version on mine. I don't know how often these cached profile infos get refreshed and what triggers such a refresh. It's a side-effect of living in the federated world. If you go straight to my profile page at [https://social.wildeboer.net/@jwildeboer](https://social.wildeboer.net/@jwildeboer) you can check my checkmarks the way they actually are :)

[^1]: I am still trying to find out how I can add the verification link to my [PeerTube](https://vid.wildeboer.net) instance, which is hosted by [Spacebear.ee](https://federation.spacebear.ee/software/peertube) so I don't have direct access to the theme files to add the link to the header part. If you know the trick, please add a comment :)
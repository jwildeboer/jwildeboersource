---
title: A short, concise guide to participating in a protest
categories:
  - Blog
header:
  image: mh/mh005.jpg
comments:
  host: social.wildeboer.net
  username: jwildeboer
  id: 109020151437129611
---

(A while ago I [kinda complained](https://social.wildeboer.net/@jwildeboer/108222111872939281) that it is not easy to convert a thread on Mastodon to Markdown so I can turn it into a blog post. Well. [Vadim](https://social.vrutkovs.eu/@vadim) saw that, thought a bit and created [exactly that](https://mtr.wildeboer.net) - post the thread, get the markdown. This blog entry was created in just that way. Wow :)

**Modern protest guide in times of Surveillance Capitalism].**

First and foremost: GO. Participate in protests. Feet On The Street beats any sort of online petition (many of those platforms are full of trackers anyway).

Once you’ve decided to go - prepare. You should get a cheap digital camera. Doesn’t need a lot megapixels. Having zoom on that camera is nice. Put an empty SD card in your pocket too. I’ll explain why.

Do NOT take your mobile phone with you. If you have prepared where to be, how to get there and how to get back, you don’t need it. It’s a tracker device. Even when switched off, iPhones, for example, are still announcing their existence with the Bluetooth based „Find My“ function.

Android phones have comparable functions. So just leave your mobile at home. If you fear for violent clashes or need the assurance to have it with you, switch it off, put it in a faraday cage and only switch it back on when you are at a busy place where many people obviously meet.

At the protest, use your cheap camera to take the pictures you want - but also take pictures when you see things that go wrong - as long as it is safe for you. Your safety comes first.

**Avoid taking pictures in a way that makes other protestors identifiable.**

Should you get pulled into violent or dangerous situations, after you took the pictures, pull the SD card out ASAP and put in the empty one mentioned at the beginning. Take some pictures of innocent situations immediately. Should you get confronted with authorities and they demand to see your pictures, hand them the camera. Switch it on and show them how to view pictures. 

**You don’t have to mention the „real“ card to anyone.**

If you left your mobile phone at home, there’s nothing more you can give them ;) 

Should they find the „real“ SD card, don’t make a fuss. Hand it to them. Again - your safety comes first.

If everything went fine and you’re safe back home, go through the pix and post them in a concise thread with your story as a guide. This will help you put things in context.

**You don’t have to immediately share everything. You don’t have to livestream. It’s always out of context and will be hard to correct later.**

I could go on for hours, but I will stop here. Go out. Protest. Be safe. Be part of the change.

PS: You could also put some innocent pictures on the „empty“ SD card beforehand. Architecture pictures. Trees. Whatever. Just make sure there are no identifiable people on them and that the pix are not too old. Take them on the way to the protest, for example.
---
title: Cars, Robots and Standards
categories:
  - Blog
header:
  image: mh/mh005.jpg
published: false
#comments:
#  host: social.wildeboer.net
#  username: jwildeboer
#  id:
---

**This blog entry started as a thread on Mastodon [here](https://social.wildeboer.net/@jwildeboer/109422002828121032).**

![Cars](/images/2022/11/cars.jpg)
*Cars, not looking really different*

[\#Socialism](https://social.wildeboer.net/tags/Socialism) is waiting 20 years to get the one car that everybody gets.

[\#Capitalism](https://social.wildeboer.net/tags/Capitalism) is waiting one year to get the one car that everybody gets but from 20 different manufacturers.  [\#SarcasmButOnlyHalf](https://social.wildeboer.net/tags/SarcasmButOnlyHalf) ;)

There's a deeper thought hiding here, IMHO. The current form of capitalism that centralises on a lot of levels, creates behemoths that start to act like former socialist countries, complete with 5 year plans and limiting choice for consumers.

It becomes what it was supposed to fight ;)

This is not a new insight, BTW. But it needs a reminder, IMHO. The logical solution is, IMHO, quite simple:

Decentralise the hell out of everything.

Based on open standards that warrant interoperability while allowing for “on top” individualisation either DIY style or as a service.

The very complex problem is to find the delicate balance of such standards to ensure they stay neutral and don’t stifle innovation while discouraging deviations that cause breaking interoperability.

But no open standard can be completely neutral, IMHO. Being the weird person I am, I do think that standards should follow Asimov’s four laws of robotics. Let’s try and replace robots with standards and see how far we can get to recycle them as axioms for open standards :)

### Zeroth Law
A standard may not harm humanity, or, by inaction, allow humanity to come to harm.

### First Law
A standard may not injure a human being or, through inaction, allow a human being to come to harm.

### Second Law
A standard must obey the orders given it by human beings except where such orders would conflict with the First Law.

### Third Law
A standard must protect its own existence as long as such protection does not conflict with the First or Second Law.

Oh wow! That actually makes a lot of sense already! We need to do some rephrasing of at least the second law, but apart from that - I think this does make a good starting point for a discussion on and possibly a working definition of using open standards to lower entrance barriers to markets, allowing for micro to macro market coverage, The Open Source Way.
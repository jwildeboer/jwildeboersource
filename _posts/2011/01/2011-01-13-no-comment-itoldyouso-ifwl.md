---
id: 1376
title: 'No comment. #IToldYouSo #IFWL'
date: 2011-01-13
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1376
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
[Peter T. King](http://homeland.house.gov/about/chair), Chairman of the House Committee on Homeland Security. 

> To Clinton:
> 
> Dear Secretary Clinton:
> 
> I am writing to request that you undertake an immediate review to determine whether WikiLeaks could be designated a Foreign Terrorist Organization (FTO) in accordance with Section 210 of the Immigration and Nationality Act (INA). In addition, I urge you to work with the Swedish government to determine the means by which Mr. Julian Assange can be brought to justice for his actions while recognizing and respecting Swedish sovereign law.
> 
> As Admiral Mike Mullen, Chairman of the Joint Chiefs of Staff concluded, the &#8220;irresponsible posting of stolen classified documents by WikiLeaks puts lives at risk and gives adversaries valuable information.&#8221; I concur with Chairman Mullen&#8217;s statement. 
> 
> As you know, on Sunday, November 28, 2010, WikiLeaks released its third trove of classified information, in part consisting of 250,000 classified documents from the Department of State, causing grave damage to U.S. national security and our diplomatic relations. The latest release follows WikiLeaks&#8217; October 22, 2010 disclosure, which surmounted to the largest classified military leak in history. The latter, known as &#8220;The Iraq War Logs,&#8221; included 391,832 classified documents pertaining to U.S. military operations in Iraq from January 1, 2004 through December 31, 2009. That release followed WikiLeaks&#8217; July 2010 release of 76,900 classified documents pertaining to U.S. operations military in Afghanistan. By the sheer volume of the classified materials released, rendering harm to the United States seems inevitable and perhaps irreversible.
> 
> From these acts, WikiLeaks appears to meet the legal criteria for FTO designation as a (1) a foreign organization; (2) engaging in terrorist activity or terrorism which (3) threatens the security of U.S. nationals or the national security of the United States. Specifically, pursuant to Section 212 (a)(3)(B) of INA (8 U.S.C. § 1182(a)(3)(B)) WikiLeaks engaged in terrorist activity by committing acts that it knew, or reasonably should have known, would afford material support for the commission of terrorist activity. 
> 
> The Department of Defense has already recognized that WikiLeaks&#8217; dissemination of classified U.S. military and diplomatic documents affords material support to terrorist organizations, including Al Qaeda, Tehrik-e-Taliban Pakistan (TTP), Al Shabaab, and Hezbollah. In fact, On October 22, 2010, Pentagon spokesman Col. Dave Lappan stated:
> 
> We know terrorist organizations have been mining the leaked Afghan documents for information to use against us and this Iraq leak is more than four times as large. By disclosing such sensitive information, WikiLeaks continues to put at risk the lives of our troops, their coalition partners and those Iraqis and Afghans working with us.
> 
> The WikiLeaks releases provide valuable information and insights to FTOs throughout the world on U.S. military and diplomatic sources and methods and allow our enemies to better prepare for future U.S. and allied military, intelligence, and law enforcement operations targeting them. In addition, the leaks allow nation-states such as Russia, China, and Iran access to information regarding how the United States collects, analyzes, and produces intelligence products.
> 
> WikiLeaks presents a clear and present danger to the national security of the United States. I strongly urge you to work within the Administration to use every offensive capability of the U.S. government to prevent further damaging releases by WikiLeaks.
> 
> Thank you for your time and consideration of this urgent matter.
> 
> Sincerely,
> 
> PETER T. KING

And:

> To Holder:
> 
> Dear Attorney General Holder:
> 
> I am writing to urge you to criminally charge WikiLeaks activist Julian Assange under the Espionage Act, 18 U.S.C. § 793.
> 
> On Sunday, November 28, 2010, WikiLeaks released its third trove of classified information, which in part consists of 250,000 classified documents from the Department of State, causing grave damage to U.S. national security and our diplomatic relations. This latest release follows WikiLeaks&#8217; October 22, 2010 disclosure, which surmounted to the largest classified military leak in history. The latter, known as &#8220;The Iraq War Logs,&#8221; included 391,832 classified documents pertaining to U.S. military operations in Iraq from January 1, 2004 through December 31, 2009. That release followed WikiLeaks&#8217; July 2010 release of 76,900 classified documents pertaining to U.S. military operations in Afghanistan. By the sheer volume of the classified materials released, rendering harm to the United States seems inevitable and perhaps irreversible. Moreover, the repeated releases of classified information from WikiLeaks, which have garnered international attention, manifests Mr. Assange&#8217;s purposeful intent to damage not only our national interests in fighting the war on terror, but also undermines the very safety of coalition forces in Iraq and Afghanistan. As the Department of Defense has explicitly recognized, WikiLeaks&#8217; dissemination of classified U.S. military and diplomatic documents affords material support to terrorist organizations, including Al Qaeda, Tehrik-e-Taliban Pakistan (TTP) and Al Shabaab. 
> 
> As you are aware, following Private First Class Bradley E. Manning&#8217;s unauthorized access and transmission of classified materials, he was charged under the Uniform Code of Military Justice for several counts, including violation of 18 U.S.C. § 793(e) for willfully transmitting a &#8220;classified video of a military operation filmed at or near Baghdad, Iraq, on or about 12 July 2007 to a person not entitled to receive it.&#8221; Given Mr. Assange&#8217;s active role in encouraging the theft and distribution of classified material, he should be held liable pursuant to section 793(g), which provides that if more than one person conspire to violate any section of the Espionage Act and perform an act to the conspiracy, then &#8220;each of the parties to such conspiracy shall be subject to the punishment provided for the offense which is the object of such conspiracy.&#8221; In addition, Mr. Assange should be chargeable for obtaining classified documents pertaining to national defense initially acquired in violation of the Espionage Act and for willfully retaining such documents with the knowledge that he was not entitled to receive them.
> 
> There should be no misconception that Mr. Assange passively operates a forum for others to exploit their misappropriation of classified information. He actively encourages and solicits the leaking of national defense information. He pursues a malicious agenda, for which he remains totally immune to the consequences of his actions. In his July 2010 interview, Mr. Assange discussed his editorial control over the classified materials, noting that WikiLeaks is &#8220;clear about what we will publish and what we will not. We do not have
> 
> ad hoc editorial decisions.&#8221; He was even more resolute in his motivations: &#8220;I enjoy crushing bastards. So it is enjoyable work.&#8221;
> 
> As Admiral Mike Mullen, Chairman of the Joint Chiefs of Staff concluded, the &#8220;irresponsible posting of stolen classified documents by WikiLeaks puts lives at risk and gives adversaries valuable information.&#8221; I concur with Chairman Mullen&#8217;s assessment. 
> 
> Thank you for your prompt attention to this matter.
> 
> Sincerely,
> 
> PETER T. KING

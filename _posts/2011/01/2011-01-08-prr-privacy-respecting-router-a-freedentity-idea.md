---
id: 1359
title: 'PRR &#8211; Privacy Respecting Router a #freedentity idea'
date: 2011-01-08T13:23:20+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1359
aktt_tweeted:
  - 1
categories:
  - FREEdentity
  - OpenMisc
tags:
  - FREEdentity
  - OpenSociety
---
In the next few weeks I will outline more parts of something we (a few friends of me) are calling FreeDentity. That&#8217;s why I have added a category and a tag to my posts with the same name. So stay tuned for the philosophy, while in this post I talk about a project idea that actually implements that philosophy.

**The PRR &#8211; Privacy Respecting Router**

The typical internet user in the developed countries has a DSL connection with quite some speed (and unused bandwidth). For many people the main communication paths are email, facebook, twitter. Domain names are cheap. DynDNS etc allow you to reach your router at home with a defined hostname.

Current news show us a future that isn&#8217;t that bright. [Subpoenas](http://www.salon.com/news/opinion/glenn_greenwald/2011/01/07/twitter/index.html) against twitter users that only see the light due to twitter legal department asking to unseal the sealed (thus secret) subpoena are just the tip of the iceberg.

The only solution is actually quite simple. In order to gain more control over your privacy and data, you should keep it under your control whenever possible. Handing your data to Facebook, twitter or gmail however is the opposite of that. You hand over your data under typically broad terms of use that give Facebook, Twitter, Google a lot of rights and leave you in the dark about what actually happens with it.

Two things have happened however that might make a change for some of us (hopefully becoming more and more):

  * **Decentralized Open Solutions** like [statusnet](http://status.net/), [diaspora](https://joindiaspora.com/) and good ole email servers
  * **Powerful routers** that can handle a lot of work, not just forwarding and NATting IP packets.

So let&#8217;s combine all of that into one thing. An Open Source firmware for typical routers that offer these functions, thus keeping your data private but still allowing you to share with the world. All of that under an extremely simple User Interface.

A firmware that when doing its first-time config asks you for your domain name, sets up a local mail server for that domain, updates the DNS entries to make sure your mail comes to you.

A firmware that sets up a simple instance of statusnet, with the possibility to sync your dents to twitter.

A firmware that runs a simple instance of diaspora, allowing you to do the facebook dance from your home.

A firmware that hands out WebID so you can use your server from the internet in a secure way, eg from your android phone, netbook etc when on the road.

A firmware that stores all of your data in a secure way either on flash, USB Harddrive or NAS &#8211; but local, not in the cloud.

The requirements are thus:

  * **Simple Installation** Ask only what is really needed, do everything with automagic in the background.
  * **Reliability** run forever, use minimal resources.
  * **Secure** Encrypt whatever goes out and comes in.
  * **Open** Use only open standards, only Free Software. Royalty Free. No calling home. No central registry of users.

Would that be what Moglen talks about when referring to his freedom box? I don&#8217;t know. But I do know that my data is too valuable to let out uncontrolleed or governed by companies that I cannot trust forever.

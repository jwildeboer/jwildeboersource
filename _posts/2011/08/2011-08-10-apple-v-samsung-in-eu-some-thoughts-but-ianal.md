---
id: 1485
title: 'Apple v Samsung in EU &#8211; some thoughts, but IANAL'
date: 2011-08-10T17:12:33+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1485
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
So the text of Apples complaint filed at a german court is available (I won&#8217;t link to it as I do not wish to promote the person that posted it) and after reading it, I noticed a few oddities worth checking. Note that the text of the injunction itself is not available ATM.

If you can read german, you should read [this post](http://www.internet-law.de/2011/08/was-steckt-hinter-der-unterlassungsverfugung-von-apple-gegen-samsung.html) from a german lawyer, it contains some good stuff.

Now AFAICS the main point of Apples complaint centers around the uniqueness of its design, which they registered in the US, europe and a few other places too (and trust me, if they could they would do it for the whole galaxy and all possible parallel universes).

The european registered design, which can be found [here](http://esearch.oami.europa.eu/copla/design/data/000181607-0001), however is surprising to me.

First, an obvious point. The iconic home button of all iPods, iPhones, iPads is **missing**! How can that be? It is one of the novel things you would definitely want to protect if you are Apple, wouldn&#8217;t you think? Fun part is that Samsungs Galaxy Tab 10.1 &#8211; you guessed it &#8211; does NOT have a home button. That&#8217;s odd.

But that is not the real problem IMHO. We need to dig a bit deeper. What are the laws, regulations saying about the quality needed for a design to be protected under these rules?

In this case you have to look at the Council Regulation (EC) nº 6/2002 of 12 December 2001 on Community Designs, which you can get [here](http://oami.europa.eu/ows/rw/resource/documents/RCD/regulations/62002_en_cv.pdf).

Besides all the legal yadayada you will find at least Article 8, para 1 very significant:

> Article 8 
> Designs dictated by their technical function and designs of interconnections
> 
> 1. A Community design shall not subsist in features of appearance of a product which are solely dictated by its technical function.
> 
> [&#8230;] 

Now unless I am completely overseeing something very significant, if your plan is to create a device in a tablet form factor, there really isn&#8217;t much you can design differently from a rectangular shaped thin case with a display.

Exactly that however is what Apple claims to &#8220;own&#8221; with this community design.

So IMHO, which is my personal, layman opinion, this community design should have not been granted as it looks impossible to me come up with a substantially different design.

If I would be Samsung, this would be one of the points I would focus on. #justsayin though.

Also worth noting is how the regulation uses the word &#8220;informed user&#8221; to set the barrier for when a design can be considered infringing. Does Apple really think that an &#8220;informed user&#8221; will mistake a tablet that does NOT have the Apple symbol and that does NOT have the iconic home button is an iPad? Do they really think we are all THAT stupid? 😉

---
id: 1471
title: Apple stops Samsung from offering Galaxy Tab 10.1 in Europe
date: 2011-08-09T17:14:26+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1471
aktt_tweeted:
  - 1
flattrss_autosubmited:
  - 'true'
categories:
  - OpenMisc
---
German court grants injunction to Apple. German article [here](http://www.faz.net/artikel/C30350/streit-mit-samsung-apple-untersagt-galaxy-tab-vertrieb-30482345.html). Quite some more, but thy all paraphrase the original article that was sent out by [dpa](http://www.dpa.de).

Apple, it is time to grow up and face competition the way you should. Not in courts. In the market. The Galaxy Tab 10.1 is a damn fine piece of hardware with a brilliant display, as far as I have seen.

Details are not available to me ATM but I will update soon when I get them. Stay tuned.

The injunction (Einstweilige Verfügung) was granted by the Landgericht in Düsseldorf. This means that the court saw enough arguments in favour of Apple to grant the injunction immediately, without waiting for the outcome of the case itself. This however also means that the main case is not decided. That will take some time, typically. As Samsung seemingly had planned to launch in the next few days, this is a tough blow for them, I guess.

Seems that the official release date would be the 17th of August. You can still order the Galaxy Tab 10.1 at Amazon.de now, BTW. 

The news came via dpa, a press agency, they claim to have the news from the court proceedings today. No case number available ATM, no official press release from the court, Apple or Samsung ATM.

Some are sure that this at least partly is about a design patent. So effectively Apple claims that everything that is rectangular, flat, has a display and an earphoneplug violates their design patent which you can find [here](http://www.google.com/patents?id=6BsWAAAAEBAJ&#038;printsec=abstract&#038;zoom=4&#038;source=gbs_overview_r&#038;cad=0#v=onepage&#038;q&#038;f=false) &#8211; this is the original US design patent that supposedly is used in the european case.

Note that a design patent is not a real patent. It is a registered design, that&#8217;s it. Has nothing to do with patents, prior art etc. That&#8217;s why it has a D at the front.

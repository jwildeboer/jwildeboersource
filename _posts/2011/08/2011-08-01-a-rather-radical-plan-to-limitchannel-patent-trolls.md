---
id: 1466
title: A rather radical plan to limit/channel patent trolls
date: 2011-08-01T12:15:53+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1466
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
**DISCLAIMER: This entry representy my personal opinion and is in now way related to my job nor does it reflect the opinion of my employer.**

Here&#8217;s a rather radical idea to limit/channel patent trolls.

  * Have all software/hardware companies put 1% of their revenue in a global patent pot, managed by WIPO. 
  * Have all patent trolls file their claims and distribute the 1% according to the claims.
  * Make laws that ensure that the 1% pot is the only source of patent license money, so all SW/HW companies can safely invest in progress and innovation.
  * Use this pot also for patent encumbered standards. So all standards are capped at max 1% royalty, allowing simple implementation and distribution as Open Source.

**Voice your opinion! Vote on the poll!**

Go to the following <a href="https://plus.google.com/112648813199640203443/posts/1EyXtb5GakL" title="Poll" target="_blank">post on googleplus</a>, +1 the YES, NO, or UNDECIDED comments to vote.

DISCLAIMER: This is my personal opinion only. Not related to my job.

---
id: 1483
title: 'Idea: Better architecture for computers'
date: 2011-08-10T15:38:11+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1483
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
SSDs that disguise as hard drives make no sense IMHO. A cool architecture would have temporary storage (RAM) and stable storage (Flash) on the same bus. Imagine what that would mean. No more stupid copying of (read-only) libraries from SSD to RAM, instantiate them directly from where they are. Such a mixed architecture would be a radical memory saver. #justsayin and now go, patent it someone, this post is prior art.

This is not really a new idea, BTW. Quite some of the embedded market works this way. Think Arduinos etc.

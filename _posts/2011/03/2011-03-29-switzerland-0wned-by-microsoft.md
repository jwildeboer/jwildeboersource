---
id: 1446
title: 'Switzerland: 0wned by Microsoft, OK by court?'
date: 2011-03-29T14:52:30+00:00
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1446
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
**DISCLAIMER**: I am not a lawyer and this is my PERSONAL blog. This article reflects my PERSONAL opinion and does not necessarily represent the position of my employer.

**UPDATE**: The text of the court decision in swiss-german is [here](http://relevancy.bger.ch/php/aza/http/index.php?lang=de&#038;type=highlight_simple_query&#038;page=1&#038;from_date=&#038;to_date=&#038;sort=relevance&#038;insertion_date=&#038;top_subcollection_aza=all&#038;query_words=2c_783%2F2010&#038;rank=1&#038;azaclir=aza&#038;highlight_docid=aza%3A%2F%2F11-03-2011-2C_783-2010&#038;number_of_ranks=1)

More background [here](https://jan.wildeboer.net/2010/07/switzerland-0wn3d-by-microsoft/)

So there we have it. The Bundesgericht in Switzerland has decided. And it&#8217;s result is stunning, to put it mildly. In layman terms, this is what has been ruled:

  * Open Source cannot offer an alternative to Microsofts offering
  * The authorities cannot be asked to do market research to find alternatives.
  * Hence it is perfectly fine to NOT tender at all and hand over the money to Microsoft.

The appeal that has been filed has not been accepted by the court, so there was no case effectively. The court ruled that FOSS failed to deliver an alternative to Microsoft. How FOSS could have shown an alternative at all as there was (and is) no tender, no rundown of needed capabilities, no description of requirements was not of interest.

Are you surprised? Well &#8211; I am. This effectively means that Microsoft&#8217;s monopoly in swiss authorities is now here to stay. By not even asking the authorities to do some due diligence wrt market research, it effectively means that the free market has just been removed from the equation.

I will update this story with more details in teh next few hours, please bear with me. I have to translate legal swiss language to english and IANAL &#8230;

Here some comments in german, more to come: [Blog entry DigitaleNachhaltigkeit](http://www.digitale-nachhaltigkeit.ch/2011/03/bundesgericht-entscheidet-fuer-microsoft/)

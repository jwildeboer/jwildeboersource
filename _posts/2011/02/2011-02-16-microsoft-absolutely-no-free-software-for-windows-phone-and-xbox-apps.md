---
id: 1419
title: 'Microsoft: Absolutely NO (GPLv3-or-compat-licensed) Free Software for Windows Phone and Xbox Apps.'
date: 2011-02-16
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1419
aktt_tweeted:
  - 1
categories:
  - OpenMisc
tags:
  - GPL
  - Microsoft
  - XBox
---
**DISCLAIMER**: I am not a lawyer and this is my PERSONAL blog. This article reflects my PERSONAL opinion and does not necessarily represent the position of my employer.

**DISCLAIMER 2**: Some articles that link here claim I say that \*ALL\* Open Source software is banned by Microsoft. That is NOT what I say. Please read on.

**UPDATE**

**Italian** translation of this article by Guglielmo Troiano at [&#8220;Microsoft: niente Software Libero per Windows Phone e Xbox Apps.&#8221;](http://www.troiano.org/blog/microsoft-no-software-libero-windows-phone-apps-xbox.html)

**UPDATE 2** Seems that the terms from Microsoft also exclude their own [MS-PL license](http://www.microsoft.com/opensource/licenses.mspx#Ms-PL) as pointed out by @webmink in the comments and [in this article](http://blogs.computerworlduk.com/simon-says/2011/02/microsoft-bans-its-own-licenses/index.htm) and also by @fontana on identi.ca &#8211; now THAT is cool if correct :-) 

**This is rather uncool, IMHO, I stumbled upon [this forum entry](http://discussion.forum.nokia.com/forum/showthread.php?219222-Symbian-C-to-WP7-migration-questions) and was quite astonished. It points to the [Microsoft Application Provider Agreement](http://create.msdn.com/downloads/?id=638) that governs the Windows Marketplace, the App Store where users can get apps and developers publish them.**

Now here&#8217;s the fun part. In article 5, Microsoft explains the Application Requirements that you need to fulfill to get your app accepted in the marketplace. It&#8217;s point E that is of interest here:

> e. The Application must not include software, documentation, or other materials that, in whole or in part, are governed by or subject to an Excluded License, or that would otherwise cause the Application to be subject to the terms of an Excluded License.

Note the full scope: in whole or in part. This means that you cannot use libraries that are under this ominous &#8220;Excluded License&#8221;. Or use documentation that is licensed under the ominous &#8220;Excluded License&#8221;. You get the point. If you use whatever stuff that is under this ominous &#8220;Excluded License&#8221; your app will not be added to the marketplace.

Now what is this ominous &#8220;Excluded License&#8221;? Scroll back in the document and find:

> “Excluded License” means any license requiring, as a condition of use, modification and/or distribution of the software subject to the license, that the software or other software combined and/or distributed with it be (i) disclosed or distributed in source code form; (ii) licensed for the purpose of making derivative works; or (iii) redistributable at no charge. Excluded Licenses include, but are not limited to the GPLv3 Licenses. For the purpose of this definition, “GPLv3 Licenses” means the GNU General Public License version 3, the GNU Affero General Public License version 3, the GNU Lesser General Public License version 3, and any equivalents to the foregoing.

So each and all &#8220;equivalents&#8221; to the GPLv3, LGPLv3, Affero GPLv3 license are excluded. Any license that <del datetime="2011-02-16T11:52:54+00:00">allows</del> requires redistribution at no charge is excluded.

The consequences of this strange exclusion are not fully clear to me as I am not a lawyer. But one thing is extremely obvious. Microsoft wants to keep its platform clear of Free Software. Period.

This coming from the company that publicly claims to be a friend of Open Source, that wants to make windows the best ever platform for Open Source should make app developers think again if this mobile platform is the platform of choice.

Geez.

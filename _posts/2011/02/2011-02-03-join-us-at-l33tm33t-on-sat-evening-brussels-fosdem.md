---
id: 1414
title: 'Join us at #l33tm33t on sat evening, brussels, FOSDEM'
date: 2011-02-03
author: jwildeboer
layout: single
guid: https://jan.wildeboer.net/?p=1414
aktt_tweeted:
  - 1
categories:
  - OpenMisc
---
Saturday evening from 20:30. 3 events meet at one place. Be there.

Register [HERE &#8211; http://l33tm33t.eventbrite.com/](http://l33tm33t.eventbrite.com/)

    Au Bon Vieux Temps
    12, Rue Marché aux Herbes
    1000 Brussels
    
    Belgium

**BREAKING**: We will have the mobile Transnational Immigration Office, so we can register citizens for the first transnational republic and give them Transnational ID cards! See [UTNR](http://www.utnr.org) for more info! **EUR 15** for covering costs per ID Card. We need your national ID to verify.

**1. L33tM33T At FOSDEM** &#8211; where people meet to discuss, define and exchange info on **decentralized** solutions for the future. Based on projects like **oStatus, Ushahidi, FREEdentity, Thimbl, Diaspora** we all agree that the future of communication, digital life and meatspace is going to be decentralized.

At **L33tM33T** we want to bring together the architects, hackers and users of the upcoming decentralized world. That&#8217;s why we do everything at the same time!

**2. Sharism Presents Brussels**
  
Let us know, by following us at @sharism you are coming, or just show up! Its a free and open event. Sharism Presents are events without slides and everyone shares one thing for an open discussion.

**3. StatusCheck** is a celebration of StatusNet and Free Network Services. Denters Unite!

Come on out!

Join us! Have a beer! Meet other people! Let&#8217;s make a change.

---
title: Running uptime-kuma with podman and RHEL 9 as a systemd service 
categories:
  - Personal
tags:
  - nginx
  - podman
  - container
published: true
header:
  image: mh/mh006.jpg
comments:
  host: social.wildeboer.net
  username: jwildeboer
  id: 109875835336804573 
---

TIL: With a simple [#podman](https://social.wildeboer.net/tags/podman) command I can install and run a container that monitors my services - [https://github.com/louislam/uptime-kuma](https://github.com/louislam/uptime-kuma) from the wonderful [Louis Lam](https://github.com/louislam).

With a second simple podman command I turn that container into a [#systemd](https://social.wildeboer.net/tags/systemd) service, so it automagically starts after a reboot. Et voilá!

![uptime-kuma](/images/2023/02/kuma.png)
*The uptime-kuma dashboard*

## Prelaminaries

I run my server with RHEL 9 (Red Hat Enterprise Linux) which means I also get a nice overview with [#cockpit](https://social.wildeboer.net/tags/cockpit).

![Cockpit](/images/2023/02/cockpit.png)
*Cockpit dashboard showing the status of my containers*

With the Red Hat Developer Subscription for individuals you get RHEL for absolutely free, even for production setups, on up to 16 physical or virtual nodes. See [https://developers.redhat.com/articles/faqs-no-cost-red-hat-enterprise-linux#](https://developers.redhat.com/articles/faqs-no-cost-red-hat-enterprise-linux#)

## Let's get it done

So how did I do this? Well, not that much work.

- Create a container using [#podman](https://social.wildeboer.net/tags/podman)
- Add a [Let's Encrypt](https://letsencrypt.org) certificate
- Configure `nginx`
- Add it as a `systemd` service using `podman`

OK. Let's start!

## Create the container:  

The container with uptime-kuma is in many registries, so no problem with that part.

```
podman run -d --restart=always -p 3001:3001 -v uptime-kuma:/app/data --name uptime-kuma louislam/uptime-kuma:1
```

This downloads the container, sets up the local container storage and port forwarding and runs it.

## Add a Let's Encrypt certificate

I use `nginx` as reverse proxy to expose my containers to the internet, so we need to add a server entry to `/etc/nginx/nginx.conf`

To save myself some time, I run the `certbot` command to add a certificate for the new subdomain, which also adds the stub entries in `/etc/nginx/nginx.conf`:

```
certbot --nginx -d up.MYDOMAIN.TLD
```

With this I don't have to worry about the certificate stuff, as `certbot`will automagically check and renew the certificate.

## Configure nginx

I will expose this service under a subdomain called `up.MYDOMAIN.TLD` as that is the most simple way. So I set up the DNS as needed, which in this case is a simple CNAME entry like

```
up.MYDOMAIN.TLD.	10800	IN	CNAME	srv02.MYDOMAIN.TLD
```

(do note the trailng . after TLD, DNS needs it and it is a common mistake to forget it. It's ALWAYS DNS when things go wrong ;)

Which means that requests for `https://up.MYDOMAIN.TLD` will be handled by the nginx server on `srv02.MYDOMAIN.TLD.

With that out of the way, I can extend the server entry in my `/etc/nginx/nginx.conf` under the `server` section to look like this:

```
server {
    server_name up.MYDOMAIN.TLD;
    location / {
        proxy_set_header   X-Real-IP $remote_addr;
        proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header   Host $host;
        proxy_pass         http://127.0.0.1:3001/;
        proxy_http_version 1.1;
        proxy_set_header   Upgrade $http_upgrade;
        proxy_set_header   Connection "upgrade";
    }
    listen [::]:443 ssl; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/up.MYDOMAIN.TLD/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/up.MYDOMAIN.TLD/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
}

server {
  if ($host = up.MAYDOMAIN.TLD) {
      return 301 https://$host$request_uri;
  } # managed by Certbot

  server_name up.MYDOMAIN.TLD;
  listen 80;
  listen [::]:80;
  return 404; # managed by Certbot
}
```

This does two things. It ensures that requests to `https://up.MYDOMAIN.TLD` get forwarded to the container listening on port 3001 and that requests to `http://up.MYDOMAIN.TLD` get redirected to the secure https connection.

If you are using a different reverse proxy setup, you can find an extensive list of configurations [here](https://github.com/louislam/uptime-kuma/wiki/Reverse-Proxy#nginx).

## Install a systemd service:

Podman can create a systemd service file from a running container, so let's do that.

```
podman generate systemd --new --name uptime-kuma -f /etc/systemd/system/uptime-kuma.service  systemctl enable uptime-kuma
```

And that's it! A container checking the uptime of my services, running as a systemd service, that just works.

Feel free to comment using Mastodon!